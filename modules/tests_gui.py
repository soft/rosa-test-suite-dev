#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@:::::::::@@@@@@@@@@@::::::@@@@@@@@@@@::::@@@@@@@@@@@@@:@@@@@@@@@   │
# │   @@@:------------::@@@@:----------::@@@@:--------::@@@@@@@:---:@@@@@@@   │
# │   @@@:---@@@@@@@@:--:@@---:@@@@@@@---:@@---:@@@@@:@@@@@@@@:-----@@@@@@@   │
# │   @@@:---@@@@@@@@:--:@:--:@@@@@@@@:---@@---:@@@@@@@@@@@@@@--:@:--@@@@@@   │
# │   @@@:---@@@@@@@:---@@:--:@@@@@@@@:---@@@:------:::@@@@@@---@@@--:@@@@@   │
# │   @@@:-----------::@@@:--:@@@@@@@@:---@@@@@@:::::---@@@@:--:@@@:--:@@@@   │
# │   @@@:---@@@@@:--:@@@@:--:@@@@@@@@:--:@@@@@@@@@@@:--:@@:-----------:@@@   │
# │   @@@:---@@@@@@:---:@@@:--:::@@@::--:@@::-:::@@::---@@:--:@@@@@@@---:@@   │
# │   @@@:::-@@@@@@@@::::@@@@:::-----::@@@@@@:::----::@@@::::@@@@@@@@:::-@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │                                                                           │
# │                              ROSA Test Suite                              │
# │                                                                           │
# │   Copyright © 2020 LLC "NTC IT ROSA"                                      │
# │   License: GPLv2                                                          │
# │   Authors:                                                                │
# │       Arthur Yalaletdinov <a.yalaletdinov@ntcit-rosa.ru> 2013             │
# │       Michael Mosolov     <m.mosolov@rosalinux.ru>       2019             │
# │                                                                           │
# │   This program is free software; you can redistribute it and/or modify    │
# │   it under the terms of the GNU General Public License as published by    │
# │   the Free Software Foundation; either version 2, or (at your option)     │
# │   any later version.                                                      │
# │                                                                           │
# │   This program is distributed in the hope that it will be useful,         │
# │   but WITHOUT ANY WARRANTY; without even the implied warranty of          │
# │   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            │
# │   GNU General Public License for more details                             │
# │                                                                           │
# │   You should have received a copy of the GNU General Public License       │
# │   License along with this program; if not, write to the                   │
# │   Free Software Foundation, Inc.,                                         │
# │   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.           │
# │                                                                           │
# ╘═══════════════════════════════════════════════════════════════════════════╛


"""
Модуль тестирования графического интерфейса
"""

import fileinput
import logging
import os
from os import sep
import re
import shutil
import signal
import subprocess
import sys
import time
from pathlib import Path

from modules.kplpack.utils import Damper, chmodR, chownR, User

with Damper():
  import gi
  gi.require_version( 'Gtk', '3.0' )
  from gi.repository import GObject, Gtk


from modules.kplpack.utils import StreamToLog, Terminal, processEx, waitCPU, waitProcess, x



log       = logging.getLogger("rts.guitests")
tool      = '/usr/lib64/rosa-test-suite/xtool'
tools     = '/usr/lib64/rosa-test-suite'
test_data = '/usr/share/rosa-test-suite/test_data'


def testDesktopThemesSX(theme_name):

  waitCPU(cpuload=5, verbose=True)

  def replaceAll(textFile, searchExp, replaceExp):
    for line in fileinput.input(textFile, inplace=1):
        if searchExp in line:
            line = line.replace(searchExp, replaceExp)
        sys.stdout.write(line)

  config_tpl = (
      '[GTK]\n'
      'sNet/ThemeName=%s \n'
  ) % theme_name

  config_path = os.path.expanduser('~/.config/lxsession/LXDE')
  config_file = os.path.join(config_path, 'desktop.conf')

  if not os.path.exists(config_file):
    try:
        os.makedirs(config_path)
    except OSError:
        pass
    with open(config_file, 'w') as f:
        f.write(config_tpl)
    subprocess.getstatusoutput('lxsession -r')
    time.sleep(5)
    os.remove(config_file)
    subprocess.getstatusoutput('lxsession -r')
  else:
    status, output = subprocess.getstatusoutput('grep -E "^sNet/ThemeName" ' +
                                              config_file)
    if status != 0:
      print('В файле конфигурации LXDE нет настройки темы.', file=sys.stderr)
    else:
      m = re.search(r'sNet/ThemeName=([^\0]+)', output)
      if m:
        prevWidgetStyle = m.group(1)
        replaceAll(config_file, prevWidgetStyle, theme_name)
        subprocess.getstatusoutput('lxsession -r')
        time.sleep(5)
        replaceAll(config_file, theme_name, prevWidgetStyle)
        subprocess.getstatusoutput('lxsession -r')

def testDesktopThemesDX(theme_name='cde'):
    from PyKDE4.kdecore import KConfig, KConfigGroup
    from PyKDE4.kdeui import KGlobalSettings
    from PyQt4 import QtCore

    #waitCPU(cpuload=5, verbose=True)

    def setStyle(ws):
        kg.writeEntry("widgetStyle", ws)
        kg.sync()
        kgs.emitChange(KGlobalSettings.StyleChanged)

    app = QtCore.QCoreApplication([])
    kc = KConfig("kdeglobals")
    kg = KConfigGroup(kc, "General")
    kgs = KGlobalSettings.self()
    prevWidgetStyle = kg.readEntry("widgetStyle")
    QtCore.QTimer.singleShot(5, lambda : setStyle(theme_name))
    QtCore.QTimer.singleShot(5000, lambda : [setStyle(prevWidgetStyle),
                                             app.exit(0)])
    return app.exec_()

def testHangingApp():

  def doom():
    #time.sleep(300)
    signal.sigwait({signal.Signals.SIGKILL})
    Gtk.main_quit()

  def sigcage(signum, frame):
    #print( f'#{signum} {frame}')
    if signum == 15 or signum == 9:
      Gtk.main_quit()

  win = Gtk.Window()
  win.set_size_request(360, 70)
  win.set_resizable(False)
  win.set_title('Проверка прерывания процесса')

  vbox = Gtk.VBox(False, 0)
  win.add(vbox)
  label = Gtk.Label( 'Это окно демонстрирует процесс, выполнение которого должно быть прервано.\n'
                     'ПОЖАЛУЙСТА, ДОЖДИТЕСЬ ОКОНЧАНИЯ ТЕСТА')

  label.set_margin_left(10)
  label.set_margin_right(10)
  label.set_margin_top(10)
  label.set_margin_bottom(10)
  label.set_justify(Gtk.Justification.CENTER)
  label.set_line_wrap(True)
  label.set_size_request(350, 40)

  vbox.add(label)

  win.connect("destroy", Gtk.main_quit)
  win.show_all()
  GObject.timeout_add(300, doom)

  for sig in signal.Signals:
    try:
      signal.signal(sig, sigcage)
    except OSError: pass
      #print( f'skip:{sig}' )

  Gtk.main()

def runOfficeApp(appname, binname, ext):

  log.info( '\tТестирование приложения {} с файлом формата {}...'.format( appname, ext ) )

  errcode = 0
  at_tool = Path( tools + '/test_{}'.format( binname ) )


  testfile = os.path.expanduser('~/Пробный_документ.' + ext)

  with Terminal( StreamToLog( log ) ) as term:
    term.error( 'Во время тестирования произошла ошибка [{code}]: {msg}' )

    term.add( '{} --nologo --norestore'.format( binname ), separated=True, quiet=True )\
      .info( '\tЗапуск {}...\n'.format( binname ) )
    term.add( '{} "{}" "Пробный документ в формате {}"'.format( at_tool.as_posix(), testfile, ext ), pause_before=5)\
      .info( '\tЗапуск тестирующей программы {}...\n'.format( at_tool.as_posix() ) )
    term.add( '{} --nologo --norestore "{}"'.format( binname, testfile ), separated=True, quiet=True )\
      .info( '\tОткрытие созданного файла "{}"...\n'.format( testfile ) )
    term.add( '{} "Пробный_документ.{} - {}" -t 15000'.format( tool, ext, appname ), separated=True, pause_before=1)\
      .info( '\tПроверка созданного файла...\n')

  errcode = term.fail[0]

  return errcode

def testSupportOfficeDocuments():

  os.environ['SAL_USE_VCLPLUGIN'] = 'gtk'
  os.environ['GTK_MODULES'] = 'gail:atk-bridge'
  os.environ['GNOME_ACCESSIBILITY'] = '1'
  os.environ['NO_AT_BRIDGE'] = '0'


  tests = [
      ('LibreOffice Writer', 'lowriter', 'txt'),
      ('LibreOffice Writer', 'lowriter', 'rtf'),
      ('LibreOffice Writer', 'lowriter', 'odt'),
      ('LibreOffice Writer', 'lowriter', 'doc'),
      ('LibreOffice Writer', 'lowriter', 'docx'),
      ('LibreOffice Calc', 'localc', 'xls'),
      ('LibreOffice Calc', 'localc', 'xlsx'),
      ('LibreOffice Calc', 'localc', 'ods'),
      #too old('LibreOffice Calc', 'localc', 'sxc'),
      ('LibreOffice Impress', 'loimpress', 'odp'),
      ('LibreOffice Impress', 'loimpress', 'ppt'),
      ('LibreOffice Impress', 'loimpress', 'pptx'),
  ]

  p = None

  try:

    errcode = processEx( 'at-spi2-registryd' )

    if errcode > 0:
      log.info( '\tСлужба at-spi-registryd уже запущена.' )

    else:
      log.info( '\tЗапуск службы at-spi-registryd...' )
      at_spi_cfg = '/usr/share/dbus-1/accessibility-services/org.a11y.atspi.Registry.service'

      if not os.path.exists(at_spi_cfg):
        log.info( '\tНе найден конфигурационный файл {}.'.format( at_spi_cfg ) )
        return 101

      at_spi_bin = None
      with open(at_spi_cfg, 'r') as f:
        at_spi_reg = f.read()
        for line in at_spi_reg.split('\n'):
          m = re.search(r'^Exec\s?=\s?(.*)\s(.*)', line)
          if m:
            at_spi_bin = m.group(1)
            at_spi_options = m.group(2)
            break

      if not at_spi_bin:
        log.info(' \tОпределить местоположение at-spi-registryd не удалось.' )
        return 102

      at_env = os.environ.copy()
      p = subprocess.Popen( [at_spi_bin, at_spi_options], start_new_session=True, env=at_env, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT )

      time.sleep(1)
      if p.poll() is not None:
        log.info( '\tОшибка при запуске at-spi-registryd.Сообщения программы: {}'.format( p.stdout.read() ) )
        return 103

    for test in tests:
      ret = runOfficeApp( *test )
      if ret != 0:
        print( ret, flush=True )
        return

  finally:
    if p and p.poll() is None:
      try:
        log.info('Завершение работы at-spi-registryd...')
        p.terminate()
      except OSError:
        pass

  print( 0, flush=True )
  return

def testSupportPdfDocuments():

  doc = Path( test_data + '/document.pdf' )

  with Terminal( stream=StreamToLog(log, logging.DEBUG ) ) as T:
    T.error('Во время тестирования произошла ошибка [{code}]: {msg}\n')

    T.add( '{} "Документ в формате PDF" -t 15000'.format( tool ), separated=True )\
      .info( '\tЗапуск проверяющией утилиты...\n' )
    T.add( 'okular {}'.format( doc.as_posix() ), separated=True, quiet=True )\
      .info( '\tЗапуск приложения okular c документом {}...\n'.format( doc.as_posix() ) )


  if T.fail[0]:
    print( T.fail[0], flush=True )
    return

  print( 0, flush=True )
  return

def testCheckingForBrowser():

  log.info('\tЗапуск браузера...')

  with Terminal( stream=StreamToLog( log ) ) as T:
    T.error( 'Во время тестирования произошла ошибка [{code}]: {msg}\n' )

    T.add( '{} "TEST PASSED" -t 30000'.format( tool ), separated=True )\
      .info( '\tПроверка браузера...' )

    T.add( 'chromium-browser /usr/share/rosa-test-suite/test_data/web/test.html', separated=True, timeout=15 )\
      .info( '\tЗапуск браузера Chromium...\n' )

  if T.fail[0] != 0:
    print( T.fail[0], flush=True )

  print( 0, flush=True )
  return

def testSupportMultimediaFiles():

  rmpConfig = os.path.expanduser('~/.config/rosamp')
  rmpConfigOld = None

  if os.path.exists(rmpConfig):
    rmpConfigOld = os.path.expanduser('~/.config/rosamp.bak')

    if os.path.exists(rmpConfigOld):
      shutil.rmtree(rmpConfigOld)
    os.rename(rmpConfig, rmpConfigOld)

  try:

    formats = { 'mp4', 'mkv', 'mpg', 'wmv', 'avi'}

    for f in formats:

      log.info( '\tПроверка поддержки формата {}... '.format( f ))

      search  = 'Video Play Test' if f=='mpg' else 'bbb'

      mPath = os.path.dirname(os.path.realpath(__file__))
      arg1  = os.path.join(mPath, '..', 'test_data', 'media', '{}.{}'.format( search, f ))

      with Terminal( stream=StreamToLog( log ) ) as T:
        T.error('\tВо время тестирования медиа-файлов произошла ошибка [{code}]: {msg}')

        T.add( '{} "Video Play Test" -t 7000'.format( tool ), separated=True, quiet=True )\
          .info( '\tОжидание окна "Video Play Test" в течение 7 секунд...')
        T.add( 'rosa-media-player "{}"'.format( arg1 ), separated=True, quiet=True )\
          .info( '\tЗапуск медиа: {}...'.format( arg1 ) )
        T.add('pkill rosa-media-player', ignore_errors=True, pause_before=1 )

      if T.fail[0]:
        print( T.fail[0] )
        return
      else:
        log.info( '\tOK')

    with Terminal( stream=StreamToLog( log ) ) as T:
      T.error( '\nВо время тестирования произошла ошибка [{code}]:\n\t{msg}' )

      T.add( 'pulseaudio --start')\
        .info( '\tЗапуск звукового сервера Pulseaudio...')
      T.add( 'audacious -q /usr/share/rosa-test-suite/test_data/media/LvB_s5.mp3', separated=True, quiet=True )\
        .info( '\tЗапуск Audacious c файлом LvB_s5.mp3...')
      T.add( '{} "Audio Play Test" -r -t 7000'.format( tool ), pause_before=5)\
        .info( '\tПроверка поддержки формата mp3... ')
      T.add( 'pulseaudio -k' )\
        .info( '\tОстановка звукового сервера Pulseaudio...')

      if T.fail[0]:
        print( T.fail[0] )
        return
      else:
        print(0)
        return

  finally:

    time.sleep(1)
    shutil.rmtree(rmpConfig)

    if rmpConfigOld:
      os.rename(rmpConfigOld, rmpConfig)

def testSupportGraphicFiles(tmpdir):

  tmpdir = Path( tmpdir )
  images = Path( tmpdir.as_posix() + '/images' )
  src    = Path( test_data + '/images' )

  shutil.copytree( src.as_posix(), images.as_posix() )

  u = User()
  chownR( images.as_posix(), u.name, u.name  )
  chmodR( images.as_posix(), '777' )

  cmd_flip    = tool + ' "{window}" -t 20000 -k Control+Shift+Right'
  cmd_inverse = tool + ' "{window}" -t 20000 -k Control+I'
  cmd_save    = tool + ' "{window}" -t 20000 -k Control+S -x'
  cmd_undo    = tool + ' "{window}" -t 20000 -k Control+Z'
  cmd_enter   = tool + ' "{window}" -t 20000 -k Return'
  cmd_close   = tool + ' "{window}" -r -t 20000'

  for i in images.iterdir():
    F = i.as_posix()
    N = i.name

    waitProcess( name='xtool', timeout=5, kill=True, stream=None )

    with Terminal( stream=StreamToLog( log ) ) as T:
      T.error( 'Во время тестирования произошла ошибка [{code}]: {msg}' )

      T.add( 'kolourpaint {}'.format( F ), separated=True, ignore_output=True )\
       .info( '\tЗапуск графического редактора с файлом {}.'.format( F ) )

      T.add( 'sync; identify -format "#OLD %wx%h %[mean]" {}'.format( F ) )\
       .info( '\tСчитывание информации о {}...\n'.format( F ) )

      T.add( cmd_flip.format( window=N ), pause_before=1 )\
       .info( '\tПоворот изображения...\n' )
      T.add( cmd_inverse.format( window=N ), pause_before=1 )\
       .info( '\tИнвертирование цветов изображения...\n' )

      if N.endswith( '.gif' ):
        T.add( cmd_undo.format( window=N ) )
        T.add( cmd_undo.format( window=N ) )\
         .info( '\tОтмена изменений для gif...\n' )
        T.add( cmd_close.format( window=N ) )\
         .info( '\tЗакрытие графического редактора...\n' )

      else:
        T.add( cmd_save.format( window=N ) )\
         .info( '\tСохранение изменений...\n' )

        if N.endswith( '.jpeg' ):
          T.add( cmd_close.format( window='Предупреждение' ), ignore_errors=True, separated=True )\
           .info( '\tДиалог: "Предупреждение об изменении файла" - OK...\n' )
          #T.add( cmd_close.format( window='Ошибка'), ignore_errors=True ) # python random artefact
          #T.add( cmd_close.format( window='Ошибка'), ignore_errors=True, pause_before=3 )
          T.add( cmd_enter.format( window='Сохранение' ), pause_before=3 )
          T.add( cmd_enter.format( window='Формат' ), pause_before=2 )\
           .info( '\tДиалог: "Формат с потерей качества" - ОК...\n' )
          T.add( cmd_close.format( window=N ) )\
           .info( '\tЗакрытие графического редактора...\n' )
          T.add( cmd_close.format( window=N ), ignore_errors=True, pause_before=2 )\
           .info( '\tЗакрытие графического редактора...\n' )

        T.add( 'sync; identify -format "#NOW %wx%h %[mean]" {}'.format( F ), pause_before=2 )\
         .info( '\tСчитывание информации сохраненного изображения...\n' )

    error      = T.fail[0]
    params_old = [0,0]
    params_now = [0,0]

    for l in T.log:
      msg = l[1]
      if '#OLD' in msg:
        params_old = msg.split()[1:]
      if '#NOW' in msg:
        params_now = msg.split()[1:]

    if params_old[0] == params_now[0]:
      log.info( 'Изображение не было развернуто! Было разрешение: {} Стало: {} '\
                .format( params_old[0], params_now[0] ) )
      error += 1

    if params_old[1] == params_now[1]:
      log.info( 'Цвета изображения не были изменены! Был mean: {} Стал: {}'\
                .format( params_old[1], params_now[1] ) )
      error += 1

    if error != 0:
      #print( error, flush=True )
      return error
    else:
      log.info( '\tПроверка {} пройдена.'.format( N ) )

  #print( 0, flush=True )

  return 0

def testSupportEmail():

  with Terminal( stream=StreamToLog( log ) ) as T:
    T.error( 'Во время тестирования произошла ошибка [{code}]: {msg}' )

    T.add( 'thunderbird -file /usr/share/rosa-test-suite/test_data/web/test.eml', separated=True, ignore_output=True )\
      .info( '\tОткрытие электронного письма в почтовой программе...\n' )
    T.add( '{} "Мастер" -r -k Escape -t 30000'.format( tool ))
    T.add( '{} "Мастер" -r -k Escape -t 30000'.format( tool ), pause_before=3 )
    T.add( '{} "Test message" -r -k control+q -t 30000'.format( tool ), pause_before=3 )
    T.add( '{} "Test message" -r -k control+q -t 5000'.format( tool ), ignore_errors=True )\
      .info( '\tПроверка почтовой программы...\n' )

  if T.fail[0] != 0:
      print( T.fail[0], flush=True )
      return

  print( 0, flush=True )
  return

def testSupportCalendar():

    log.info( '\tОткрытие календаря...' )

    with Terminal( stream=StreamToLog( log ) ) as T:
      T.error( 'Во время тестирования произошла ошибка [{code}]: {msg}\n' )

      T.add( 'thunderbird', separated=True, quiet=True )\
       .info( '\tЗапуск thunderbird с режимом календаря...\n' )
      T.add( '{} "Настройка" -k Escape -t 10000'.format( tool ), ignore_errors=True )
      T.add( '{} "Дом" -k control+shift+c -t 15000'.format( tool ) )\
       .info( '\tПереключение на календарь...\n' )
      T.add( '{} "Календарь" -t 15000'.format( tool ) )\
       .info( '\tПроверка календаря...\n' )
      T.add( '{} "Настройка" -r -k Escape -t 5000'.format( tool ), ignore_errors=True )\
       .info( '\tЗакрытие мастера настройки(если требуется)...\n' )

    if T.fail[0] != 0:
        print( T.fail[0], flush=True )
        return

    print( 0, flush=True )
    return

def testSupportWeb():

    browser = 'chromium-browser'

    with Terminal( stream=StreamToLog(log) ) as T:
      T.error( 'Во время тестирования произошла ошибка [{code}]: {msg}\n' )

      T.add( '{} "http://localhost:50080/"'.format( browser ), separated=True, quiet=True )\
        .info( '\tОткрытие HTTP-ссылки в браузере...\n' )

      T.add( '{} "TEST PASSED" -t 25000'.format( tool ), pause_before=1 )\
        .info('\tПроверка браузера...\n')

      T.add( '{} --ignore-certificate-errors "https://localhost:50443/"'.format( browser ), separated=True, quiet=True )\
        .info( '\tОткрытие HTTPS-ссылки в браузере...' )

      T.add( '{} "HTTPS Test Page" -t 25000'.format( tool ), pause_before=1)\
        .info('\tПроверка браузера...\n')

    if T.fail[0] != 0:
      print( T.fail[0], flush=True )
      return

    print( 0, flush=True )
    return

if __name__ == '__main__' : pass
