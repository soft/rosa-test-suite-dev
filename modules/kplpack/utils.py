#!/usr/bin/python3.5
# -*- coding: utf-8 -*-
# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@:::::::::@@@@@@@@@@@::::::@@@@@@@@@@@::::@@@@@@@@@@@@@:@@@@@@@@@   │
# │   @@@:------------::@@@@:----------::@@@@:--------::@@@@@@@:---:@@@@@@@   │
# │   @@@:---@@@@@@@@:--:@@---:@@@@@@@---:@@---:@@@@@:@@@@@@@@:-----@@@@@@@   │
# │   @@@:---@@@@@@@@:--:@:--:@@@@@@@@:---@@---:@@@@@@@@@@@@@@--:@:--@@@@@@   │
# │   @@@:---@@@@@@@:---@@:--:@@@@@@@@:---@@@:------:::@@@@@@---@@@--:@@@@@   │
# │   @@@:-----------::@@@:--:@@@@@@@@:---@@@@@@:::::---@@@@:--:@@@:--:@@@@   │
# │   @@@:---@@@@@:--:@@@@:--:@@@@@@@@:--:@@@@@@@@@@@:--:@@:-----------:@@@   │
# │   @@@:---@@@@@@:---:@@@:--:::@@@::--:@@::-:::@@::---@@:--:@@@@@@@---:@@   │
# │   @@@:::-@@@@@@@@::::@@@@:::-----::@@@@@@:::----::@@@::::@@@@@@@@:::-@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │                                                                           │
# │                  Additional tools for ROSA Test Suite                     │
# │                                                                           │
# │   Copyright © 2020 LLC "NTC IT ROSA"                                      │
# │   License: GPLv3                                                          │
# │   Authors:                                                                │
# │       Michael Mosolov     <m.mosolov@rosalinux.ru>       2020             │
# │                                                                           │
# │   This program is free software; you can redistribute it and/or modify    │
# │   it under the terms of the GNU General Public License as published by    │
# │   the Free Software Foundation; either version 3, or (at your option)     │
# │   any later version.                                                      │
# │                                                                           │
# │   This program is distributed in the hope that it will be useful,         │
# │   but WITHOUT ANY WARRANTY; without even the implied warranty of          │
# │   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            │
# │   GNU General Public License for more details                             │
# │                                                                           │
# │   You should have received a copy of the GNU General Public License       │
# │   License along with this program; if not, write to the                   │
# │   Free Software Foundation, Inc.,                                         │
# │   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.           │
# │                                                                           │
# ╘═══════════════════════════════════════════════════════════════════════════╛

import crypt
import filecmp
import grp
import hashlib
import logging
import os
import pwd
import dbus
import shutil
import signal
import socket
import sys
import tempfile
import time
import traceback
from typing import Union
from datetime import datetime
from enum import Enum
from mmap import MAP_SHARED, mmap
from multiprocessing import Lock as MLock
from multiprocessing import Manager, Process
from multiprocessing.synchronize import Lock as classLock
from pathlib import Path
from subprocess import PIPE, STDOUT, Popen, call, check_output
from threading import Event
from threading import Lock as TLock
from threading import Thread
from time import sleep

import psutil
import selinux

OK  = 0
BAD = 1

class ConRun:
  """
    WARNING:
      !!! not recommended for use with important users(uid, gid) !!!

      using "with-statement" is not safe. If this fails, use the following construction:
      cr = ConRun(args..)
      if cr.start():
        somecode
      cr.stop()

    If 'output' is specified, then stdout will be written to it(list of strings),
    otherwise stdout will be printed to the main process
  """

  class OKException(Exception):
    pass

  def __init__( self,
                uid=500,
                gid=500,
                raw_context="user_u:user_r:user_t:s0-s3:c0.c1023",
                memory_bytes=1024,
                output:list = [],
                wait=True,
                stdout=None,
                stdin=None,
                stderr=None  ):

    self.__con    = raw_context
    self.__uid    = uid
    self.__gid    = gid
    self.__pid    = -1
    self.__mem    = mmap(-1, memory_bytes, flags=MAP_SHARED)
    self.__out    = output
    self.__stdout = stdout
    self.__stdin  = stdin
    self.__stderr = stderr
    self.__rundir = '/run/user/{}'.format( self.__uid )
    self.__btrace = sys.gettrace()

    self.__STP    = True # do NOT touch this!
    self.__WAIT   = wait

    self.__user   = User( uid=self.__uid )

    if User().uid == self.__uid or os.getuid() == self.__uid:
      self.__PROT = True
    else:
      self.__PROT = False

  def write_bytes(self, data:bytes):

    if ( self.__mem.tell() + sys.getsizeof( data ) ) > self.__mem.rfind (b'\0' ):
      raise Exception('Not enough memory to write bytes')
    else:
      self.__mem.write( data + b'\n' )
      self.__mem.flush()

  def read_bytes(self) -> list:

    strings = list()
    self.__mem.seek(0)

    for line in iter( self.__mem.readline, b'' ):
      if b'\n' in line:
        strings.append( line.decode('utf-8').strip('\n') )

    return strings

  def start(self):

    self.__setdirs()
    self.__pid = os.fork()

    if self.__pid == 0:
      self.__detach()
      self.__setenv()
      return True
    else:
      return False

  def stop(self):

    if self.__pid == 0:
      os._exit(0) # need to think about it
    else:
      os.waitpid(self.__pid, 0)

      self.__setdirs(unset=True)

      if not self.__PROT:
        x( 'pkill -u {}'.format( self.__uid ) )

      return True

  def __setenv(self):

    os.environ['HOME'] = self.__user.home
    os.environ['XDG_RUNTIME_DIR'] = '/run/user/{}'.format( self.__uid )
    os.environ['DISPLAY'] = ':0'
    os.environ['XAUTHORITY'] = '{}/.Xauthority'.format( self.__user.home )
    os.environ['DBUS_SESSION_BUS_ADDRESS'] = ''

    os.environ['PWD']           = os.environ['HOME']
    os.environ['USER']          = self.__user.name
    os.environ['USERNAME']      = self.__user.name
    os.environ['LOGNAME']       = self.__user.name
    os.environ['MAIL']          = 'MAIL=/var/spool/mail/' + self.__user.name
    os.environ['ENV']           = '/{}/.bashrc'.format( self.__user.name )
    os.environ['XDG_DATA_DIRS'] = '{}/.local/share/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share:/usr/share'\
                                  .format( self.__user.home )
    os.environ['SCREENDIR']     = self.__user.home + '/tmp'

    if 'XDG_SESSION_ID' in os.environ.keys():
      del os.environ['XDG_SESSION_ID']

  def __setdirs(self, unset=False):

    #create /var/user/run/xxx dir
      ex = os.path.exists( self.__rundir )
      if not self.__STP and unset and ex:

        try:
          #unmount gvfs from /run/user/xxx
          dir_gvfs = '/run/user/{}/gvfs'.format( self.__uid ) #ROSA NICKEL path
          if os.path.exists( dir_gvfs ):
            if x( 'unmount {}'.format( dir_gvfs ) ):
              print( 'ConRun: unmount {} failed! Unmount and delete manually.'.format( dir_gvfs ) )
          shutil.rmtree( self.__rundir )
        except IsADirectoryError as e:
          pass

      if not ex and not unset:
        os.mkdir( self.__rundir )
        os.chown( self.__rundir, self.__uid, self.__gid )
        chmodR( self.__rundir, '0700' )
        self.__STP = False

    #get x-server cookie [ no need to unset ]
      if not self.__PROT:
        cu_xauth_file = '{}/.Xauthority'.format( User().home )
        nu_xauth_file = '{}/.Xauthority'.format( self.__user.home )

        if os.path.exists( nu_xauth_file ):
          if not filecmp.cmp( cu_xauth_file, nu_xauth_file, shallow=False ):
            shutil.copyfile( cu_xauth_file, nu_xauth_file )
        else:
          shutil.copyfile( cu_xauth_file, nu_xauth_file )

  def __detach(self):
    """ use in forked process """

    with open( '/proc/self/loginuid', 'w') as f:
      f.write( str( self.__uid ) )

    selinux.setexeccon_raw( self.__con )
    os.setgid( self.__gid )
    os.setgroups( [self.__gid] )
    os.setegid(self.__gid)
    os.setuid( self.__uid )
    os.seteuid( self.__uid )

    #print(x('id',return_type=Return.stdout), os.geteuid(), os.getegid() )

    # change work dir & file permissions mask
    os.chdir( self.__user.home )
    os.umask(0)
    os.setsid()

    # clear I/O buffers
    sys.stdout.flush()
    sys.stderr.flush()

    # collect all output
    if self.__stdin is None:
      with open(os.devnull, 'rb', 0) as devnull:
        os.dup2( devnull.fileno(), sys.stdin.fileno() )

    if self.__stdout is None:
      with open(os.devnull, 'ab', 0) as devnull:
        os.dup2( devnull.fileno(), sys.stdout.fileno() )
      sys.stdout = StreamToMem(self)

    if self.__stderr is None:
      with open(os.devnull, 'ab', 0) as devnull:
        os.dup2( devnull.fileno(), sys.stderr.fileno() )


    # write pid
    # pidpath = f'/run/user/{self.__uid}/ConRun_{self.__uid}_{self.__gid}.pid'

    # with open( pidpath, 'w') as pidfile:
    #   print( os.getpid(), file=pidfile )

    #atexit.register( lambda: os.remove(pidpath) )

    # signal handler for termination
    def sigterm_handler(signo, frame):

      print('ConRun: fork get SIGTERM.')
      #os.remove( pidpath )
      raise SystemExit(1)

    signal.signal( signal.SIGTERM, sigterm_handler )

  def __enter__(self):

    self.__setdirs()
    self.__pid = os.fork()

    if self.__pid == 0:

      self.__detach()
      self.__setenv()

    else:
      self.write_bytes( str( self.__pid ).encode() )
      if self.__WAIT:
        os.waitpid( self.__pid, 0 ) #output: status is: (40899, 0)

      # hack or raise self.OKException('normal situation')
      sys.settrace( lambda *args, **keys: None )
      frame = sys._getframe(1)
      frame.f_trace = self.__trace

    return self.__out

  def __exit__(self, clss, value, trace):

    if self.__pid == 0:

      if clss:
        print( 'Exception: {} - {}; {}'.format( clss.__name__, value, traceback.format_exc() ) )
      os._exit(0)

    else:

      if clss is None:
        return

      if clss.__name__ == 'OKException':
        sys.settrace( self.__btrace )
        self.__setdirs(unset=True)

        if not self.__PROT:
          x( 'pkill -u {}'.format( self.__uid ) )

        if len(self.__out) > 0 and self.__stdout:
          for l in self.read_bytes():
            print( l )
        else:
          self.__out[:] = self.read_bytes()

        return True

      else: #another Exceptions
        print( clss.__name__, value, trace )
        return False

  def __trace(self, frame, event, arg):
    raise self.OKException('normal situation')

class StreamToLog(object):
  """ Fake file-like stream object that redirects writes to a logger instance.
      example: sys.stderr = StreamToLogger(logger, logging.DEBUG)
  """
  def __init__(self, logger, log_level=logging.INFO):

    self.logger    = logger
    self.log_level = log_level
    self.linebuf   = ''

    if logger.handlers:
      with open( logger.handlers[0].baseFilename, 'r') as f:
        self.fileno  = f.fileno()

  def write(self, buf):

    for line in buf.rstrip().splitlines():

      if 'Gtk-WARNING' in line:
        continue
      else:
        self.logger.log(self.log_level, line.rstrip())

  def flush(self):
    pass

class StreamToMem(object):

  def __init__(self, mem_file):

    self.mem     = mem_file
    self.linebuf = ''

  def write(self, buf):

    for line in buf.splitlines():

      if 'Gtk-WARNING' in line:
        continue
      else:
        self.mem.write_bytes(line.encode())

  def flush(self):
    pass

class StreamToNull(object):

  def write(self, *nothing):
    pass

  def flush(self):
    pass

class Damper:

  def __init__(self, stream = sys.stderr):
    """ Suppresses any output in %stream% until the end of with-block
    """

    if hasattr(stream, 'fileno') and hasattr(stream, 'flush'):
      self.stream        = stream
      self.stream.flush()
      self.stream_file   = self.stream.fileno()
      self.stream_backup = os.dup( self.stream_file )

  def __enter__(self):

    try:
      self.devnull = os.open( os.devnull, os.O_WRONLY )
      os.dup2( self.devnull, self.stream_file)
    finally:
      os.close( self.devnull )

  def __exit__(self, type, value, traceback):

    os.dup2(  self.stream_backup, self.stream_file )
    os.close( self.stream_backup )

    if value:
      raise Exception(value)

class Return(Enum):

  stdout  = 1
  codeout = 2
  errcode = 3
  process = 4
  ignore  = 5

class Command:

  def __init__(self):

    self.line          = ''
    self.info          = ''
    self.error         = ''
    self.pause_before  = 0
    self.ignore_output = False
    self.ignore_errors = False
    self.quiet         = False
    self.separated     = False
    self.timeout       = 0

class Terminal:
  """ This class allows you to organize the sequential execution
      of commands as if they were typed in the terminal.
  """

  class TerminalException(Exception):
    pass

  def __init__(self, stream = sys.stdout, flush=True):

    self.__commands = []

    self.__subjects = []
    self.__lock     = MLock()
    self.__manager  = Manager()

    self.__stream   = stream
    self.__flush    = flush
    self.__err_msg  = ''

    self.log        = self.__manager.list( [[0, 'Log start']] )
    self.fail       = [ 0, 'No errors' ]

  def __str__(self):

    mxlinesize = 1

    for c in self.__commands:
      mxlinesize = max(mxlinesize, len(c.line) )

    out =  'Commands'.center( mxlinesize+7, '─' )
    out += 'Separated'.center(12, '─')
    out += '\n'

    def makel(cmd):

      l = ''
      l += (cmd.line + ' ').ljust(mxlinesize+6,'.')

      l += '|' + ( '+'   if cmd.separated else '-').center(11)  # thread col
      l += '\n'
      return l

    for c in self.__commands:
      out += makel(c)

    return out

  def __enter__(self):
    return self

  def __exit__(self, type, value, traceback):

    self.run()

    if not self.ok():
      print(self.fail[1], file=self.__stream)

  def __soft_exit(self):

    for t in self.__subjects:
      t.join()

  def __hard_exit(self):

    for t in self.__subjects:
      if t.join(timeout=1.5) == None:
        t.terminate()

  def add(self, command='', separated=False, ignore_output=False, ignore_errors=False, quiet=False, pause_before=0, timeout=0):
    """ Adds a command to the pool of commands that will be run sequentially.

        Parameters:
        command       (str)    - the command to be executed;
        separated     (bool)   - execute command in a separate process;
        ignore_output (bool)   - all output will be directed to /dev/null;
        ignore_errors (bool)   - in any case, the command will return 0;
        quiet         (bool)   - command output will not be processed;
        pause_before  (float)  - pause before executing this command (seconds);
        timeout       (float)  - time allotted for command execution. if the time runs out, the command will return an error;

        Returns:
        (self)
    """
    if command:
      cmd               = Command()
      cmd.line          = command
      cmd.separated     = separated
      cmd.ignore_output = ignore_output
      cmd.ignore_errors = ignore_errors
      cmd.quiet         = quiet
      cmd.pause_before  = pause_before
      cmd.timeout       = timeout

      self.__commands.append( cmd )
      return self

  def ok(self, right_code=0):
    """ Returns true if everything is OK (== right_code), otherwise fills the last error field
    """

    for l in self.log:

      if l[0] != right_code:

        if not l[1] and not self.__err_msg:
          self.fail = [ l[0], 'Error message not provided.' ]

        elif not self.__err_msg:
          self.fail = [ l[0], l[1] ]

        else:
          self.fail = [ l[0], self.__err_msg.format( code=l[0], msg=l[1] ) ]

        return False

    return True

  def info(self, msg):
    """
      Add description to last command.
      it will be sent to __stream before executing the command
    """

    if msg and self.__commands:
      self.__commands[-1].info = msg
    return self

  def error(self, msg='Error[{code}]: {msg}'):
    """
      Adds a general error message.
      If not specified, raw output is used.
      Example: msg='Message:{msg}, Code{code}'
    """

    if msg:
      self.__err_msg = msg

  def run(self):

    try:
      for cmd in self.__commands:

        if cmd.separated:
          self.__run_one_process( cmd )
        else:
          self.__run_one( cmd )

      self.__soft_exit()

    except:
      self.__hard_exit()

  def __run_one(self, cmd):

    line = cmd.line

    if line:

      if cmd.info:
        print( cmd.info, end='', file=self.__stream )
        if self.__flush:
          self.__stream.flush()

      if cmd.ignore_errors:
        rt = Return.ignore
      elif cmd.quiet:
        rt = Return.errcode
      else:
        rt = Return.process

      result = x( command=line,
                  return_type=rt,
                  ignore_output=cmd.ignore_output,
                  log_and_lock={'log':self.log, 'Lock':self.__lock},
                  pause=cmd.pause_before,
                  timeout=cmd.timeout )

      if not cmd.ignore_errors:
        if not cmd.quiet and result.returncode != OK:
          raise self.TerminalException( 'Terminal error {}: {}'.format( result.returncode, result.stdout.read() ) )
        if cmd.quiet and result != OK:
          raise self.TerminalException( 'Terminal error {}'.format( result.returncode ) )

  def __run_one_process(self, cmd):

    line = cmd.line

    if line:

      if cmd.ignore_errors:
        rt = Return.ignore
      elif cmd.quiet:
        rt = Return.errcode
      else:
        rt = Return.process

      self.__subjects.append(
        Process( target=x, daemon=False,
                 args=( line,
                        rt,
                        cmd.ignore_output,
                        {'log':self.log, 'Lock':self.__lock},
                        cmd.pause_before,
                        cmd.timeout
                      ) ) )

      if cmd.info:
        print( cmd.info, end='', file=self.__stream )
        if self.__flush:
          self.__stream.flush()

      self.__subjects[-1].start()
      time.sleep( 1 )

  def print_log(self):

    print('----------log-----------\n', end=' ', file=self.__stream)

    for out in self.log:
      print('{:4} | {}\n'.format( out[0], out[1] ), end=' ', file=self.__stream)

  def clear(self):

    self.__hard_exit()

    del self.__commands[:]
    del self.__subjects[:]
    del self.log[:]
    del self.fail[:]

    self.__err_msg   = ''

class Ticker(object):
  """ Timer. It starts immediately after initialization.

      Parameters:
      timeout         (int, float) - sets timeout in seconds or fractions of seconds
      high_resolution (bool)       - use performance counter with highest available resolution

  """

  tmfunc  = time.time
  timeout = 10

  def __init__(self, timeout=10, high_resolution=False):

    if high_resolution:
      self.tmfunc = time.perf_counter

    self.timeout = timeout
    self.times   = []

    self.start()

  def start(self):

    self.times.clear()
    self.times.append( [ self.tmfunc(), 'start point' ] )

  def finish(self):
    self.times.pop(0)

  def tick(self) -> bool:

    delta = self.tmfunc() - self.times[0][0]

    if delta < self.timeout:
      return True

    else:
      self.finish()
      return False

  def tock(self) -> float:
    return self.tmfunc() - self.times[0][0]

  def check(self, comment=''):

    if comment:
      self.times.append( [ self.tock(), comment ] )
    else:
      self.times.append( self.tock() )

  def __enter__(self):

    self.start()
    return self.times

  def __exit__(self, type, value, trace):

    self.check()
    self.finish()

class ConPlace(object):
  """ Clears console output after with-block exit
  """

  CURSOR_UP     = '\x1b[1A'
  ERASE_LINE    = '\x1b[K'
  CLEAR_TO_END  = '\x1b[0J'

  __SAVE_CURSOR   = '\x1b[s'
  __LOAD_CURSOR   = '\x1b[u'

  def __init__(self, stream=sys.stdout, end_pause=1.0):
    """ end_pause - make pause after with-block complete, seconds
    """

    self.stream = stream
    self.pause  = end_pause

  def do(self, action):
    """ action - one of fields ConPlace-class
        Example: do( ConPlace.CURSOR_UP )
    """

    if action:
      self.stream.write( action )

  def __enter__(self):

    self.stream.write( self.__SAVE_CURSOR )
    self.stream.write( '\n' )
    return self

  def __exit__(self, type, value, trace):

    time.sleep( self.pause )
    self.stream.write( self.ERASE_LINE )
    self.stream.write( self.__LOAD_CURSOR )
    self.stream.write( self.CLEAR_TO_END )
    self.stream.flush()
    pass

class Indicator(object):
  """ Threaded single line indicator.
      This class allows you to create a text indicator of the progress in the with-block.
  """

  __SAVE_CURSOR = '\x1b[s'
  __LOAD_CURSOR = '\x1b[u'
  __CLEAR_TO_END  = '\x1b[0J'

  __indicator = [ '[   ]','[.  ]', '[.. ]', '[...]', '[ ..]', '[  .]']
  __status    = 0
  __border    = len( __indicator )
  __interval  = 0.1
  __message   = ' Please wait for the process to complete...'
  __suffix    = ''

  __stream  = None
  __thread  = None
  __lock    = TLock()
  __exit    = Event()

  def __init__(self, message='', suffix='', stream=sys.stdout, interval_sec=0.1 ):
    """ Creates a textual progress bar.

        Parameters:
        message      (str)    - message displayed after the indicator;
        suffix       (str)    - variable part of the message used to display additional information,
                                if the suffix contains {elaps}, the elapsed time will be substituted;
        stream       (TextIO) - output file. e.g. sys.stdout;
        interval_sec (float)  - indicator drawing frequency, seconds;

        Returns:
        (self)
    """
    self.__interval = interval_sec
    self.__stream   = stream or sys.stdout
    self.__thread   = Thread( target=self.__shift, args=(), daemon=False )
    self.__message  = message or self.__message
    self.__elapsed  = False
    self.__started  = datetime.now()
    self.__suffix   = suffix

    self.__check_elaps( suffix )

  def __shift(self):

    while not self.__exit.wait( self.__interval ):

      self.__status = ( self.__status + 1 ) % self.__border

      if self.__elapsed:
        now = str( datetime.now() - self.__started )
        self.__stream.write( ' ' + self.__indicator[ self.__status ] + self.__message + self.__suffix.format( elaps=now ) + '\r' )
      else:
        self.__stream.write( ' ' + self.__indicator[ self.__status ] + self.__message + self.__suffix + '\r' )

      self.__stream.flush()

    # cleaning
    self.__stream.write( ' ' * ( len( self.__indicator[0] ) + len( self.__message ) + 1 )  )
    self.__stream.write( '\r' )
    self.__stream.flush()

  def __check_elaps(self, suffix:str=''):

    if '{elaps}' in suffix:
      self.__elapsed = True
    else:
      self.__elapsed = False

  def set_message(self, text:str):

    with self.__lock:
      self.__message = text

  def set_suffix(self, text:str):
    self.__check_elaps( text )

    with self.__lock:
      self.__suffix = text

  def __enter__(self):

    self.__exit.clear()
    self.__stream.write( self.__SAVE_CURSOR )
    self.__stream.write( '\n' )
    self.__stream.flush()
    self.__thread.start()

    return self

  def __exit__(self, type, value, trace):

    self.__exit.set()
    self.__thread.join(0.1)

    self.__stream.write( self.__LOAD_CURSOR )
    self.__stream.write( self.__CLEAR_TO_END )
    self.__stream.flush()

class Context:
  full = "file_not_found"
  u    = "_"
  r    = "_"
  t    = "_"
  s    = []
  c    = []

class User(object):
  """ This class allows you to create a user
      and delete it after the object is destroyed.
  """

  def __init__(self, name:str='', password:str='', uid:int=-1, home='', se_user:str='', se_range:str='', system=False):

    self.name     = name
    self.password = password
    self.uid      = uid
    self.gid      = -1
    self.gecos    = ''
    self.home     = home
    self.shell    = ''
    self.system   = system
    self.groups   = []

    self.created  = False # if the specified user did not exist
    self.deleted  = False
    self.selected = False # if user exists or current is selected

    # selinux
    self.se_created = False
    self.se_deleted = False

    self.se_user  = se_user
    self.se_range = se_range
    self.context  = None

    if not name and uid < 0:
      self.current()

    elif self.exist():

      if uid >= 0:
        self.updByUid()
      else:
        self.updByName()

      self.updCon()
      self.selected = True

    else:
      self.create()

      if self.se_user and not self.se_exist():
        self.selinux_link()

  def __del__(self):
    self.delete()

  def __str__(self):

    out = ''
    out += 'name:    ' + self.name        + '\n'
    out += 'pass:    ' + self.password    + '\n'
    out += 'uid:     ' + str(self.uid)    + '\n'
    out += 'gid:     ' + str(self.gid)    + '\n'
    out += 'gecos:   ' + self.gecos       + '\n'
    out += 'home:    ' + self.home        + '\n'
    out += 'shell:   ' + self.shell       + '\n'
    out += 'system:  ' + str(self.system) + '\n'

    if self.se_user:
      out += 'selinux: ' + self.context.full + '\n'

    return out

  def create(self) -> bool:

    if not self.created and not self.deleted:

      flags = ''

      if self.system:
        flags += ' --system'

      if self.password:
        encrypted_password = crypt.crypt( self.password, '22' )
        flags += ' --password {}'.format( encrypted_password )

      if self.home:
        flags += ' -d {}'.format( self.home )

      else:
        flags += ' -M'

      code, res = x( 'useradd{} {}'.format( flags, self.name ), Return.codeout )

      if code != 0:
        return False

      self.updByName()
      self.updCon()
      self.created = True
      return True

  def delete(self, force=False) -> bool:

    if (self.name and self.created and not self.deleted) or force :
      res = x( 'userdel -f --remove{} {}'.format( ' -Z' if self.se_user else '', self.name ), Return.stdout )

      self.deleted = True
      return True

    return False

  def exist(self) -> bool:

    names = [ struct.pw_name for struct in pwd.getpwall()]
    uids  = [ struct.pw_uid  for struct in pwd.getpwall()]

    return self.name in names or self.uid in uids

  def se_exist(self) -> bool:

    out = x( 'semanage login -l', Return.stdout )

    for l in out.splitlines():
      if l and self.name in l.strip().split()[0]:
        return True

    return False

  def updByUid(self):

    users  = [ struct for struct in pwd.getpwall()]

    for u in users:
      if u[2] == self.uid:
        self.name, self.password, self.uid, self.gid, self.gecos, self.home, self.shell = u

  def updByName(self):

    users  = [ struct for struct in pwd.getpwall()]

    for u in users:
      if u[0] == self.name:
        self.name, self.password, self.uid, self.gid, self.gecos, self.home, self.shell = u

  def current(self) -> bool:

    code, out = x( 'logname', Return.codeout )

    if code != 0:
      return False

    self.name = out.strip()
    self.updByName()
    self.updCon()
    self.selected = True
    return True

  def updCon(self):

    self.context = Context()

    usr = 'user_u'
    rng = 's0'

    if not self.se_exist() and self.se_user:
      usr = self.se_user
      rng = self.se_range

    elif self.se_exist():
      usr = selinux.getseuserbyname( self.name )[1]
      rng = selinux.getseuserbyname( self.name )[2]

    self.context.full = '{}:{}:{}:{}'.format( usr, 'unknown_r', 'unknown_t', rng  )
    self.context.u    = usr
    self.context.r    = 'unknown_r'
    self.context.t    = 'unknown_t'
    self.context.s    = rangeGetFromCon( self.context.full, mcs=False )

    if len( rng.split(':') ) == 2:
      self.context.c = rangeGetFromCon( self.context.full, mcs=True  )

  def addGroup(self, group:str) -> bool:

    if (self.created or self.selected) and not self.deleted:
      code, res = x( 'usermod -a -G {} {}'.format( group, self.name ), Return.codeout )
      if code == 0:
        self.updGroups()
        return True

    return False

  def updGroups(self) -> bool:

    if (self.created or self.selected) and not self.deleted:
      if self.exist():
        code, res = x( 'groups ' + self.name, Return.codeout )

        if code == 0:
          self.groups = res.split(':')[1].split()
          return True

    return False

  @staticmethod
  def selinux_getcon() -> Context:
    """
      fields:
        str:str, u:str, r:str, t:str, s:list, c:list
    """
    context_str = selinux.getcon_raw()[1]
    context_lst = context_str.split(':')

    if len(context_lst) == 5:
      return Context(
                      context_str,
                      context_lst[0],
                      context_lst[1],
                      context_lst[2],
                      rangeGetFromCon( context_str, mcs=False ),
                      rangeGetFromCon( context_str, mcs=True )
                    )

    if len(context_lst) == 4:
      return Context(
                      context_str,
                      context_lst[0],
                      context_lst[1],
                      context_lst[2],
                      rangeGetFromCon( context_str, mcs=False ),
                      None
                    )

  def selinux_link(self) -> bool:

    if not self.se_created and not self.se_deleted:

      code, res = x( 'semanage login --add {} -s {} -r "{}"'.format( self.name, self.se_user, self.se_range ), Return.codeout )

      if code == 0:

        # set the context of all files in the home directory
        code, res = x( 'chcon -R -l "{}" {}'.format( self.se_range, self.home ), Return.codeout )

        if code == 0:
          self.se_created = True
          self.updCon()
          return True

    return False

  def selinux_unlink(self):

    if self.se_created and not self.se_deleted:

      code, res = x( 'semanage login --delete {}'.format( self.name ), Return.codeout )

      if code == 0:
        self.se_deleted = True
        return True

    return False

class SuspendGuard(object):
  """ This class avoids session termination due to inactivity.
      It uses dbus.
  """

  def __init__(self, appname:str, reason:str):

    self.__bus     = dbus.SessionBus()
    self.__proxy   = self.__bus.get_object('org.freedesktop.PowerManagement.Inhibit', '/org/freedesktop/PowerManagement/Inhibit')
    self.__appname = appname
    self.__reason  = reason
    self.__cookie  = -1

    self.__activate()

  def isActive(self) -> bool:
    return bool(self.__proxy.HasInhibit())

  def block(self):

    try:
      while not sleep(1): pass
    except KeyboardInterrupt as e:
      pass

  def __activate(self):
    self.__cookie = self.__proxy.Inhibit( self.__appname, self.__reason )

  def __deactivate(self):

    if self.__cookie > 0:
      self.__proxy.UnInhibit( self.__cookie )

  def __del__(self):
    self.__deactivate()

def x(command, return_type=Return.errcode, ignore_output=False, log_and_lock={'log': [], 'Lock': None}, pause=0, timeout=0):
  """ Python shell runner. This is a "simple" wrapper.
      log_and_lock = { [ [errcode, output], ...], multiprocessing.Lock() }

      example: print( x('echo 123', return_type=Return.stdout) )

      if return_type == Return.codeout then x returns tuple(errcode,stdout\stderr )
      if return_type == Return.stdout  then x returns str
      if return_type == Return.errcode then x returns int
      if return_type == Return.process then x returns object of subrocess.Popen(...)
      if return_type == Return.ignore  then x returns 0

      pause         (float) - make pause before executing, in seconds.
      timeout       (float) - seconds. if timed out, return errcode(1). default: not limited.
      ignore_output (bool)  - adds suffix '> /dev/null' to command.
  """

  suffix    = ''
  waskilled = False
  output    = tempfile.SpooledTemporaryFile( mode='w+', encoding='UTF-8' )

  LnL = log_and_lock if type( log_and_lock['Lock'] ) is classLock else None

  if not command:
    print( 'x: command is empty' )
    return 1

  if ignore_output or return_type == Return.errcode:
    suffix = ' > /dev/null'

  if pause > 0:
    time.sleep(pause)

  process = Popen( command + suffix, shell=True, universal_newlines=True, stdout=output, stderr=STDOUT )
  process.stdout = output

  if timeout > 0:
    timer = Ticker( timeout )
    while timer.tick() and process.poll() == None:
      pass

    if not timer.tick() and process.poll() == None:
      process.kill()
      process.returncode = 1
      waskilled = True
  else:
    process.wait()

  process.stdout.seek(0)
  retcode = process.returncode if not waskilled else 1

  if return_type != Return.errcode:
    if not waskilled:
      retout = process.stdout.read()
      process.stdout.seek(0)
      if LnL:
        retout = retout.replace('\n', ';') # if writing to log
    elif return_type == Return.stdout:
      retout  = 'Timed out or stdout overloaded!'


  if return_type == Return.stdout:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ retcode, retout ] )
    return retout

  if return_type == Return.codeout:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ retcode, retout ] )
    return retcode, retout

  elif return_type == Return.errcode:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ retcode, 'Command: ' + command ] )
    return process.returncode

  elif return_type == Return.process:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ retcode, retout ] )
    return process

  elif return_type == Return.ignore:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ OK, '[ignore errors] {}'.format( retout ) ] )
    return OK

def fileBackup(uri='', restore=False, replacement=''):

  backup = uri + '.bkp'
  path   = ''
  name   = ''

  if restore == False and not os.path.exists( uri ):
    raise Exception('There is nothing to back up. File {} not found!'.format( uri ))

  if replacement and os.path.exists( replacement ):
    path = os.path.split( uri )[0]
    name = os.path.split( replacement )[1]

  elif replacement and not os.path.exists( replacement ):
    raise Exception('Replacement does not exist. File {} not found!'.format( uri ))

  # "some_magic.was".[::-1].replace('was.','',1)[::-1]

  if restore:

    if os.path.exists ( backup ):

      if os.path.exists( uri ):
        os.remove( uri )

      os.rename( backup, uri )

    else:
      raise Exception('Backup of "{}" not found!'.format( backup ))

  else:

    if os.path.exists ( uri ):

      if os.path.exists( backup ):
        raise Exception('Backup file "{}" already exists!'.format( backup ))
      else:
        os.rename( uri, backup )

        if replacement:
          shutil.copy2( replacement, uri)

    else:
      raise Exception('File to save "{}" not found!'.format( uri ) )

  pass

def waitCPU(pid = -1, cpuload = 10, verbose=False, silent=False, stream=sys.stdout, end_pause = 1.0):
  """
    end_pause - seconds
  """

  import functools

  CURSOR_UP     = '\x1b[1A'
  ERASE_LINE    = '\x1b[K'
  SAVE_CURSOR   = '\x1b[s'
  LOAD_CURSOR   = '\x1b[u'
  CLEAR_TO_END  = '\x1b[0J'

  stream.write( SAVE_CURSOR )
  stream.write( '\n' )

  if not silent:
    if pid == 0:
        print( 'waitCPU(): Process with PID:{} not found!'.format( pid ), file=stream )
        return
    elif pid < 0:
        print( 'waitCPU(): Режим общей загрузки ЦП! Ожидание пока загрузка ЦП не станет < {}%.'.format( cpuload ), file=stream )

  p = psutil.Process( os.getpid() if pid < 0 else pid)

  if verbose and not silent:
      print( 'NOW: pid = {}, status = {}, user = {}'.format( p.pid, p.status(), p.username() ), file=stream )
      print( 'CMD line = {}'.format( p.cmdline() ), file=stream )
      print( 'RAM load = {} \n'.format( p.memory_percent() ), file=stream )

  if pid > 0:
    get_cpu_load = functools.partial( p.cpu_percent, 0.1 )
  else:
    get_cpu_load = functools.partial( psutil.cpu_percent, 0.1 )

  load = 100.0
  while ( True ):
    load = get_cpu_load()
    if not silent:
      print( 'Загрузка ЦП = {}%, ожидание...'.format( load ), end='\r', file=stream)

    if ( load < cpuload ):
      break

  if not silent:
    print( 'Загрузка ЦП = {}%, продолжение...'.format( load ), end='', file=stream, flush=True)
    time.sleep(1)

  stream.write( ERASE_LINE )
  stream.write( LOAD_CURSOR )
  stream.write( CLEAR_TO_END )
  stream.flush()
  time.sleep( end_pause )

  pass

def waitProcess(pid=-1, name='', stream=sys.stdout, end_pause=1.0, timeout=10, kill=False):
  """
    end_pause - seconds
    timeout   - seconds
    kill      - kill process after timeout

    Check if process with pid or name exists.

    return 1 - process exceeded timeout
    return 2 - process killed by timeout
    return 3 - process not found
    return 0 - process terminate self
  """

  indicator = [ '[   ]','[.  ]', '[.. ]', '[...]', '[ ..]', '[  .]']
  shift = len( indicator )
  i = 0
  process = None
  stopped = False

  t = Ticker( timeout=timeout )

  if stream == None:
    stream = StreamToNull()

  with ConPlace( end_pause=end_pause ) as CP:

    for proc in psutil.process_iter():
      try:
        if name:
          if name.lower() in proc.name().lower():
            process = proc
        else:
          if proc.pid == pid:
            name = str( pid )
            process = proc

      except ( psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess ) as e:
        pass
      else:
        pass

    if process:
      while t.tick():
        try:
          if process.is_running():
            print( ' {} Ожидание завершения процесса {}...'.format( indicator[i], name ), end='\r', file=stream, flush=True )
            i = (i + 1) % shift
            if kill and not t.tick():
              print( ' Процесс {} уничтожен!'.format( process.name() ), end='\r', flush=True, file=stream )
              process.kill()
              return 2
          else:
            CP.do( CP.ERASE_LINE )
            print( 'Процесс завершен.', flush=True, file=stream )
            return 0
        except ( psutil.ZombieProcess ):
          pass # mb late
        time.sleep(0.2)
      else:
        return 1 #timeout
    print( 'Процесс не найден! ', file=stream )
    return 3

def waitForPrinterIdle(check_interval = 0.1, waiting_timeout_sec = 10):
    """
        return True if any printer is busy
    """
    tick = time.time()

    with ConPlace():
      while (True):

        time.sleep(check_interval)
        out = check_output([ 'lpstat', '-o' ])
        tock = time.time()

        print('Waiting for printer..{}/{} sec\r'.format( int (tock-tick), waiting_timeout_sec), end=' ', file=sys.stdout)

        if len(out) <= 0:
            return True
        if tock - tick > waiting_timeout_sec:
            raise ValueError('Printer timed out!')

    # print 'waitForPrinterIdle: lpstat out: {}, return: {}'.format(out, len(out) > 0)


    return False

def backup_mozilla(thunderbird = False):

  time.sleep(5)
  mPath = os.path.dirname(os.path.realpath(__file__))

  if (not thunderbird):
      webDir    = os.path.join(mPath, '..', '..', 'test_data', 'web')
      ConfigNew = os.path.join(webDir, 'config.firefox')
      # nickel: "./mozilla/firefox" RED: "./mozilla"
      Config    = os.path.expanduser('~/.mozilla/firefox')
      ConfigOld = os.path.expanduser('~/.mozilla.bak')
  else:
      webDir    = os.path.join(mPath, '..', '..', 'test_data', 'web')
      ConfigNew = os.path.join(webDir, 'config.thunderbird')
      Config    = os.path.expanduser('~/.thunderbird')
      ConfigOld = os.path.expanduser('~/.thunderbird.bak')

  if not os.path.exists(Config):
      #os.makedirs(Config)
      shutil.copytree(ConfigNew, Config)

  elif not os.path.exists(ConfigOld):
      os.rename(Config, ConfigOld)
      backup_mozilla(thunderbird) # sorry for that

  else:
      shutil.rmtree(Config)
      os.rename(ConfigOld, Config)

def clone_mozilla_profile(delete = False, thunderbird = False):
  """
  return absolute path to test.profile for firefox
  """
  this        = os.path.dirname(os.path.realpath(__file__))

  if not thunderbird:
      profile     = os.path.join(this, '..', '..' 'test_data', 'web', 'config.firefox', 'test.profile')
      destination = os.path.expanduser('~/.mozilla/firefox')
      tmp_profile = os.path.join(destination,'test.profile')
  else:
      profile     = os.path.join(this, '..', '..', 'test_data', 'web', 'config.thunderbird', 'test.profile')
      destination = os.path.expanduser('~/.thunderbird')
      tmp_profile = os.path.join(destination,'test.profile')

  if not os.path.exists(tmp_profile):
      print('creating {}'.format(tmp_profile), file=sys.stdout)
      shutil.copytree(profile, tmp_profile)

  elif os.path.exists(tmp_profile) and not delete:
      print('recreating {}'.format(tmp_profile), file=sys.stdout)
      clone_mozilla_profile(True, thunderbird)
      clone_mozilla_profile(False, thunderbird)

  if delete:
      print('removing {}'.format(tmp_profile), file=sys.stdout)
      shutil.rmtree(tmp_profile)

  return tmp_profile

def usb_device_reset(deviceClass='Printer', sleepPerDevice=1):

    # (?:Bus.(\d+).+Dev.(\d+))(?=[\w\W]+Printer)
    # regwork = re.compile(r'(?:Bus.(\d+).+Dev.(\d+))(?=[\w\W]+{})'.format(deviceClass))

    # i.marochkin@rosalinu.ru
    # lsusb -t | grep 'Bus\|Printer' | tr '\n' 'x' | sed 's/x\//\n/g' | grep 'Printer' | cut -f1 -d',' | sed 's/[^0-9]/ /g'

    cmd = "lsusb -t | grep 'Bus\|Printer' | tr '\\n' 'x' | sed 's/x\//\\n/g' | grep '{}' | cut -f1 -d',' | sed 's/[^0-9]/ /g'".format(deviceClass)
    out = check_output(cmd, shell=True).splitlines()

    devs = []
    for d in out:
        devs.append( [int(i) for i in d.split()] )

    # print devs

    # UNBINDING
    BUS = DEV = err = 0
    for d in devs:
        BUS = d[0]
        DEV = d[2]
        # print >> sys.stderr, 'Printer found on BUS:{} DEV: {}'.format(BUS,DEV)
        # print >> sys.stderr, 'Unbind USB{} & sleep {}'.format(BUS, sleepPerDevice)

        err = call('echo \'{}-{}\' | tee /sys/bus/usb/drivers/usb/unbind'.format(BUS,DEV), stdout=PIPE, shell=True)

        if (err):
            print('Can\'t unbind {}-{}'.format(BUS,DEV), file=sys.stderr)

        time.sleep(sleepPerDevice)

    # BINDING
    for d in devs:
        BUS = d[0]
        DEV = d[2]
        #print >> sys.stderr, 'Rebind USB{} & sleep {}'.format(BUS, sleepPerDevice)

        err = call('echo \'{}-{}\' | tee /sys/bus/usb/drivers/usb/bind'.format(BUS,DEV), stdout=PIPE, shell=True)

        if (err):
            print('Can\'t bind {}-{}'.format(BUS,DEV), file=sys.stderr)

        time.sleep(sleepPerDevice)

    if (err):
        # raise ValueError('Not all ports have been restarted')
        return 1

    return 0

def cls():
    os.system('clear')

def processEx(name, kill=False) -> int:
  """
      process found     - return pid
      process killed    - return 0
      process not found - return -1
      error             - return -2

  """
  for proc in psutil.process_iter():
    try:
      if name.lower() in proc.name().lower():
        if ( kill ):
          proc.kill()
        return proc.pid
    except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
      return -2

  return -1

def whatlibs(Exit=True):

    pid  = os.getpid() # awk '{print $NF}' after
    deps = check_output("lsof -p {} | awk {} | grep -v 'NAME' | grep '.so'" \
                                   .format(pid, "'{print $NF}'"), shell=True).splitlines()
    print("Dependencies: \n", file=sys.stdout)
    for d in deps:
        print(d, file=sys.stdout)

    if (Exit):
        exit(0)

def groupCreate(name=''):

  if not name:
    print('Groupname is empty! Exit.', file=sys.stderr)
    exit(1)

  flags = ' {}'.format( name )

  res = x( 'groupadd' + flags, return_type=Return.process )

  if res.returncode:
    print('groupCreate error: {}'.format( res.stdout.read() ), file=sys.stderr)
    exit(res.returncode)

def groupDelete(name=''):

  if not name:
    print('Groupname is empty! Exit.', file=sys.stderr)
    exit(1)

  flags = ' {}'.format( name )

  res = x( 'groupdel' + flags, return_type=Return.process )

  if res.returncode:
    print('groupDelete error: {}'.format( res.stdout.read() ), file=sys.stderr)
    exit(res.returncode)

def chownR(path='', user='', group=''):

  if not path or not user or not group:
    print("One of parameters is empty! Exit.", file=sys.stderr)
    exit(1)

  for root, dirs, files in os.walk( path ):
    for d in dirs:
      os.chown( os.path.join( root, d ),
                pwd.getpwnam( user ).pw_uid,
                grp.getgrnam( group ).gr_gid )

    for f in files:
      os.chown( os.path.join( root, f ),
                pwd.getpwnam( user ).pw_uid,
                grp.getgrnam( group ).gr_gid )

  pass

def chmodR(path='', rights=''):

  if not path or not rights:
    print("One of parameters is empty! Exit.", file=sys.stderr)
    exit(1)

  for root, dirs, files in os.walk( path ):
    for d in dirs:
      os.chmod( os.path.join( root, d ), int( rights, base=8) )

    for f in files:
      os.chmod( os.path.join( root, f ), int( rights, base=8) )

def timerBlockCountDown(description='', suffix='', check_interval = 0.1, waiting = 10, unit='sec'):
  """ Prints message %description% to stdout and counts down from %waiting%
  """
  t = Ticker( waiting )

  with ConPlace() as CP:
    print( description )

    while ( t.tick() ):

      time.sleep( check_interval )
      tock = time.time()

      print( '{} {:.0f}/{} {}'.format( suffix, t.tock(), waiting, unit ), end='\r' )
      sys.stdout.flush()

def gen_sha512(phrase=''):

  return hashlib.sha512(phrase).hexdigest()

def contextTest(runascon='user_u:user_r:user_t:s0'):

  print( 'before run-context: {}'.format( selinux.getexeccon_raw() ) )
  print( 'before usr-context: {}'.format( selinux.getcon_raw() ) )
  print( 'before: {}\n '.format( x("id", return_type=Return.stdout) ) )

  # not safe
  with ConRun():
    print( '\tnow context: {}'.format( selinux.getexeccon_raw() ) )
    print( '\tnow: {}\n'.format( x("id ", return_type=Return.stdout) ) )

  # safe
  # cr = ConRun(uid=500, gid=500, raw_context="user_u:user_r:user_t:s0-s3:c0.c1023")
  # if cr.start():
  #   print(f'\tnow context: {selinux.getexeccon_raw()}')
  #   print(f'\tnow: {x("id ", return_type=Return.stdout)}\n')
  # cr.stop()

  print( 'after run-context: {}'.format( selinux.getexeccon_raw() ) )
  print( 'after: {}'.format( x("id ", return_type=Return.stdout) ) )

  pass

def quest(question:str, prepared='', variants='') -> Union[bool, str]:
  """ answer is always case insensitive
      example: quest( 'Hello, ? {variants}', 'mike', 'Mike|Michael|Mihairu' )
  """

  variants = [ ans.strip().lower() for ans in variants.split('|') if ans != '' ]
  varmode  = False

  if '{variants}' in question:
    question = question.format( variants=','.join(variants) )

  if len(variants) > 0:
    if prepared and prepared not in variants:
      print( 'quest: prepared answer does not match any of variants' )
      return False

    varmode = True

  import readline

  readline.set_startup_hook( lambda: readline.insert_text( prepared ) )
  try:
    if varmode:

      variant = ''
      while variant not in variants:
        variant = input(question).strip().lower()

      return variant

    elif prepared in input(question).lower():
      return True

    else:
      return False

  except KeyboardInterrupt:
    return '' if varmode else False

  finally:
    readline.set_startup_hook()

def se_enforce(stream = sys.stdout):

  # static var
  if not hasattr(se_enforce, 'SE_ENFORCING_STATUS'):
    se_enforce.SE_ENFORCING_STATUS = selinux.security_getenforce()

  if se_enforce.SE_ENFORCING_STATUS != 1:
    if selinux.security_getenforce():
      selinux.security_setenforce( 0 )
      print( 'SElinux: отключение... [ теперь {} ]'\
                    .format( 'permissive' if selinux.security_getenforce()==0 else 'enforcing' )
                    ,file=stream )
    else:
      selinux.security_setenforce( 1 )
      print( 'SElinux: включение... [ теперь {} ]'\
                    .format( 'permissive' if selinux.security_getenforce()==0 else 'enforcing' )
                    ,file=stream )

def seMakeSuite(clear=False):

  if not clear:

    u = User(name='tester', password='tester', home='/home/tester', se_user='user_u', se_range='s0-s3')
    u.deleted = True

    files = {
      '/home/tester/s0.txt'   : 'user_u:object_r:user_home_t:s0',
      '/home/tester/s3.txt'   : 'user_u:object_r:user_home_t:s3',
      '/home/tester/s2c4.txt' : 'user_u:object_r:user_home_t:s0:c4'
    }
    print(files)
    for path, con in files.items():
      with open( path, 'w' ) as f:
        f.write('text')
      selinux.setfilecon_raw( path, con )

  else:
    u = User(name='tester')
    u.delete(force=True)

def rangeGetFromCon(context:str, mcs=False):

  if   not context:            return []
  elif not context[3]:         return []
  elif mcs and not context[4]: return []

  _range = context.split(':')[4] if mcs else context.split(':')[3]
  _range = _range.replace( 'c' if mcs else 's', '')

  if   '.' in _range:
    enu = '.'
  elif '-' in _range:
    enu = '-'
  else:
    enu = ''

  if enu:
    _range = [ int(_) for _ in _range.split( enu ) ]
    _range = [ _ for _ in range( _range[0], _range[-1:][0]+1) ]
    return _range

  else:
    _range = [ int(_) for _ in _range.split( ',' ) ]
    return _range

def fileConGet(path:str) -> Context:

  P = Path( path )

  if P.is_symlink():
    P = Path( os.readlink( P.as_posix() ) )

  if not P.exists():
    return Context()

  if not os.path.exists(path): return Context()
  try:
    context_str = selinux.getfilecon_raw( path )[1]
    context_lst = context_str.split(':')
  except:
    return Context()

  if len(context_lst) == 5:
    return Context(
                    context_str,
                    context_lst[0],
                    context_lst[1],
                    context_lst[2],
                    rangeGetFromCon( context_str, mcs=False ),
                    rangeGetFromCon( context_str, mcs=True )
                  )

  if len(context_lst) == 4:
    return Context(
                    context_str,
                    context_lst[0],
                    context_lst[1],
                    context_lst[2],
                    rangeGetFromCon( context_str, mcs=False ),
                    None
                  )

def kernelParamCheck(param_name, need_value='y|m', config = '') -> int:
  """
  return 0 - all is ok
  return 1 - kernel parameter has unexpected value
  return 2 - kernel parameter not found
  """

  if not config:
      for f in os.listdir( '/boot' ):
        if 'config' in f:
          config = '/boot/' + f
          break

  found = False
  with open( config, 'r' ) as kconf:
    for l in kconf.readlines():

      if l.startswith('#'): continue

      if param_name in l:
        found = True

        need_value = need_value.split('|')

        if l.strip('\n').split('=')[1] not in need_value:
          return 1
        else:
          break

  if not found:
    return 2
  else:
    return 0

def packageCheck(pkg_name):

  code, out = x( 'rpm -qa {}'.format( pkg_name ), Return.codeout )
  if code == 0:
    if pkg_name in out:
      return True
  else:
    print( 'packageCheck: error {} - {}'.format( code, out ) )

  return False

def lineUpdate(file:Path, old_line:str, new_line:str, all=False, delete=0, strict=True) -> bool:
  """ The function updates the line in the file.

      Parameters:
      file     (str)  - path to the file where you want to replace the line
      old_line (str)  - a fragment of a line in need of replacement, case sensitive
      new_line (str)  - new line fragment to replace old_line fragment, case sensitive
      all      (bool) - if true, then all lines matching the old_line will be replaced,
                        otherwise the function will return after the first replacement
      delete   (int)  - if > 0 and new_line is '', will delete an integer number of
                        lines starting from the line containing old_line. You must
                        specify a number including old_line
      strict   (bool) - if false the whole string will be replaced with new_line, not
                        part of it. Don't forget about end of line.

      Returns:
      (bool) : True  if all replacements are successful
             : False if no file or line is found, or an error occurred while replacing
  """

  replaced = -1 if delete > 0 else 0
  deleted  = 0

  if not file.exists():
    return False

  new = new_line or '$16_#@23*&' # (marker) necessary for correct replacement
  old = old_line

  if len(old) > 0:

    # replacing
    with open( file.as_posix(), 'r' ) as f:
      content = f.readlines()

      for i,l in enumerate(content):

        if old in l:

          if strict:
            content[i] = l.replace( old, new )
          else:
            content[i] = new

          replaced += 1

          if not all: break

    # deleting and writing
    with open( file.as_posix(), 'r+' ) as f:
      for i,l in enumerate( content ):

        f.truncate()

        if new in l and delete > 0:
          f.write( '\n' if '\n' in l else '' )
          deleted = 1

        elif deleted != 0 and deleted < delete:
          f.write( '\n' if '\n' in l else '' )
          deleted += 1

        else:
          f.write( l )


  if replaced == 0 and deleted == 0:
    #print( f'  Файл {file} не был обновлён.' )
    return False

  #print( f'  Файл {file} успешно обновлён. Заменено: {replaced}, Удалено: {deleted}.' )
  return True

def colorCon( string:str, foreground:int, background:int=-1) -> str:
  """ The function returns a string with added ansi colors.

      Parameters:
      string     (str)  - source string to be colored
      foreground (int)  - 256-bit color code
      background (int)  - 256-bit color code

      Returns:
      (str) : colored line
  """

  RESET = '\033[0m'

  res     = ''
  content = string.splitlines()

  for part in content:
    buf = '\033[38;5;{fg}m{content}{reset}'.format( fg=foreground, content=part, reset=RESET  )

    if background > 0:
      buf = '\033[48;5;{bg}m'.format( bg=background ) + buf

    if part != content[-1]:
      res += buf + '\n'
    else:
      res += buf

  return res

def colorMap( stream=sys.stdout ):

  I = colorCon
  W = shutil.get_terminal_size().columns
  H = shutil.get_terminal_size().lines

  standard  = ''
  intensity = ''
  color216  = ''
  greyscale = ''

  for c in range(8):
    standard += I( '{:^6}'.format(c), 15, c )

  for c in range(8,16):
    intensity += I( '{:^6}'.format(c), 0, c )

  a,b = 16, 34
  for _ in range( 1, 13 ):
    for c in range( a, b ):
      color216 += I( '{:^6}'.format(c), 16, c )
    color216 += '\n'
    a += 18
    b += 18

  for c in range( 232, 244 ):
    greyscale += I( '{:^6}'.format(c), 15, c )
  greyscale += '\n'

  for c in range( 244, 256 ):
    greyscale += I( '{:^6}'.format(c), 16, c )

  print( I( '{:^{width}} {:^{width}}', 16, 244 ).format( 'STANDART', 'HIGH-INTENSITY', width=int(96/2) ) )
  print( '{:<} {:<{width}}'.format( standard, intensity, width=int(W/2) -1 ) )
  print()

  print( I( '{:^{width}}', 16, 244 ).format( '216 COLORS', width=108 ) )
  print( color216 )

  print( I( '{:^{width}}', 16, 244 ).format( 'GREYSCALE', width=72 ) )
  print( greyscale )

def socket_test():

  import socket

  socket_f = Path( '/tmp/testfolder/test.socket' )

  if socket_f.exists() and socket_f.is_socket():
    socket_f.unlink()

  with socket.socket( socket.AF_UNIX, socket.SOCK_STREAM ) as sock:
    sock.bind( socket_f.as_posix() )
    sock.listen()

    while True:
      conn, addr = sock.accept()
      data = conn.recv(150)

      print( 'recieved: ' + data.decode('utf-8') )

      conn.send( 'Hello, neighbor!\n'.format( addr ).encode() )
      conn.close()

def dmesgCheck(txt:str) -> bool:

  code, out = x( 'dmesg', Return.codeout )

  return code == 0 and txt in out

def dmesgClear() -> bool:

  code, out = x( 'dmesg -c', Return.codeout )

  if code != 0:
    print( 'dmesgClear: error {} - {}'.format( code, out ) )
    return False

  return True

def hasInet(host="8.8.8.8", port=53, timeout=5) -> bool:

  try:
    socket.setdefaulttimeout( timeout )
    socket.socket( socket.AF_INET, socket.SOCK_STREAM ).connect( (host, port) )
    return True

  except socket.error as e:
    print( 'socket: ' + str(e) )
    return False

def getInetDevices():

  devs = []

  code, out = x( 'ifconfig | grep BROADCAST | grep UP', Return.codeout )

  if code != 0:
    print( 'getInetDevices: ethernet devices not found!' )
    return None

  for line in out.splitlines():
    devs.append( line.split(':')[0] )

  return devs

def etherdog( interval:float=5.0 ):

  devs = getInetDevices()

  if devs is None:
    exit(1)

  try:
    while not sleep( interval ):

      if not hasInet():
        print( 'trying to restart network...')

        for d in devs:
          print( 'restarting: ' + d )

          # code, out = x( 'ip link set {i} down; ip link set {i} up'.format( i=d ), Return.codeout )
          # if code != 0:
          #   print( 'etherdog: ' + out )

          # code, out = x( 'dhclient -r {i}; dhclient {i}'.format( i=d ), Return.codeout )
          # if code != 0:
          #   print( 'etherdog: ' + out )

          code, out = x( 'systemctl restart networkmanager.service', Return.codeout )
          if code != 0:
            print( 'etherdog: ' + out )



  except KeyboardInterrupt:
    os.system( 'clear' )
    print( 'Network monitoring process stopped.' )

def save_env( path:Path ):

  env = os.environ
  res = []

  with path.open( 'w+' ) as f:
    for key, value in env.items():
      res.append( key + '=' + value + '\n' )
    res.sort()

    f.writelines( res )

  # with open( '/mnt/hgfs/1SH/ggg.txt', 'w+') as f:
  #   code, out = x( 'systemctl status systemd-user-sessions.service', Return.codeout )
  #   f.write( out )

if __name__ == '__main__':
  # whatlibs()
  cls()

