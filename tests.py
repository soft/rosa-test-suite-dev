#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@:::::::::@@@@@@@@@@@::::::@@@@@@@@@@@::::@@@@@@@@@@@@@:@@@@@@@@@   │
# │   @@@:------------::@@@@:----------::@@@@:--------::@@@@@@@:---:@@@@@@@   │
# │   @@@:---@@@@@@@@:--:@@---:@@@@@@@---:@@---:@@@@@:@@@@@@@@:-----@@@@@@@   │
# │   @@@:---@@@@@@@@:--:@:--:@@@@@@@@:---@@---:@@@@@@@@@@@@@@--:@:--@@@@@@   │
# │   @@@:---@@@@@@@:---@@:--:@@@@@@@@:---@@@:------:::@@@@@@---@@@--:@@@@@   │
# │   @@@:-----------::@@@:--:@@@@@@@@:---@@@@@@:::::---@@@@:--:@@@:--:@@@@   │
# │   @@@:---@@@@@:--:@@@@:--:@@@@@@@@:--:@@@@@@@@@@@:--:@@:-----------:@@@   │
# │   @@@:---@@@@@@:---:@@@:--:::@@@::--:@@::-:::@@::---@@:--:@@@@@@@---:@@   │
# │   @@@:::-@@@@@@@@::::@@@@:::-----::@@@@@@:::----::@@@::::@@@@@@@@:::-@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │                                                                           │
# │                              ROSA Test Suite                              │
# │                                                                           │
# │   Copyright © 2020 LLC "NTC IT ROSA"                                      │
# │   License: GPLv2                                                          │
# │   Authors:                                                                │
# │       Arthur Yalaletdinov <a.yalaletdinov@ntcit-rosa.ru> 2013             │
# │       Michael Mosolov     <m.mosolov@rosalinux.ru>       2019             │
# │                                                                           │
# │   This program is free software; you can redistribute it and/or modify    │
# │   it under the terms of the GNU General Public License as published by    │
# │   the Free Software Foundation; either version 2, or (at your option)     │
# │   any later version.                                                      │
# │                                                                           │
# │   This program is distributed in the hope that it will be useful,         │f
# │   but WITHOUT ANY WARRANTY; without even the implied warranty of          │
# │   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            │
# │   GNU General Public License for more details                             │
# │                                                                           │
# │   You should have received a copy of the GNU General Public License       │
# │   License along with this program; if not, write to the                   │
# │   Free Software Foundation, Inc.,                                         │
# │   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.           │
# │                                                                           │
# ╘═══════════════════════════════════════════════════════════════════════════╛

import fcntl
import http.server
import logging
import multiprocessing as MP
import os
import re
import shutil
import socket
import ssl
import subprocess
import sys
import threading
import time
import unittest
from collections import Counter
from datetime import datetime
from logging.handlers import QueueHandler, QueueListener
from mmap import mmap
from pathlib import Path
from struct import pack, unpack
from subprocess import getoutput, getstatusoutput
from tempfile import mkdtemp
from time import asctime, localtime, sleep

import selinux
from psutil import Process

import audit
import cairo
import jsonconfig
import keyboard
import ldap
import modules.tests_gui as guitests
import paramiko
from modules.kplpack.utils import *
from modules.kplpack.utils import colorCon as I
from modules.kplpack.utils import dmesgCheck, dmesgClear
from modules.kplpack.utils import x as X
from paramiko.ssh_exception import NoValidConnectionsError

with Damper():
  import gi
  gi.require_version( 'Gtk', '3.0' )
  from gi.repository import Gtk, Gdk, Pango, PangoCairo


log         = logging.getLogger( "rts.libtests" )
CURRTEST    = jsonconfig.Test()

# heavy tests "12", "55", "133", "136"

def logIfError(errcode, msg):
    if errcode:
        log.error(msg)

class BootConfig:
    """
    Вспомогательный класс загрузки опций конфигурации ядра из /boot/config
    """

    def __init__(self, filepath):
        with open(filepath, "r") as f:
            filecontent = f.readlines()
        self._params = {}
        for line in filecontent:
            if line.startswith("#"):
                continue
            pv = line.strip("\n").split("=")
            if len(pv) == 2:
                self._params[pv[0]] = pv[1]
            else:
                self._params[pv[0]] = ""

    def check(self, params):
        ret = []
        for p in params:
            if p in self._params:
                ret.append(self._params[p])
        return ret

def getCPUsNum():
    """ Возвращает число процессоров, установленных в системе """
    if hasattr(os, "sysconf"):
        if "SC_NPROCESSORS_ONLN" in os.sysconf_names:
            n = os.sysconf("SC_NPROCESSORS_ONLN")
            if isinstance(n, int) and n > 0:
                return n
    return 1

def executeGuiTest(test_name, arg1):
  python_bin = 'python3.5'
  cpid = os.getpid()
  user = os.popen( 'ps -p {} -ouser='.format(cpid) ).read().strip()

  while user == "root":
      ppid = os.popen( 'ps -p {} -oppid='.format(cpid) ).read().strip()
      if not (ppid):
          break
      cpid = int(ppid)
      if cpid == 0:
          break
      user = os.popen( 'ps -p {} -ouser='.format(cpid) ).read().strip()

  script_path = os.path.join(os.path.dirname(__file__), 'modules', 'tests_gui.py')
  log.debug( '  Запуск вспомогательного модуля под именем {}...'.format(test_name) )

  cm = 'su {} -c "{} {} --testname {} --arg1 {}"'.format( user, python_bin, script_path, test_name, arg1 )

  p = X( cm, return_type=Return.process)

  out = ''
  for line in p.stdout.read():
    if 'Gtk-WARNING' in line:
      continue
    else:
      out += line

  if p.poll():
      log.debug('  Cообщения выполненного модуля:\n' + out )
  return p.returncode, out

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            pack('256s'.encode(), ifname[:15].encode())
        )[20:24])
    except IOError:
        return '0.0.0.0'

def finishProcess(proc, procName):
    if proc and proc.poll() is None:
        try:
            log.debug('  Завершение работы %s...' % procName)
            proc.terminate()
        except OSError:
            pass

class BaseTest(unittest.TestCase):

  def setUp(self):
    log.info( '┌{:─^50}┐\n'.format('┤ Начало базовой проверки ├') )

    if len( CURRTEST.kconfig ) > 0 :

      err = CURRTEST.check_kconfig()
      self.assert_( err < 0, I( 'Проверка конфигурации ядра не пройдена: параметр - {}', 9 )
                            .format( CURRTEST.kconfig[err] ) )
      log.info( 'проверка конфигурации ядра - ok'.center( 50, ' ' ) )

    if len( CURRTEST.package ) > 0:

      err = CURRTEST.check_package()
      self.assert_( err < 0, I( 'Проверка установленных пакетов не пройдена: параметр - {}', 9 )
                            .format( CURRTEST.package[err] ) )
      log.info( 'проверка установленных пакетов - ok'.center( 50, ' ' ) )

    if len( CURRTEST.kmodule ) > 0:

      err = CURRTEST.check_kmodule()
      self.assert_( err < 0, I( '  Проверка наличия модулей ядра не пройдена: параметр - {}', 9 )
                            .format( CURRTEST.kmodule[err] ) )
      log.info( 'проверка наличия модулей ядра - ok'.center( 50, ' ' ) )

    if len( CURRTEST.service ) > 0:

      err = CURRTEST.check_service()
      self.assert_( err < 0, I( 'Проверка доступных сервисов не пройдена: параметр - {}', 9 )
                            .format( CURRTEST.service[err] ) )
      log.info( 'проверка доступных сервисов - ok'.center( 50, ' ' ) )


    if len( CURRTEST.kmodule ) > 0:

      CURRTEST.clear_dmesg()
      err = CURRTEST.run_kmodules()
      self.assert_( err < 0, I( '  Подключение модулей ядра вызвало ошибки: параметр - {}', 9 )
                            .format( CURRTEST.kmodule[err] ) )
      log.info( 'подкючение модулей ядра - ok'.center( 50, ' ' ) )


    if len( CURRTEST.service ) > 0:

      err = CURRTEST.run_services()
      self.assert_( err < 0, I( '  Запуск сервисов вызвал ошибки: параметр - {}', 9 )
                            .format( CURRTEST.service[err] ) )
      log.info( 'запуск сервисов - ok'.center( 50, ' ' ) )

    log.info( '\n {:┄^50}\n '.format(' СТАРТ ') )

    self.addCleanup( self.endStage )

  def endStage(self):

    log.info( '\n {:┄^50}\n '.format(' ФИНИШ ') )

    if len( CURRTEST.kmodule ) > 0:

      err = CURRTEST.rem_kmodules()
      self.assert_( err < 0, I( 'Отключение модулей ядра вызвало ошибки: параметр - {}', 9 )
                            .format( CURRTEST.kmodule[err] ) )
      log.info( 'отключение модулей ядра - ok'.center( 50, ' ' ) )

    if len( CURRTEST.dmesg ) > 0:

      err = CURRTEST.check_dmesg()
      self.assert_( err < 0, I( 'Ошибка при проверке dmesg: параметр - {}', 9 )
                            .format( CURRTEST.dmesg[err] ) )
      log.info( 'проверка сообщений dmesg - ok'.center( 50, ' ' ) )

    if len( CURRTEST.service ) > 0:

      err = CURRTEST.stop_services()
      self.assert_( err < 0, I( '  Остановка сервисов вызвала ошибки: параметр - {}', 9 )
                            .format( CURRTEST.service[err] ) )
      log.info( 'остановка сервисов - ok'.center( 50, ' ' ) )

    log.info( '\n└{:─^50}┘\n'.format( '┤ Конец базовой проверки ├' ) )

  def runTest(self):
    log.info( '  Generic-тест. Дополнительные проверки не проводятся.' )

def toggleRosaDeviceManager():

  if not hasattr(toggleRosaDeviceManager, 'ON'):
    toggleRosaDeviceManager.ON = True

  if toggleRosaDeviceManager.ON:
    log.info( 'rosa-device-manager: отключение блокирующего режима...' )

    code, out = x( 'rosa-device-manager -d; rosa-device-manager -n', Return.codeout )
    if code != 0:
      log.info( 'Отключить rosa-device-manager не удалось: {}.'.format( out ) )

    log.info( 'rosa-device-manager: OK' )
    toggleRosaDeviceManager.ON = False

  else:
    log.info( 'rosa-device-manager: включение блокирующего режима...' )

    code, out = x( 'rosa-device-manager -e; rosa-device-manager -b', Return.codeout )
    if code != 0:
      log.info( 'Включить rosa-device-manager не удалось: {}.'.format( out ) )

    log.info( 'rosa-device-manager: OK' )
    toggleRosaDeviceManager.ON = True

######################    Тесты функциональности     ###########################

class testKernelModuleSignature(BaseTest):

  def runTest(self):
    with Indicator( '  Проверка механизма подписи модулей ядра' ):
      with Terminal( StreamToLog( log ) ) as term:
        term.add( 'rmmod unsignedmod', ignore_errors=True ) \
            .info( '  Отключение модуля ядра unsignedmod (если включен)' )
        term.add( 'dkms remove -m unsignedmod -v 1.0 --all', ignore_errors=True )\
            .info( '  Удаление предыдущей версии unsignedmod...\n' )
        term.add( 'dkms add -m unsignedmod -v 1.0' )\
            .info( '  Подготовка исходников модуля unsignedmod...\n' )
        term.add( 'dkms build -m unsignedmod -v 1.0' )\
            .info( '  Сборка модуля unsignedmod...\n' )
        term.add( 'dkms install -m unsignedmod -v 1.0' )\
            .info( '  Установка модуля unsignedmod...\n' )
        term.add( 'dmesg -C' )\
            .info( '  Очистка dmesg...\n' )

      self.assert_( term.fail[0] == 0, 'При подготовке модуля ядра unsignedmod произошла ошибка [{}]: {}'
                                        .format( term.fail[0], term.fail[1] ) )

      log.info( '  Подключение и запуск модуля ядра unsignedmod...' )
      # out: ERROR: could not insert 'unsignedmod': Key was rejected by service
      code, out = X( 'modprobe unsignedmod', Return.codeout )

      conclusion =  dmesgCheck( 'Loading of unsigned module is rejected' ) \
                    and not dmesgCheck( 'unsignedmod: test failed!' )      \
                    and code == 1

    self.assert_( conclusion, 'Не получена ожидаемая ошибка при подключении неподписанного модуля ядра.' )

  def tearDown(self):
    with Terminal( StreamToLog( log ) ) as term:
      term.add( 'rmmod unsignedmod', ignore_errors=True ) \
          .info( '  Отключение модуля ядра unsignedmod' )
      term.add( 'dkms remove -m unsignedmod -v 1.0 --all' ) \
          .info( '  Удаление модуля ядра unsignedmod...\n' )

    self.assert_( term.fail[0] == 0, 'При удалении модуля ядра unsignedmod произошла ошибка [{}]: {}'
                             .format( term.fail[0], term.fail[1] ) )

class testSLAB_srm(BaseTest):

  tool        = Path( '/usr/lib64/rosa-test-suite/srm_test.py' )
  pass_phrase = 'srm_test: test passed'

  def runTest(self):
    self.assert_( self.tool.exists(), 'Утилита необходимая для проверки не обнаружена: {}'.format( self.tool.as_posix() ) )

    code, out = X( self.tool.as_posix(), Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

class testSLAB_rm(BaseTest):

  tool        = Path( '/usr/lib64/rosa-test-suite/rm_test.py' )
  pass_phrase = 'rm_test: test passed'

  def runTest(self):
    self.assert_( self.tool.exists(), 'Утилита необходимая для проверки не обнаружена: {}'.format( self.tool.as_posix() ) )

    code, out = X( self.tool.as_posix(), Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

class testSLAB_shred(BaseTest):

  tool        = Path( '/usr/lib64/rosa-test-suite/shred_test.py' )
  pass_phrase = 'shred_test: test passed'

  def runTest(self):
    self.assert_( self.tool.exists(), 'Утилита необходимая для проверки не обнаружена: {}'.format( self.tool.as_posix() ) )

    code, out = X( self.tool.as_posix(), Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

class testNPTL(BaseTest):

  tool = Path( '/usr/lib64/rosa-test-suite/npttest' )

  def runTest(self):
    self.assert_( self.tool.exists(), 'Утилита необходимая для проверки не обнаружена: {}'.format( self.tool.as_posix() ) )

    code, out = X( self.tool.as_posix(), Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

class testAtaScsi(BaseTest):

  tool  = Path( '/usr/sbin/amtu' )
  flags = '-i'

  def runTest(self):
    self.assert_( self.tool.exists(), 'Утилита необходимая для проверки не обнаружена: {}'.format( self.tool.as_posix() ) )

    code, out = X( self.tool.as_posix() + ' ' + self.flags, Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

class testVGA_VESA(BaseTest):

  tool  = Path( '/usr/lib64/rosa-test-suite/fb_test' )
  flags = '/dev/fb0'

  def runTest(self):
    self.assert_( self.tool.exists(), 'Утилита необходимая для проверки не обнаружена: {}'.format( self.tool.as_posix() ) )

    code, out = X( self.tool.as_posix() + ' ' + self.flags, Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

class testNET(BaseTest):

  tool  = Path( '/usr/sbin/amtu' )
  flags = '-n'

  def runTest(self):
    self.assert_( self.tool.exists(), 'Утилита необходимая для проверки не обнаружена: {}'.format( self.tool.as_posix() ) )

    code, out = X( self.tool.as_posix() + ' ' + self.flags, Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

class testIP(BaseTest):

  tool1  = 'ping'
  flags1 = '-c 2 127.0.0.1'

  tool2  = 'ping6'
  flags2 = '-c 2 0:0:0:0:0:0:0:1'

  def runTest(self):

    code, out = X( self.tool1 + ' ' + self.flags1, Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

    code, out = X( self.tool2 + ' ' + self.flags2, Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

class testUSB(BaseTest):

  tool1  = 'test'
  flags1 = '-d /sys/bus/usb/devices'

  def runTest(self):
    log.info( '  Запуск проверяющей утилиты...' )
    code, out = X( self.tool1 + ' ' + self.flags1, Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

class testPrintLocal(BaseTest):

  tool  = 'lp'
  flags = '/usr/share/cups/data/testprint'


  def runTest(self):

    code, out = X( self.tool + ' ' + self.flags, Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

class testMemorySeparation(BaseTest):

  tool  = Path( '/usr/sbin/amtu' )
  flags = '-s'

  def runTest(self):
    self.assert_( self.tool.exists(), 'Утилита необходимая для проверки не обнаружена: {}'.format( self.tool.as_posix() ) )

    code, out = X( self.tool.as_posix() + ' ' + self.flags, Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

class testAuthentication(BaseTest):

  tool    = Path( '/usr/lib64/rosa-test-suite/pamtest.sh' )
  command = 'echo "n" | pamtester login root open_session'

  def runTest(self):
    self.assert_( self.tool.exists(), 'Утилита необходимая для проверки не обнаружена: {}'.format( self.tool.as_posix() ) )

    code, out = X( self.tool.as_posix(), Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

    code, out = X( self.command, Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

class testInstalledPartEd(BaseTest):

  tool  = 'which'
  flags = 'diskdrake'

  def runTest(self):

    code, out = X( self.tool + ' ' + self.flags, Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

class testIPC(BaseTest):

  tools = [
            [ Path('/usr/lib64/rosa-test-suite/netlinktest') , 'hello' ],
            [ Path('/usr/lib64/rosa-test-suite/signaltest')  , '' ],
            [ Path('/usr/lib64/rosa-test-suite/shmemtest')   , '' ],
            [ Path('/usr/lib64/rosa-test-suite/semtest')     , '' ],
          ]

  def runTest(self):

    for t in self.tools:
      self.assert_( t[0].exists(), 'Утилита необходимая для проверки не обнаружена: {}'.format( t[0].as_posix() ) )

      code, out = X( t[0].as_posix() + ' ' + t[1], Return.codeout )

      for l in out.splitlines():
        log.info( '  {}'.format( l ) )

      self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

class testI2C(BaseTest):

  def runTest(self):
    code, out = CURRTEST.run_kmodule(0)
    self.assert_( code == 0, 'При запуске модуля ядра {} произошла ошибка [{}]: {}'
                             .format( CURRTEST.kmodule[0], code, out ) )
    log.info( '  Модуль {} отработал без ошибок.'.format( CURRTEST.kmodule[0] ) )

class testQuota(BaseTest):
  """ #540 """

  tfs = Path( '/var/tmp/testpart.tfs' )
  mnt = Path( '/mnt/tfs' )

  user = 'rosa-test-user'

  def runTest(self):

    log.info( '  Создание нового пользователя {} ..."'.format( self.user ) )
    U = User( name=self.user, password=self.user, home='/home/'+self.user, se_user='user_u', se_range='s0' )

    with Terminal( StreamToLog( log ) ) as term:
      term.add( 'dd if=/dev/zero bs=1M count=1 of={}'.format( self.tfs.as_posix() ) )\
          .info( '  Выделение места под тестовую файловую систему...\n')
      term.add( 'mkfs -t ext4 {}'.format( self.tfs.as_posix() ) )\
          .info( '  Создание тестовой файловой системы(tfs)...\n' )
      term.add( 'mkdir {}'.format( self.mnt.as_posix() ) )\
          .info( '  Создание точки монтирования({})...\n'.format( self.mnt.as_posix() ) )
      term.add( 'mount -t auto -o loop,usrquota,grpquota {} {}'.format( self.tfs.as_posix(), self.mnt.as_posix() ) )\
          .info( '  Монтирование тестовой файловой системы(tfs) в {} \n'.format( self.mnt.as_posix() ) )
      term.add( 'mount -o remount,rw {}'.format( self.mnt.as_posix() ) )\
          .info( '  Переключение тестовой файловой системы(tfs) в режим чтения и записи.\n' )
      term.add( 'chown {u}:{u} {p}'.format( u=self.user, p=self.mnt.as_posix() ) )\
          .info( '  Смена владельца файловой системы\n' )
      term.add( 'quotacheck -cuvg {p}; quotaon {p}'.format( p=self.mnt.as_posix() ) )\
          .info( '  Активация механизма квотирования...\n'.format( self.mnt.as_posix() ) )
      term.add( 'setquota -u {} 0 4 2 4 -a {}'.format( self.user, self.mnt.as_posix() ) )\
          .info( '  Установка квот на {}: 1Кб, 4 файла\n'.format( self.mnt.as_posix() ) )
      term.add( 'setquota -t 5 5 {}'.format( self.mnt.as_posix() ) )\
          .info( '  Установка срока действия квот(soft) на {}: 2 файла, 5 секунд\n'.format( self.mnt.as_posix() ) )

    self.assert_( term.fail[0] == 0, 'При создании тестовой файловой системы и настройке квот произошла ошибка [{}]: {}'
                                       .format( term.fail[0], term.fail[1] ) )

    log.info( '  Запуск тестирования квот...' )

    with Indicator( ' Тестирование квот...'):
      with ConRun( U.uid, U.gid ) as mem:

        # testing the quota for the number of files
        try:
          for n in range( 4 ):
            with open( self.mnt.as_posix() + "/{}.file".format( n ), 'w+' ) as f: pass

        except OSError as e:
          if e.errno == 122:
            print( 'OK' )
          else:
            print( e )

        try:
          os.unlink( self.mnt.as_posix() + "/2.file" )
        except OSError as e:
          print( e )

        # testing the file size quota
        try:
          with open( self.mnt.as_posix() + "/large.file", 'a+' ) as f:
            for _ in range( 4096 ):
              f.write( 'b' )

        except OSError as e:
          if e.errno == 122:
            print( 'OK' )
          else:
            print( e )

        try:
          os.unlink( self.mnt.as_posix() + "/large.file" )
        except OSError as e:
          print( e )

        sleep(5)

        # testing temporary quotas
        try:
          with open( self.mnt.as_posix() + "/extra.file", 'w+' ) as f:
            pass

        except OSError as e:
          if e.errno == 122:
            print( 'OK' )
          else:
            print( e )

    log.info( '  Протестирована квота на ограничение кол-ва файлов, квота на объем файла, soft квота.')

    for m in mem:
      if m not in [ 'OK', '' ]:
        self.fail( 'Во время проверки механизма квотирования произошла ошибка: {}'.format( m ) )


  def tearDown(self):

    if self.mnt.exists():
      log.info( '  Попытка размонтировать тестовую файловую систему(tfs)...' )
      code, out = X( 'umount {}'.format( self.mnt.as_posix() ), Return.codeout )

      self.assert_( code == 0, 'При размонтировании тестовой файловой системы произошла ошибка [{}]: {}'.format( code, out ) )

      self.mnt.rmdir()

    if self.tfs.exists():
      self.tfs.unlink()

class testPriorityNICE(BaseTest):
  """ #522 """

  def setUp(self):
    super(testPriorityNICE, self).setUp()

    self.cores = os.cpu_count()


  @staticmethod
  def race( i:int=100 ):

    for x in range( i ):
      for y in range( x + 1 ):
       ( x + 1 ) / ( y + 1 ) * ( x + y )

  def renice(self, nice:int, pid:int):

    code, out = X( 'renice {} {} '.format( nice, pid ), Return.codeout )
    self.assert_( code == 0, 'Не удалось изменить приоритет процесса. NICE={}: {}'
                             .format( nice, out ) )

  @staticmethod
  def payload_cpu( cores:int, duration:int ):
    X( 'stress --cpu {} --timeout {}'.format( cores, duration ) )

  def do(self, delay:int, renice:int, duration:int = 10000 ):

    if delay > 0:
      sleep( delay )

    P = MP.Process( target=self.race, args=( duration, ) )
    P.start()

    while not P.pid : pass
    self.renice( renice, P.pid )

    P.join()

  def runTest(self):

    log.info( '  Нагрузка свободных вычислительных мощностей ( {} ядра(ер) cpu )'.format( self.cores ) )
    payload = MP.Process( target=self.payload_cpu, args=( self.cores, 40 ), daemon=True )
    payload.start()

    self.renice( 5, os.getpid() )

    with Indicator( ' Работа процесса с приоритетом(низкий) NICE=20...' ):
      log.info( '  Процесс NICE=20 начал свою работу.' )

      with Ticker( timeout=15, high_resolution=True ) as res1:
        self.do( 0, 0, 10000 )
      log.info( '  Процесс NICE=20 завершил свою работу за {} сек.'.format( res1[0] ) )

    with Indicator( ' Работа процесса с приоритетом(высокий) NICE=-20...' ):
      log.info( '  Процесс NICE=-20 начал свою работу c задержкой 1 сек.' )

      with Ticker( timeout=15, high_resolution=True ) as res2:
        self.do( 1, -20, 10000 )
      log.info( '  Процесс NICE=-20 завершил свою работу за {} сек.'.format( res2[0] ) )

    delta = res1[0] - res2[0]
    self.assert_( delta > 1.0, 'Не удалось проверить рекцию системы на приоритет NICE. {} - {} <= 1.0 сек.'
                                           .format( res1[0], res2[0] ) )

    log.info( '  Процесс с высоким приоритетом NICE завершил работу быстрее на {} секунд(ы).'.format( delta ) )


  def tearDown(self):
    self.renice( -5, os.getpid() )

class testFailSaveStatus(BaseTest):
  """ #560 """

  tfs = Path( '/var/tmp/testfile.tfs' )
  utf = Path( '/unlimited.file')
  mnt = Path( '/mnt/tfs/' )


  def init(self):
    super(testFailSaveStatus, self).setUp()

    with Terminal( StreamToLog( log ) ) as term:
      term.add( 'dd if=/dev/zero bs=1M count=1 of={}'.format( self.tfs.as_posix() ) )\
          .info( '  Выделение места под тестовую файловую систему...\n')
      term.add( 'mkfs -t ext4 {}'.format( self.tfs.as_posix() ) )\
          .info( '  Создание тестовой файловой системы(tfs)...\n' )
      term.add( 'mkdir {}'.format( self.mnt.as_posix() ) )\
          .info( '  Создание точки монтирования({})...\n'.format( self.mnt.as_posix() ) )
      term.add( 'mount -t auto -o loop {} {}'.format( self.tfs.as_posix(), self.mnt.as_posix() ) )\
          .info( '  Монтирование тестовой файловой системы(tfs) в {} \n'.format( self.mnt.as_posix() ) )
      term.add( 'mount -o remount,rw {}'.format( self.mnt.as_posix() ) )\
          .info( '  Переключение тестовой файловой системы(tfs) в режим чтения и записи.\n' )

    # code, out = X( 'dd if=/dev/zero bs=100 count=1 of={}'.format( self.testfile.as_posix() ), Return.codeout )

    self.assert_( term.fail[0] == 0, 'При создании тестовой файловой системы произошла ошибка [{}]: {}'
                                       .format( term.fail[0], term.fail[1] ) )

    self.user = User( 'rosa-test-user', home='/home/rosa-test-user' )

  @staticmethod
  def payload_ram(size):
    α = 1000
    Δ = b'44765425243242662457573698374592837465923764592364'
    free = size

    with mmap( -1, size ) as mem:

      while free > 0:
        free = size - mem.tell()

        if free >= α * len( Δ ):
          mem.write( α * Δ )
        elif len( Δ ) > 1:
           Δ = Δ[1:]
        elif α > 1:
          α -= 1

    return 0

  def runTest(self):

    self.init()

    log.info( '  Попытка создать исключительную ситуацию "Ошибка ввода-вывода (soft)"... ' )

    try:
      with open( self.mnt.as_posix() + '/testfile.t', 'w+' ) as f:
        for _ in range( self.tfs.stat().st_size + 1 ):
          f.write( '1' )
    except OSError as e:
      if e.errno == 28:
        log.info( '  Произошел ожидаемый сбой ввода\вывода при переполнении файловой системы!' )

    log.info( '  Попытка создать исключительную ситуацию "Ошибка ввода-вывода (hard)"')

    with Indicator( ' Заполнение свободного пространства / ...'):
      code, out = X( 'dd if=/dev/zero of={} bs=1G count=665'.format( self.utf.as_posix() ), Return.codeout )

    if code != 0:
      log.info( '  Произошел ожидаемый сбой ввода\вывода при переполнении файловой системы!' )
    else:
      self.fail( 'Произошел неожиданный сбой [{}]: {}'.format( code, out ) )

    log.info( '  Попытка создать исключительную ситуацию "Ошибка выделения памяти (soft)"')

    ram = psutil.virtual_memory().total
    swp = psutil.swap_memory().free
    rns = ram + swp

    log.info( '  Доступно RAM: {} SWAP: {}'.format( ram, swp ) )
    log.info( '  Попытка захвата памяти {} * 2 ...'.format( rns ) )

    with Indicator( ' Захват памяти, возможна кратковременная потеря контроля над ЭВМ...' ):
      with Damper():
        P = MP.Process( target=self.payload_ram, args=( rns, ) )
        P.start()
        P.join()

    if P.exitcode in [ -9, -7 ]:
      log.info( '  Произошел ожидаемый сбой при выделении памяти в ОЗУ!' )
    else:
      self.fail( 'Произошел неожиданный сбой [{}].'.format( P.exitcode ) )

    log.info( '  Попытка создать исключительную ситуацию "Устройство или ресурс занято (soft)"')
    try:
      self.mnt.rmdir()

    except OSError as e:
      if e.errno == 16:
        log.info( '  Произошел ожидаемый сбой "Устройство или ресурс занято"!' )
      else:
        self.fail( 'Произошел неожиданный сбой [{}]: {}'.format( e.errno, e.strerror ) )

    ## log.info( '  Попытка создать исключительную ситуацию "Превышено время ожидания (soft)"')
    ## dd if=/dev/sda1 of=/home/test/u.f bs=2G count=1 iflag=fullblock,direct oflag=nocache

    log.info( '  Проверка безопасности состояния после сбоев..."')

    with Indicator( ' Проверка безопасности состояния после сбоев...' ):
      se_enforce( StreamToLog( log ) )
      log.info( '  Запуск rosa-security-test...')
      err = X( 'sudo -u {} rst --fault-code --logtype nolog'.format( self.user.name ) )
      se_enforce( StreamToLog( log ) )

    self.assert_( err == 0, 'Не был пройден тест #{}!'.format( err ) )

    log.info( 'Тест пройден успешно.')

  def tearDown(self):

    if self.mnt.exists() and os.path.ismount( self.mnt.as_posix() ):
      log.info( '  Попытка размонтировать тестовую файловую систему(tfs)...' )
      code, out = X( 'umount {}'.format( self.mnt.as_posix() ), Return.codeout )

      self.assert_( code == 0, 'При размонтировании тестовой файловой системы произошла ошибка [{}]: {}'.format( code, out ) )

    if self.mnt.exists():
      self.mnt.rmdir()

    if self.tfs.exists():
      self.tfs.unlink()

    if self.utf.exists():
      self.utf.unlink()

class testSLAB_swap(BaseTest):
  """ #487 """

  swp_copy = Path('/var/tmp/swp.bin')
  cleaner  = 'rosa-memory-clean --swap'

  def swp(self):
    return psutil.swap_memory().percent

  def setUp(self):
    super(testSLAB_swap, self).setUp()

    if self.swp_copy.exists():
      self.swp_copy.unlink()
    os.sync()

  def has_zram(self):
    code, out = X( 'zramctl -b -o name,disksize,mountpoint -n --raw', Return.codeout )

    total = 0
    devs  = []
    mnts  = []

    if code == 0:
      for l in out.splitlines():
        dev, size, mnt = l.split(' ')
        total += int( size )
        devs.append( dev )
        mnts.append( mnt )

    if total > 0 and len(devs) > 0:
      log.info( '  Обнаружен ZRAM: {} byte'.format( total ) )
      for i,d in enumerate(devs):
        log.info( '     {}'.format( d, mnts[i] ) )
      return True
    else:
      return False

  def runTest(self):

    if psutil.swap_memory().total > 0 and not self.has_zram():

      log.info( '  Обнаружен swap размером {} байт.'.format( psutil.swap_memory().total ) )

      log.info( '  Проверка доступной ОЗУ...')
      check_ram = psutil.virtual_memory().percent
      self.assert_( check_ram <= 60.0, ( 'Для данного теста необходимо иметь от 40% свободной ОЗУ.'
                                        'Текущий процент свободной ОЗУ: {}'.format( check_ram ) ) )

      log.info( '  Текущий swap = {}% '.format( self.swp() ) )
      log.info( '  Очистка swap c помощью "{}"...'.format( self.cleaner ) )

      code, out = X( self.cleaner, Return.codeout )
      self.assert_( code == 0, 'При очистке раздела swap произошла ошибка [{}]: {}'.format( code, out ) )

      os.sync()
      log.info( '  Текущий swap = {}% '.format( self.swp() ) )

      log.info( '  Клонирование нового раздела swap...' )

      with Indicator( ' Клонирование нового раздела swap' ):
        code, out = X( 'dd if=/dev/sda5 bs=512 count=3491840 oflag=nocache of={}'.format( self.swp_copy.as_posix() ), Return.codeout )
        self.assert_( code == 0, 'При клонировании раздела swap произошла ошибка [{}]: {}'.format( code, out ) )

      check_swp = self.swp()
      self.assert_( check_swp == 0.0, 'Во время клонирования был задействован swap( {}% ), освободите ОЗУ и попробуйте заново.'
                                      .format( check_swp ) )

      log.info( '  Во время клонирования swap задействован не был.' )

      log.info( '  Проверка данных склонированного раздела...')
      with Indicator( ' Проверка данных склонированного раздела' ):

        with open( self.swp_copy.as_posix(), 'rb' ) as F:
          zeroed   = 0
          notnull  = 0
          position = 0

          for i,byte in enumerate( F.read() ):
            if byte != 0:
              notnull += 1
              position = i
              break
            else:
              zeroed += 1

      self.assert_( notnull == 0, 'В чистом swap обнаружены данные. На момент обнаружения: зануленных байт {} до позиции {}.'
                                  .format( zeroed, position ) )

    else:
      log.info( '  Раздел swap не обнаружен. Проверка не требуется.' )


  def tearDown(self):
    if self.swp_copy.exists():
      self.swp_copy.unlink()
    os.sync()

class testServicePriority(BaseTest):
  """ #522 """

  CPU_CORES = psutil.cpu_count() - 1 if psutil.cpu_count() > 1 else 1
  TMP_FILE  = '/var/tmp/testfile.stress'

  def set_param(self, param:str, val:str):

    res = X( 'cgset -r {}="{}" test'.format( param, val ), Return.process )

    log.info( '  Установлен параметр {}={}.'.format( param, val ) )

    self.assert_( res.returncode == 0, 'Не удалось установить параметр: {}={} .'.format( param, val ) )

  def set_group(self, pid:int):

    log.info( '  Добавление процесса[{}] в контролирующую группу /test...'.format( pid ) )
    code, out = X( 'cgclassify -g cpu,cpuset,memory,blkio:/test --sticky {}'.format( pid ), Return.codeout )

    self.assert_( code == 0, 'Не удалось добавить процесс[{}] в группу /test: {}'.format( pid, out ) )

  def unset_group(self, pid:int):

    log.info( '  Удаление процесса[{}] из контролирующей группы /test...'.format( pid ) )
    code, out = X( 'cgclassify -g cpu,cpuset,memory,blkio:/ {}'.format( pid ), Return.codeout )

    self.assert_( code == 0, 'Не удалось удалить процесс[{}] из группы /test: {}'.format( pid, out ) )

  def setUp(self):

    super(testServicePriority, self).setUp()

    log.info( 'Для правильной работы теста необходимо закрыть все пользовательские приложения кроме rosa-test-suite!' )

    log.info( '  Создание контролирующей группы /test ...' )
    res = X( 'cgcreate -g cpu,cpuset,memory,blkio:/test', Return.process )

    self.assert_( res.returncode == 0, 'Не удалось создать группы [{}]:{}'.format( res.returncode, res.stdout.read() ) )

    self.set_param( 'cpuset.cpus', '0-{}'.format( self.CPU_CORES ) )
    self.set_param( 'cpuset.mems', '0' )

  @classmethod
  def challenge(self, q, payload):
    _log = logging.getLogger()
    _log.setLevel( logging.DEBUG )
    _log.addHandler( QueueHandler(q) )

    _log.info( '  Создан процесс: {}'.format( os.getpid() ) )
    _log.info( '  Генерация нагрузки...')

    payload()

    return 0

  @staticmethod
  def payload_cpu():
    X( 'stress --quiet --cpu {} --timeout 10'.format( testServicePriority.CPU_CORES ) )

  @staticmethod
  def payload_ram():
    X( 'stress --quiet --vm 1 --vm-bytes 1G --vm-hang 0 --vm-keep --timeout 15' )

  @staticmethod
  def payload_io():
    #X( 'stress --hdd 30 --hdd-bytes 4096 -t 10s' )
    X( 'timeout -s SIGKILL 5s dd if=/dev/zero of={} oflag=direct,dsync,sync bs=1M count=4000'.format( testServicePriority.TMP_FILE ) )

  def get_loaded_cores(self):
    counter = 0

    for _ in range(3):
      cores = psutil.cpu_percent( interval=0.1, percpu=True )

      for c in cores:
        if c > 75.0: counter += 1

    counter = counter / self.CPU_CORES
    return counter

  def get_loaded_ram(self, pid, measurements=4):

    results = []

    for _ in range( measurements ):
      time.sleep(1)
      percent =  psutil.Process( pid ).memory_percent()

      for proc in psutil.Process( pid ).children( recursive=True ):
        percent +=  proc.memory_percent()
      results.append( percent )

    return sum( results ) / len( results )

  def get_io_serviced(self, pid):
    results = []

    b =  psutil.Process( pid ).io_counters().read_count + psutil.Process( pid ).io_counters().write_count
    results.append( b )

    for proc in psutil.Process( pid ).children( recursive=True ):
      b +=  proc.io_counters().read_count + proc.io_counters().write_count
    results.append( b )

    return sum( results ) / len( results )

  def Go(self, payload, sleep=5):

    ctx = MP.get_context('fork')
    process = ctx.Process( target=self.challenge, args=( logging.getLogger("rts").Q, payload ) )
    #process.daemon = True
    process.start()

    if sleep:
      time.sleep( sleep ) # heating-up

    return process

  def runTest(self):

    with Indicator( ' Проведение тестов с нагрузкой CPU...'):
      log.info( '\n  Начинается проверка ограничений CPU...' )

      p = self.Go( self.payload_cpu )
      experiment1 = self.get_loaded_cores()
      log.info( '  Определено кол-во нагруженных(>75%) вычислительных ядер: {}'.format( experiment1 ) )
      p.join()

      self.set_param( 'cpu.cfs_quota_us',  '10000'  ) # < 10% per core
      self.set_param( 'cpu.cfs_period_us', '100000' )

      self.set_group( os.getpid() )

      p = self.Go( self.payload_cpu )
      experiment2 = self.get_loaded_cores()
      log.info( '  Определено кол-во нагруженных(>75%) вычислительных ядер: {}'.format( experiment2 ) )
      p.join()

    self.unset_group( os.getpid() )
    self.assert_( experiment1 > experiment2, "Не удалось ограничить процессу использование вычислительных ядер CPU.")

    # ------------------------------------------------------------------------------------------------------------------

    with Indicator( ' Проведение тестов с нагрузкой RAM...' ):
      log.info( '\n  Начинается проверка ограничений RAM...' )

      p = self.Go( self.payload_ram )
      experiment1 = self.get_loaded_ram( p.pid )
      log.info( '  Определено кол-во используемой RAM: {}'.format( experiment1 ) )
      p.join()


      self.set_param( 'memory.limit_in_bytes', '100M'  )
      self.set_param( 'memory.memsw.limit_in_bytes', '100M'  )
      # self.set_param( 'memory.soft_limit_in_bytes', '10M'  )
      self.set_param( 'memory.swappiness', '0' )
      self.set_param( 'memory.oom_control', '1' )
      # self.set_param( 'memory.use_hierarchy', '1' )
      self.set_group( os.getpid() )

      try:
        p = self.Go( self.payload_ram )
        experiment2 = self.get_loaded_ram( p.pid )
        log.info( '  Определено кол-во используемой RAM: {}'.format( experiment2 ) )
        p.join()

      except OSError as e:
        p.terminate()
        if e.errno == 12:
          experiment2 = 0
          log.info( '  Определено кол-во используемой RAM: {}'.format( experiment2 ) )
        else:
          self.fail( 'Произошла непредвиденная ошибка [{}]: {}'.format( e.strerror, e.errno ) )

    self.unset_group( os.getpid() )
    self.assert_( experiment1 - experiment2 > 10, "Не удалось ограничить процессу использование RAM.")

    # ------------------------------------------------------------------------------------------------------------------

    with Indicator( ' Проведение тестов с нагрузкой IO...' ):
      log.info( '\n  Начинается проверка ограничений IO...' )

      p = self.Go( self.payload_io )
      p.join()

      pth = Path( self.TMP_FILE )
      experiment1 = pth.stat().st_size
      pth.unlink()
      log.info( '  Определено кол-во записанных байт IO: {}'.format( experiment1 ) )

      self.set_param( 'blkio.throttle.read_bps_device',  '1:5 10000000' ) # /dev/zero
      self.set_param( 'blkio.throttle.read_iops_device', '1:5 1000000' )

      self.set_param( 'blkio.throttle.write_bps_device', '8:0 10000000' ) # /dev/sda
      self.set_param( 'blkio.throttle.write_iops_device', '8:0 1000000' )

      self.set_param( 'blkio.weight', '100' ) # min
      self.set_param( 'blkio.weight_device', '1:5 100' ) # min

      self.set_group( os.getpid() )

      p = self.Go( self.payload_io, 0 )
      p.join()

      pth = Path( self.TMP_FILE )
      experiment2 = pth.stat().st_size
      pth.unlink()
      log.info( '  Определено кол-во записанных байт IO: {}'.format( experiment2 ) )

    self.unset_group( os.getpid() )
    self.assert_( experiment1 - experiment2 > 30000000, "Не удалось ограничить процессу использование IO.")


  def tearDown(self):

    self.set_param( 'memory.force_empty', '0' )

    res = X( 'cgdelete cpu,cpuset,memory,blkio:/test', Return.process )

    self.assert_( res.returncode == 0, 'Не удалось удалить группы [{}]:{}'.format( res.returncode, res.stdout.read() ) )

class testFileLocking(BaseTest):
  """ #536 """

  tool = '/usr/lib64/rosa-test-suite/file-locking' + ' -g'

  def runTest(self):

    res = X( self.tool, Return.process )

    log.info( '  Вывод проверяющей утилиты:' )
    for l in res.stdout.readlines():
      log.info( '    ' + l.strip('\n') )

    self.assert_( res.returncode == 0, 'Родительский процесс потерял доступ к открытому файлу.' )

    log.info( '  Родительский процесс смог прочитать свой открытый файл после модификации и удаления его процессом-потомком.' )

class testMemoryRandomMapping(BaseTest):
  """ #537 """

  conf = '/proc/sys/kernel/randomize_va_space'
  tool = '/usr/lib64/rosa-test-suite/page-translator'

  # virtual memory page number for testing
  PFN = 3

  @classmethod
  def getRealAddr(self, pid:int, virtual_address_hex:str):
    """ return: "file_or_shared(bool) | physical address" """

    ans = X( '{} {} {}'.format( self.tool, pid, virtual_address_hex ), Return.process )

    #self.assertTrue( ans.returncode == 0, 'Не удалось найти физический адрес для {} : '.format( pid, virtual_address_hex ) )

    if ans.returncode == 0:
      ans = ans.stdout.readline().strip('\n').split('|')
    else:
      ans = [ -1, '0' ]

    return ans

  @classmethod
  def self_check_one(self, q):
    _log = logging.getLogger()
    _log.setLevel( logging.DEBUG )
    _log.addHandler( QueueHandler(q) )

    _log.info( '  Создан процесс A: {}'.format( os.getpid() ) )

    #self.printMemMap( os.getpid() )

    addr = psutil.Process( os.getpid() ).memory_maps(False)[self.PFN].addr.split('-')[0]
    addr = ( addr, self.getRealAddr( os.getpid(), '0x' + addr )[1] )

    sleep(5)

    return addr

  @classmethod
  def self_check_two(self, q):
    # just-in-case block
    FAKE_TEXT_SEGMENT_CHANGES = 'fake .text segment changes'
    if True is not False:
      if False != True:
        FAKE_TEXT_SEGMENT_CHANGES = not False or True

    _log = logging.getLogger()
    _log.setLevel( logging.DEBUG )
    _log.addHandler( QueueHandler(q) )

    _log.info( '  Создан процесс B: {}'.format( os.getpid() ) )

    #self.printMemMap( os.getpid() )

    addr = psutil.Process( os.getpid() ).memory_maps(False)[self.PFN].addr.split('-')[0]
    addr = ( addr, self.getRealAddr( os.getpid(), '0x' + addr )[1] )

    return addr

  @staticmethod
  def printMemMap(pid=0, counts=8):

    if pid <= 0:
      for proc in psutil.process_iter():
        print( proc.name() )
        for num, nmap in enumerate( proc.memory_maps(False) ):
          print( '\t' + nmap.addr )
          if num == counts: break
    else:
      proc = psutil.Process( pid )
      for num, nmap in enumerate( proc.memory_maps(False) ):
        print( '\t' + nmap.addr )
        if num == counts: break

  def runTest(self):

    log.info( "  Проверка значения в {}...".format( self.conf ) )

    with open( self.conf, 'r' ) as f:
      ans = f.read(1)

      self.assertTrue( ans in ['1','2'], 'randomize_va_space: значение не равно 1 или 2 (получено:{}).'.format( ans ) )
      log.info( '  Проверка пройдена, значение равно {}.'.format( ans ) )

    counter   = Counter([''])
    procs     = {}
    whitelist = [ 'vmtoolsd',
                  'VGAuthService',
                  'wsdd-wrapper',
                  'vmware-vmblock-fuse',
                  'kded4',
                  'kdeinit4',
                  'python3.5'
                ] # interconnected processes

    log.info( "{:^40} {:^6}  \t{:^14} {:^14} {:^11} ".format('Процесс', 'PID', 'Virt', 'Real', 'File|Shared' ))

    with Indicator( ' Выполняется проверка существующих процессов...' ):
      for proc in psutil.Process(1).children(): #  avoid shared pages with children
        try:

          virt                 = proc.memory_maps(False)[0].addr.split('-')[0]
          file_or_shared, real = self.getRealAddr( proc.pid, '0x' + virt )

          if real == '0':
            continue

          log.info( "{:40} {:6}  \t0x{:12} 0x{:12} {:^11} "\
                .format( proc.name(), proc.pid, virt, real, file_or_shared ) )

          counter[real] += 1

          if counter[real] <= 1:
            procs[real] = proc.name()
          elif procs[real] == proc.name():
            continue
          elif proc.name() in whitelist or procs[real] in whitelist:
            continue
          else:
            self.assertTrue( False, 'Найдены совпадения физических адресов: {} и {} имеют общий физический адрес 0x{}'\
                                    .format( proc.name(), procs[ real ], real ) )

        except IndexError:
          continue
        except ( psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess ):
          continue

    log.info( '  Совпадения физических адресов памяти не обнаружены.' )
    log.info( '  Проверка случайности адресов памяти новых процессов...')


    with Indicator( ' Выполняется проверка новых процессов...' ):
      ctx = MP.get_context('spawn')
      with ctx.Pool( processes=2 ) as pool:
        res = [ pool.apply_async( self.self_check_one, (logging.getLogger("rts").Q,) ),
                pool.apply_async( self.self_check_two, (logging.getLogger("rts").Q,) )  ]
        res = [ '0x' + res[0].get()[0], '0x' + res[0].get()[1],
                '0x' + res[1].get()[0], '0x' + res[1].get()[1]  ]

    self.assertTrue( res[1] != res[3], 'Новые процессы имеют общие физические адреса ОЗУ {} == {} при виртуальных {} == {} !'
                                       .format( res[1], res[3], res[0], res[2] ) )

    log.info( '  Новые процессы НЕ имеют общий физический адрес: {} != {}, при виртуальных {} == {}'
              .format( res[1], res[3], res[0], res[2]  ) )

  def tearDown(self):
    pass

class testApacheComplex(BaseTest):

  url             = 'http://localhost'
  site_path_www   = '/var/www/html/'
  test_page       = '/usr/share/rosa-test-suite/test_data/web/index.py'
  test_conf       = '/usr/share/rosa-test-suite/test_data/web/apache.conf'

  orig_page       = '/var/www/html/index.html'
  orig_conf       = '/etc/httpd/conf/httpd.conf'

  test_values     = ['hello tester!','time to done your work','Bye!']
  test_user       = 'tester'
  test_pass       = 'TopSecret'
  test_db         = 'testdb'
  test_table      = 'testtable'
  test_util       = '/usr/lib64/rosa-test-suite/xtool'
  stream_backup   = sys.stderr

  def genUrlGET(self):

    out = self.url + '/?'

    out += 'db={}&'.format( self.test_db )
    out += 'u={}&'.format( self.test_user )
    out += 'p={}&'.format( self.test_pass )
    out += 'h=localhost&'

    for i, v in enumerate( self.test_values ):
      out += 'f{}={}&'.format( i, v ).replace(' ', '+')
    out = out[:-1] # delete last coma

    return out

  def genInsertInto(self):

    out = 'insert into {}(id, message) values '.format( self.test_table )

    for i, v in enumerate( self.test_values ):
      out += '({},\'{}\'),'.format( i, v )
    out = out[:-1] # delete last coma

    return out

  def runTest(self):

    log.info('Начинается комлексное тестирование веб-сервера Apache + СУБД PostgreSQL + Python')

    log.info('  Создание стартовой страницы веб-сервера...')
    shutil.copy2( self.test_page, self.site_path_www)
    os.chmod( self.site_path_www + os.path.split( self.test_page )[1], int( '755', base=8) )

    log.info('  Создание резервной копии конфигурации веб-сервера...')
    fileBackup( self.orig_conf, replacement=self.test_conf)

    plan = Terminal( StreamToLog( log ), flush=False)

    plan.add( 'systemctl restart postgresql11.service') \
        .info( '  Перезапуск СУБД PostgreSQL\n')

    plan.add( 'echo "create user {} with password \'{}\'" | sudo -u postgres psql'
              .format( self.test_user, self.test_pass )) \
        .info('  Создание тестового пользователя базы данных...\n')

    # plan.add( 'echo "\du" | sudo -u postgres psql'
    #           .format( self.test_user, self.test_pass ))

    plan.add( 'echo "create database {}" | sudo -u postgres psql'
              .format( self.test_db )) \
        .info('  Создание тестовой базы данных...\n')

    plan.add( 'echo "grant all privileges on database {} to {}" | sudo -u postgres psql'
              .format( self.test_db, self.test_user )) \
        .info('  Предоставление всех привилегий над базой данных тестовому пользователю...\n')

    plan.add( 'systemctl restart postgresql11.service') \
        .info( '  Перезапуск базы данных PostgreSQL...\n')

    plan.add( 'echo "create table {}(id int PRIMARY KEY, message TEXT)" | sudo -u postgres psql -h localhost {} {}'
              .format( self.test_table, self.test_db, self.test_user )) \
        .info( '  Создание тестовой таблицы...\n')

    plan.add( 'echo "{}" | sudo -u postgres psql -h localhost {} {}'
              .format( self.genInsertInto(), self.test_db, self.test_user )) \
        .info( '  Создание тестовых данных...\n')

    plan.add( 'systemctl restart httpd.service' ).info( '  Перезагрузка веб-сервера...\n')
    plan.add( 'apachectl -k restart' )
    plan.add( 'systemctl restart postgresql11.service' )

    plan.add( 'sudo -u {} chromium-browser "{}" '
              .format( User().name, self.genUrlGET() ), separated=True, ignore_output=True, pause_before=7 ) \
        .info( '  Запуск браузера для проверки веб-сервера...\n')

    plan.add( self.test_util + ' "TEST PASSED!" -t 5000', separated=True, pause_before=15 ) \
        .info( '  Запуск проверяющей утилиты в режиме ожидания 20 сек...\n')

    plan.run()

    for l in plan.log:
      self.assertEqual( l[0], 0, 'Во время тестирования произошла ошибка:\n{}'.format( l[1] ) )


  def tearDown(self):

    log.info('  Принудительное завершение работы браузера (если требуется)...')
    processEx('chrome', kill=True )

    log.info('  Удаление тестовой базы данных и тестового пользователя...')
    planb = Terminal( stream=sys.stderr )

    planb.add( 'echo "drop database {}" | sudo -u postgres psql'
               .format( self.test_db ))

    planb.add( 'echo "drop user {}" | sudo -u postgres psql'
               .format( self.test_user ))

    planb.run()

    log.info('  Удаление тестовой стартовой страницы веб-сервера...')
    os.remove( self.site_path_www + os.path.split( self.test_page )[1] )

    log.info('  Восстановление конфигурации веб-сервера...')
    fileBackup( self.orig_conf, restore=True)

    pass

  pass

class testApache(BaseTest):

  site_path_www   = '/var/www/html'
  test_page       = '/usr/share/rosa-test-suite/test_data/web/test.html'
  orig_page       = '/var/www/html/index.html'
  test_util       = '/usr/lib64/rosa-test-suite/xtool'
  test_util_flags = ' "TEST PASSED" -t 15000'

  def runTest(self):

    log.info('Начинается тестирование сервера Apache')

    log.info('  Создается резервная копия страницы index.html на сервере...')
    fileBackup( self.orig_page, replacement=self.test_page)

    log.info('  Принудительное завершение работы браузера (если требуется)...')
    processEx('chrome', kill=True)

    waitCPU( silent=True )

    log.info('  Перезапуск сервера Apache и открытие тестовой страницы...')
    with Terminal( StreamToLog( log ) ) as plan:
      plan.add( 'apachectl -k restart' )
      plan.add( 'sudo -u {} chromium-browser http://127.0.0.1 --no-sandbox'.format( User().name ), separated=True, ignore_output=True )
      plan.add( self.test_util + ' "TEST PASSED" -t 15000' )


    #plan.print_log()

    for l in plan.log:
      self.assertEqual( l[0], 0, 'Во время тестирования произошла ошибка.\n{}'.format( l[1] ) )

    log.info('Тест успешной пройден.')
    pass

  def tearDown(self):

    log.info('  Принудительное завершение работы браузера (если требуется)...')
    processEx('chrome', kill=True )

    log.info('  Восстановление исходной страницы index.html на сервере...')
    fileBackup( self.orig_page, restore=True)

    pass

class testSMB(BaseTest):

  _conf    = '/etc/samba/smb.conf'
  old_conf = '/etc/samba/smb.conf.bkp'
  new_conf = '/usr/share/rosa-test-suite/test_data/samba_conf.conf'

  smb_dir       = '/var/tmp/smb'
  smb_share_dir = smb_dir + '/test_secure_share'

  test_file_name = 'welcome.txt'
  test_file_path = '{}/{}'.format( smb_share_dir, test_file_name )
  test_content_l = 'Hello, samba client!'
  test_content_r = '\nHello, samba server!'

  user      = 'samba_user'
  user_home = '/home/{}/'.format( user )
  password  = 'topsecret'

  def runTest(self):
    log.info('Начинается тестирование samba...')

    log.info('  Создается резервная копия конфигурации samba...')
    fileBackup( self._conf, replacement=self.new_conf)

    log.info('  Создается тестовая группа...')
    groupCreate('test_smbgrp') # in /test_data/samba_conf.conf

    log.info('  Создается тестовый пользователь...')
    U = User( name=self.user, password=self.password, home=self.user_home, se_user='user_u', se_range='s0' )
    U.addGroup( 'test_smbgrp' )

    log.info('  Импорт тестового пользователя в базу samba...')
    res = X( 'echo -e "{p}\n{p}" | smbpasswd -s -a {u}'
              .format( p=self.password, u=self.user ), return_type=Return.process)

    self.assertEqual( res.returncode, 0,
                      'При импорте тестового пользователя произошла ошибка: {}'
                      .format( res.stdout.read() ) )

    log.info('  Создаются тестовые файлы для сервера samba...')
    os.mkdir( self.smb_dir )
    os.mkdir( self.smb_share_dir )
    with open( self.test_file_path, 'w' ) as f:
      f.write( self.test_content_l )

    log.info('  Изменяются права на тестовые файлы для сервера samba...')
    chmodR( self.smb_share_dir, '770' )
    chownR( self.smb_share_dir, 'root', 'test_smbgrp')

    log.info('  Перезагружается сервер samba...')
    res = X( 'systemctl restart smb.service', return_type=Return.process )
    if res.returncode:
      log.error('При перезагрузке сервера samba произошла ошибка:\n'
                   .format( res.stdout.read() ) )

    log.info('  Получение файла с сервера samba...')

    res = X( 'smbclient -U {u}%{p} //127.0.0.1/Secure -c "get {rf} {f}"'
             .format( u=self.user, p=self.password, rf=self.test_file_name, f=self.user_home + self.test_file_name ),
             return_type=Return.process )

    self.assert_( res.returncode == 0, 'При получении файла с сервера samba произошла ошибка: {}'
                                       .format( res.stdout.read() ) )

    log.info('  Проверка содержимого полученного файла.')
    with open( self.user_home + self.test_file_name, "r") as f:
      welcome = f.readlines()[0]
    self.assertEqual( welcome, self.test_content_l,
                      'Содержимое файла полученного с сервера samba "{}" не соответствует "{}". '
                      .format( welcome, self.test_content_l ) )

    log.info('  Добавление нового контента в полученный файл.')
    with open( self.user_home + self.test_file_name, "a") as f:
      f.write( self.test_content_r )

    log.info('  Отправка файла на сервер samba...')
    res = X( 'smbclient -U {u}%{p} //127.0.0.1/Secure -c "put {f} {rf}" '
             .format( u=self.user, p=self.password, f=self.user_home + self.test_file_name, rf=self.test_file_name ),
             return_type=Return.process )

    self.assert_( res.returncode == 0, 'При отправке файла на сервер samba произошла ошибка: {}'.format( res.stdout.read() ) )


    log.info('  Проверка содержимого отправленного файла на сервере samba.')
    with open( self.test_file_path, "r") as f:
      welcome = f.readlines()[1]
    self.assertEqual( welcome, self.test_content_r.replace( '\n', '' ),
                      'Содержимое файла на сервере samba "{}" не соответствует "{}". '
                      .format( welcome, self.test_content_r.replace( '\n', '' ) ) )


  def tearDown(self):
    log.info('  Восстановление исходной конфигурации сервера samba...')
    fileBackup( self._conf, restore=True)

    log.info('  Удаление тестовой группы test_smbgrp...')
    groupDelete( 'test_smbgrp' )

    log.info('  Удаление тестовой директории "{}" ...'.format( self.smb_dir ) )
    if os.path.exists( self.smb_dir ):
      shutil.rmtree( self.smb_dir )

    log.info('Удаление отладочной информации завершено успешно.'.format( self.user ) )

class testArchMulticore(BaseTest):
    """
    Тест поддержки 64-разрядных архитектур х86_64, в т. ч.
    многоядерных и многопроцессорных конфигураций
    """
    samplename = "python"

    def runTest(self):
        if getCPUsNum() == 1:
            self.fail("В системе есть только одно процессорное ядро.")

        cpu_mask = 2
        proc = subprocess.Popen("taskset %d %s" % (cpu_mask, self.samplename),
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT, shell=True)
        sleep(1)
        errcode, output = getstatusoutput("taskset -p %s" % str(proc.pid))
        proc.kill()

        self.assertEqual(errcode, 0, "Получить привязку процесса к CPU не удалось.\n" + output )

        log.debug("  Привязка процесса к CPU: %s" % output )
        cpu_mask_cur = int(output.split(": ")[1])

        self.assertEqual(cpu_mask_cur, cpu_mask,
                         "Маска привязки процесса к ядрам (%d)"
                         " отличается от заданной (%d)." %
                         (cpu_mask_cur, cpu_mask))

class testRAID(BaseTest):
  """
  Тест возможности программной организации дисковых разделов в RAID
  уровней 0, 1, 5 и их комбинаций, а также томов LVM
  http://www.anthonyldechiaro.com/blog/2010/12/19/lvm-loopback-how-to/
  """

  def setUp(self):
    super(testRAID, self).setUp()

    self.diskFiles = ['/var/tmp/virtpart1', '/var/tmp/virtpart2', '/var/tmp/virtpart3']
    self.devices = []
    self.raidDev = '/dev/md0'

    for df in self.diskFiles:
      errcode, output = getstatusoutput( 'dd if=/dev/zero of={} bs=1024 count=30720'.format( df ) )
      self.assertEqual(errcode, 0, output)
      # find and change free loop device
      errcode, device = getstatusoutput( 'losetup -f' )
      self.assertEqual(errcode, 0, device)
      self.devices.append(device)
      # attach loopback device with disk file
      errcode, output = getstatusoutput( 'losetup {} {}'.format( device, df ) )
      self.assertEqual(errcode, 0, output)
    pass

  def delRaid(self, dev):

    errcode, output = getstatusoutput( 'mdadm -S {}'.format( dev ) )
    self.assertEqual( errcode, 0, 'Ошибка остановки устройства RAID:\n{}'.format( output ) )
    for device in self.devices:
      errcode, output = getstatusoutput( 'mdadm --zero-superblock {}'.format( device ) )
      self.assertEqual( errcode, 0, 'Ошибка размонтирования раздела RAID:\n{}'.format( output ) )
    pass

  def runTest(self):

    for level in [1, 0]:
      errcode, output = getstatusoutput(  'mdadm --create {} '.format( self.raidDev )
                                           + '--metadata=0.90 --level={} '.format( level )
                                           + '--raid-devices=2 {} {}'.format( self.devices[0], self.devices[1] )
                                        )

      self.assertEqual( errcode, 0, 'Ошибка создания RAID-массива уровня {}.'.format( level ) )
      sleep(1)

      for line in X('cat /proc/mdstat', return_type=Return.stdout).split(';'):
        log.info( '  {}'.format( line ) )

      self.delRaid(self.raidDev)

    errcode, output = getstatusoutput( 'mdadm --create {} --raid-devices=3 '.format( self.raidDev )
                                       + '--level=5 {} {} {}'\
                                         .format( self.devices[0], self.devices[1], self.devices[2] )
                                      )

    self.assertEqual(errcode, 0, "Ошибка создания RAID массива уровня 5.")
    sleep(1)

    for line in X('cat /proc/mdstat', return_type=Return.stdout).split(';'):
      log.info( '  {}'.format( line ) )

    self.delRaid( self.raidDev )


  def tearDown(self):
    log.info('  Восстановление исходных параметров...')
    errcode, output = getstatusoutput( 'mdadm -S {}'.format( self.raidDev ) )
    #log.info(f'  {output}')
    sleep(1)
    for df in self.devices:
      errcode, output = getstatusoutput( 'losetup -d {}'.format( df ) )
      logIfError(errcode, output)
      self.assertEqual(errcode, 0, output)
    for filepath in self.diskFiles:
      try:
        os.remove(filepath)
      except OSError as e:
        log.exception(str(e))
    pass

class testFS(BaseTest):
  """
  Тест поддержки журналируемой файловой системы
  """

  def setUp(self):
    super(testFS, self).setUp()

    self.diskFile = Path('/var/tmp/rosa-test-suite/testFSF')
    self.device = None
    self.mntpoint = None

  def runTest(self):

    self.diskFile.parent.mkdir( parents=True, exist_ok=True )
    self.diskFile.touch( exist_ok=True )

    errcode, output = getstatusoutput( "dd if=/dev/zero of={} bs=1024 count=30720".format( self.diskFile.as_posix() ) )

    self.assertEqual(errcode, 0, output)
    # find and change free loop device
    errcode, self.device = getstatusoutput("losetup -f")
    self.assertEqual(errcode, 0, self.device)
    # attach loopback device with disk file
    errcode, output = getstatusoutput("losetup %s %s" %
                                      (self.device, self.diskFile))
    self.assertEqual(errcode, 0, output)
    # create a EXT3 file system with 1% reserved block count
    #  on the loopback device
    errcode, output = getstatusoutput("mkfs -t ext4 -m 1 -v %s" %
                                      self.device)
    self.assertEqual(errcode, 0,
                      "Ошибка создания файловой системы:\n%s" % output)
    # create a directory (as mount point)
    self.mntpoint = mkdtemp()
    # mount the loopback device (disk file) to mntpoint
    errcode, output = getstatusoutput("mount -t ext4 %s %s" %
                                      (self.device, self.mntpoint))
    self.assertEqual(errcode, 0, "Ошибка монтирования:\n%s" % output)

  def release(self):
    if self.mntpoint:
      getstatusoutput("umount %s" % self.mntpoint)
      self.mntpoint = None
    if self.device:
      getstatusoutput("losetup -d %s" % self.device)
      self.device = None
    try:
      self.diskFile.unlink()
    except OSError as e:
      log.exception(e.message)

  def tearDown(self):
    self.release()

class testNAT(BaseTest):
    """
    Тест поддержки механизма преобразования сетевых адресов NAT
    (RFC 1631, RFC 3022)
    """
    def runTest(self):

        log.info('Тест поддержки механизма преобразования сетевых адресов NAT.')

        test_util = '/usr/lib64/rosa-test-suite/test_NAT'

        cmd_setup_pull = [
            'ip netns add R',
            'ip netns add V1',
            'ip netns add V2',

            'ip link add name con1a type veth peer name con1b',
            'ip link add name con2a type veth peer name con2b',

            'ip link set dev con1a netns R',
            'ip link set dev con2a netns R',
            'ip link set dev con1b netns V1',
            'ip link set dev con2b netns V2',

            'ip netns exec R ip address add 192.168.1.1/24 dev con1a',
            'ip netns exec R ip address add 192.168.2.1/24 dev con2a',
            'ip netns exec V1 ip address add 192.168.1.2/24 dev con1b',
            'ip netns exec V2 ip address add 192.168.2.2/24 dev con2b',

            'ip netns exec R ip link set dev con1a up',
            'ip netns exec R ip link set dev con2a up',
            'ip netns exec V1 ip link set dev con1b up',
            'ip netns exec V2 ip link set dev con2b up',

            'ip netns exec V1 ip route add default via 192.168.1.1',
            'ip netns exec V2 ip route add default via 192.168.2.1',

            'ip netns exec R sysctl -w net.ipv4.ip_forward=1 -q',

            'ip netns exec R iptables -F',
            'ip netns exec R iptables -P FORWARD DROP'
        ]
        cmd_nat_ON = [
            'ip netns exec R iptables -A FORWARD -o con1a -i con2a -j ACCEPT',
            'ip netns exec R iptables -A FORWARD -i con1a -o con2a -j ACCEPT'
        ]

        # setup
        log.debug("Настройка виртуальной сетевой инфраструктуры")
        for cm in cmd_setup_pull:
            log.debug("Executing: " + cm)
            self.assertEqual( subprocess.call(cm, shell=True), 0, 'Ошибка при настройке виртуальной сетевой инфраструктуры!')

        # check line with NAT OFF
        log.debug("Проверка работы виртуальной сетевой инфраструктуры с отключенной NAT")
        p = psutil.Popen( test_util, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT )
        self.assertEqual( p.wait(), 1, 'Проверка не пройдена! NAT уже включена!')
        log.info( 'Вывод тестирующей утилиты: \n\n{}\nУСПЕШНО!'.format( p.stdout.read().decode("utf-8") ) )

        # NAT turn ON
        log.debug("Проверка работы виртуальной сетевой инфраструктуры со включенной NAT")
        for cm in cmd_nat_ON:
            log.debug("Executing: " + cm)
            self.assertEqual( subprocess.call(cm, shell=True), 0, 'Ошибка при настройке iptables!')

        # check line with NAT ON
        p = psutil.Popen( test_util, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT )
        self.assertEqual( p.wait(), 0, 'Проверка не пройдена! Нет соединения!')
        log.info( 'Вывод тестирующей утилиты: \n\n{}\nУСПЕШНО!'.format( p.stdout.read().decode("utf-8") ) )

        log.info('Проверка поддержки механизма преобразования сетевых адресов NAT пройдена успешно.')

    def tearDown(self):

        log.debug("Удаление виртуальной сетевой инфраструктуры")
        cmd_clear = ['ip netns del R', 'ip netns del V1', 'ip netns del V2']

        for cm in cmd_clear:
            log.debug("Executing: " + cm)
            self.assertEqual( subprocess.call(cm, shell=True), 0, 'Ошибка при очистке виртуальной сетевой инфраструктуры!')

class testCdRom(BaseTest):
  """
  Тест поддержки устройств чтения с оптических носителей CD+/-, DVD+/-
  """

  def setUp(self):
    super(testCdRom, self).setUp()
    self.mntDir = mkdtemp()

  def runTest(self):

    with Indicator( ' Тестирование устройств чтения оптических дисков...' ):
      log.info('  Поиск доступного устройства чтения оптических дисков...')

      roms = dict()

      code, out = x( "cat /proc/sys/dev/cdrom/info | grep 'drive name'", Return.codeout )
      self.assert_( code == 0, 'При поиске устройства чтения оптических дисков произошла ошибка [{}]: {} '.format( code, out ) )

      for name in out.split( ':' )[-1].split():
        roms[ '/dev/' + name ] = 'noinfo'

      self.assert_( len(roms) > 0, '  Ни одного устройства чтения оптических дисков не обнаружено')

      for r in roms.keys():
        code, out = x( "udisksctl info -b {} | grep 'Drive'".format( r ), Return.codeout )
        self.assert_( code == 0, 'При получении информации об устройстве "{}" произошла ошибка [{}]: {} '.format( r, code, out ) )

        roms[r] = out.split( '/' )[-1]

        log.info( '  Обнаружено устройство чтения оптических дисков: {} : {}'.format( r, roms[r] ) )
        log.info( '  Производится попытка монтирования в {}...'.format( self.mntDir ) )

        code, out = x( 'mount {} {}'.format( r, self.mntDir ), Return.codeout )
        self.assert_( code == 0, 'При монтировании устройства произошла ошибка [{}]: {} '.format( code, out ) )

        log.info( '  Производится размонтирование устройства...' )

        code, out = x( 'umount {}'.format( self.mntDir ), Return.codeout )
        self.assert_( code == 0, 'При размонтировании устройства произошла ошибка [{}]: {} '.format( code, out ) )

        log.info( '  Устройство прошло проверку.' )

        #bonus
        x( 'eject' )

class testBluRay(BaseTest):
    """
    Тест поддержки поддержки устройств чтения с оптических носителей Blu-Ray
    """

    def setUp(self):
        super(testBluRay, self).setUp()

        mPath = os.path.dirname(os.path.realpath(__file__))
        self.iso = os.path.join(mPath, 'test_data', 'udf', 'udf_2.50.iso')
        self.mntDir = mkdtemp()

    def runTest(self):
        log.info('Начинается проверка файловой системы UDF 2.50, '
                    'применяемой на дисках Blu-ray.')
        log.debug('  Подключение ISO-образа с файловой системой UDF 2.50...')
        errcode, output = getstatusoutput('mount -o loop -t udf %s %s' %
                                          (self.iso, self.mntDir))
        self.assertEqual(errcode, 0, 'Подключить образ диска с UDF 2.50 '
                                     'не удалось.\n' + output)
        log.debug('  Чтение тестового файла...')
        with open(os.path.join(self.mntDir, 'README'), 'r') as f:
            file_content = f.read()
        log.debug('  Содержимое тестового файла:\n' + file_content)
        log.info('Проверка поддержки файловой системы UDF 2.50 '
                    'пройдена успешно.')

    def tearDown(self):
        getstatusoutput('umount %s' % self.mntDir)

class testNvidiaAti(BaseTest):
    """
    Тест возможности использования драйверов для современных
    высокопроизводительных графических ускорителей (ATI, Nvidia)
    """

    def runTest(self):
        cmd = 'glxinfo'
        errcode, output = getstatusoutput(cmd)
        logIfError(errcode, 'Cmd: %s Error: %s' % (cmd, output))
        self.assertEqual(0, errcode, output)
        res = False
        # "ATI" and "NVIDIA" are for proprietary drivers
        # "nouveau" is a free NVIDIA driver
        # As for free drivers for ATI cards, different card/drivers can
        # return different values and we try to check them all here
        for item in ["ATI", "NVIDIA", "nouveau", "radeon", "X.Org R300"]:
            if "vendor string: %s" % item in output:
                res = True
                break
        if re.search('renderer string: .* on (AMD|ATI)', output):
            res = True

        self.assertTrue(res, 'Возможность использования драйверов для '
                             'современных высокопроизводительных графических '
                             'ускорителей (ATI, Nvidia) в данной конфигурации оборудования отсутствует.')

class testSLIP_PPP(BaseTest):
  """
  Тест поддержки управления коммутируемыми линиями SLIP, PPP средствами ядра
  """

  socat = None
  sl0   = None
  sl1   = None
  ppp0  = None
  ppp1  = None

  def runTest(self):

    toggleRosaDeviceManager()

    with Indicator( ' Тестирование SLIP...' ):

      log.info( 'Начинается проверка управления линиями SLIP...' )
      log.debug( '  Связывание последовательных портов (socat)...' )

      self.socat = subprocess.Popen( [ 'socat PTY,link=/dev/ttyS10 PTY,link=/dev/ttyS11' ],
                                      stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE,
                                      shell=True )
      sleep(3); os.sync()

      if self.socat.poll() is not None:
        self.fail( 'Ошибка при запуске socat.\nСообщения программы:\n' + self.socat.stderr.read() )

      log.debug( '  Подключение сетевого интерфейса к последовательному порту 0 (slattach)...' )

      self.sl0 = subprocess.Popen( [ 'slattach -p slip /dev/ttyS10' ],
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE,
                                    shell=True)

      log.debug( '  Подключение сетевого интерфейса к последовательному порту 1 (slattach)...' )

      self.sl1 = subprocess.Popen( ['slattach -p slip /dev/ttyS11'],
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE,
                                    shell=True )

      sleep(3); os.sync()
      log.debug( '  Настройка IP...' )
      errcode, output = getstatusoutput('ifconfig sl0 10.255.0.100 && ifconfig sl1 10.255.0.101')

      self.assertEqual( errcode, 0, 'Назначить IP-адреса устройствам sl0/sl1 не удалось.\n{}'.format( output ) )

      log.debug( '  Проверка соединения...' )
      errcode, output = getstatusoutput( 'ping -c 1 -r -W 1 10.255.0.101 -I sl0' )
      log.debug( '  Получение информации об интерфейсе sl1...' )

      errcode, output = getstatusoutput( 'ifconfig sl1' )
      self.assertEqual( errcode, 0, 'Получить информацию об интерфейсе sl1 не удалось. {}'.format( output ) )

      lere = None
      rxre = None

      # sl1: flags=4305<UP,POINTOPOINT,RUNNING,NOARP,MULTICAST>  mtu 296
      # inet 10.255.0.101  netmask 255.255.255.255  destination 10.255.0.101
      # slip  txqueuelen 10  (Serial Line IP)
      # RX packets 24  bytes 5228 (5.1 KiB)
      # RX errors 0  dropped 0  overruns 0  frame 0
      # TX packets 22  bytes 5060 (4.9 KiB)
      # TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

      for line in output.split( '\n' ):
        if not (lere):
          lere = re.search( r'slip\s+.*\((.*?)\)\s*$', line )
          if not lere:
            lere = re.search( r'^sl1\s+Link encap:(.*?)\s*$', line )
          if lere:
            self.assertEqual( lere.group(1), 'Serial Line IP', 'Интерфейс sl1 использует неверную инкапсуляцию ({}).'
                                                              .format( lere.group(1) ) )
        if not (rxre):
          rxre = re.search( r'\s*RX packets?\s*(\d+)', line )
          if rxre:
            self.assert_( rxre.group(1) != '0', 'Интерфейс sl1 не получил пакетов.' )

      self.assertTrue( lere is not None, 'Получить тип инкапсуляции интерфейса sl1 не удалось.' )
      self.assertTrue( rxre is not None, 'Получить число пакетов для интерфейса sl1 не удалось.' )

      log.info( 'Проверка управления линиями SLIP пройдена успешно.' )
      finishProcess( self.sl0, 'slattach (0)' )
      finishProcess( self.sl1, 'slattach (1)' )
      finishProcess( self.socat, 'socat' )

    with Indicator( ' Тестирование PPP...' ):

      sleep(1)
      log.info( 'Начинается проверка управления линиями PPP...' )
      log.debug( '  Связывание последовательных портов (socat)...' )
      self.socat = subprocess.Popen( [ 'socat PTY,link=/dev/ttyS10 PTY,link=/dev/ttyS11' ],
                                      stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE,
                                      shell=True )
      sleep(1)
      if self.socat.poll() is not None:
          self.fail('Ошибка при запуске socat.\nСообщения программы:\n' + self.socat.stderr.read() )

      log.debug('  Установление принимающей стороны соединения PPP через последовательный порт 0 (pppd)...')

      self.ppp0 = subprocess.Popen( [ 'pppd -detach /dev/ttyS10 10.255.1.1: nomagic' ],
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE,
                                    shell=True )

      log.debug( '  Установление вызывающей стороны соединения PPP через последовательный порт 1 (pppd)...' )

      self.ppp1 = subprocess.Popen( ['pppd -detach /dev/ttyS11 10.255.1.2: nomagic'],
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE,
                                    shell=True )
      iface = None

      sleep(5)
      while not iface:
        pline = self.ppp1.stdout.readline().decode("utf-8")
        if pline:
          ifre = re.search(r'^Using interface\s+(.*)$', pline)
          if ifre:
            iface = ifre.group(1).strip()
        else:
          break

      self.assertTrue( iface is not None, 'Получить название интерфейса для вызывающей стороны соединения не удалось.\n'
                                          'Получено сообщение: ' + pline )

      log.debug( '  Получение информации об интерфейсе {}...'.format( iface ) )

      errcode, output = getstatusoutput( 'ifconfig {}'.format( iface ) )

      self.assertEqual( errcode, 0, 'Получить информацию об интерфейсе {} не удалось.\n{}'.format( iface, output ) )

      lere = None
      adre = None
      for line in output.split( '\n' ):
        if not (lere):
          lere = re.search( r'ppp\s+.*\((.*?)\)\s*$', line )
          if not lere:
            lere = re.search( '^' + iface + r'\s+Link encap:(.*?)\s*$', line )
          if lere:
            self.assertEqual( lere.group(1), 'Point-to-Point Protocol', 'Интерфейс {} использует неверную инкапсуляцию ({}).'
                                                                          .format( iface, lere.group(1) ) )
        if not (adre):
          adre = re.search( r'^\s*inet\s*([\d\.]+).*destination\s*([\d\.]+)', line )
          if not adre:
            adre = re.search( r'^\s*inet addr:([\d\.]+)\s+P-t-P:([\d\.]+)', line )
          if adre:
            self.assertTrue( adre.group(2) == '10.255.1.1', 'Через интерфейс {} установлено соединение с неверным адресом ({}).'
                                                            .format( iface, adre.group(2) ) )

      self.assertTrue( lere is not None, 'Получить тип инкапсуляции интерфейса {} не удалось.'.format( iface ) )
      self.assertTrue( adre is not None, 'Получить адреса интерфейса {} не удалось.'.format( iface ) )

      log.info('Проверка управления линиями PPP пройдена успешно.')

  def tearDown(self):
    finishProcess(self.sl0, 'slattach (0)')
    finishProcess(self.sl1, 'slattach (1)')
    finishProcess(self.ppp0, 'pppd (0)')
    finishProcess(self.ppp1, 'pppd (1)')
    finishProcess(self.socat, 'socat')

    log.info( '  Перезагрузка NetworkManager...' )
    code, out = x( 'systemctl restart networkmanager.service', Return.codeout )

    if code != 0:
      log.info( 'Не удалось перезагрузить NetworkManager [{}]: {}'.format( code, out ) )

    toggleRosaDeviceManager()

class testSERIAL8250(BaseTest):
    """
    Тест поддержки универсального асинхронного приѐмопередатчика UART
    в стандартах RS232/RS485
    """

    def setUp(self):
        super(testSERIAL8250, self).setUp()
        self.stremout = open("/var/tmp/testSERIAL8250F", 'w+b')
        self.ttyss = ['/dev/ttySm0', '/dev/ttySm1']
        self.proc = None

    def tearDown(self):
        self.stremout.close()
        for item in self.ttyss:
            try:
                os.unlink(item)
                if self.proc is not None:
                    self.proc.terminate()
            except OSError as e:
                log.exception(e.message)

    def runTest(self):
        self.proc = subprocess.Popen('socat -d -d pty,raw,echo=0 '
                                     'pty,raw,echo=0',
                                     stdout=self.stremout,
                                     stderr=self.stremout,
                                     shell=True)
        sleep(3)
        curPos = self.stremout.tell()
        self.stremout.seek(0)
        content = self.stremout.readlines()
        self.stremout.seek(curPos)
        i = 0
        for line in content:
            reg = re.search(r"/dev/pts/\d+", line.decode("utf-8"))
            if reg:
                pts = reg.group()
                os.symlink(pts, self.ttyss[i])
                i += 1

        with open(self.ttyss[0], 'wb') as f1:
            content1 = 'a'
            f1.write(pack('c'.encode(), content1.encode()))
        sleep(1)
        with open(self.ttyss[1], 'rb') as f2:
            content2 = unpack('c', f2.read(1))[0]
        self.assertEqual(content1, content2.decode())

class testSDIO_UART(BaseTest):
  """
  Тест поддержки стандарта обмена данными с периферийными устройствами
  через слот расширения формата SD SDIO
  """

  def setUp(self):
    super(testSDIO_UART, self).setUp()

    self.sdDev = '/dev/sdc1'
    self.mntDir = mkdtemp()

  def getExecutables(self):
    return ['mount -t vfat %s %s' % (self.sdDev, self.mntDir)]

  def tearDown(self):
    getstatusoutput('umount %s' % self.mntDir)

class testPciePortBus(BaseTest):
  """
  Тест поддержки высокоскоростного периферийного интерфейса PCI Express
  """

  pass_phrase = 'pcie_test: device has PCI-E capabilities.'

  def runTest(self):
    log.debug('  Получение списка PCI-устройств...')
    errcode, output = getstatusoutput('lspci -n -mm')
    logIfError(errcode, output)

    self.assertEqual(errcode, 0, 'Получить список PCI-устройств не удалось:\n%s' % output)

    pcie_found = False
    for line in output.split('\n'):
      devinfo = line.split(' ')
      vnd = devinfo[2].replace('"', '')
      dev = devinfo[3].replace('"', '')
      log.debug('  Проверка устройства {}:{}...'.format( vnd, dev ) )

      self.assert_( CURRTEST.clear_dmesg(), 'Очистить буфер dmesg не удалось:\n%s' % output)

      code, out = CURRTEST.run_kmodule(0,'vendor_id="0x{}" device_id="0x{}"'.format( vnd, dev ) )

      # no need
      # self.assert_( code == 0, 'При запуске модуля {} произошла ошибка [{}]: {}'
      #                           .format( CURRTEST.kmodule[0], code, out ) )

      if CURRTEST.check_dmesg( self.pass_phrase ) < 0:
        pcie_found = True
        log.debug( '  Устройство {}:{} использует интерфейс PCI Express.'.format( dev, vnd ) )

      if code == 0:
        code, out = CURRTEST.rem_kmodule(0)

        self.assert_( code == 0, 'При выгрузке модуля {} произошла ошибка [{}]: {}'
                                  .format( CURRTEST.kmodule[0], code, out ) )

      if pcie_found:
        break

    self.assert_( pcie_found == True, 'В системе не найдено ни одного устройства PCI-E.' )


###################### Тесты средств защиты информации #########################

class seTestBase(BaseTest):
  """
  Базовый класс тестов selinux
  """

  se_enforce    = selinux.security_getenforce()
  stream_backup = sys.stderr

  def setUp(self):
      super(seTestBase, self).setUp()
      sys.stderr = StreamToLog(log, logging.DEBUG)

  def runcon(self, context, program, *args):
      try:
          errcode = selinux.setexeccon(context)
      except OSError as e:
          log.error('Не установлен контекст безопасности: {}, {}'.format( context, e.errno ) )
          errcode = e.errno
      if errcode:
          return errcode
      progargs = (program,) + args
      try:
          errcode = os.spawnv(os.P_WAIT, program, progargs)
      except OSError as e:
          log.error('Потомок не запущен ' + str(progargs) + ': ' +
                        e.message)
          errcode = e.errno
      return errcode

  def __del__(self):
    sys.stderr = self.stream_backup

class testProcessIsolation(BaseTest):
  """ #535 """

  tool = '/usr/lib64/rosa-test-suite/process-intervention'

  @classmethod
  def self_check(self, q):
    _log = logging.getLogger()
    _log.setLevel( logging.DEBUG )
    _log.addHandler( QueueHandler(q) )

    _log.info('  Создан процесс: {}'.format( os.getpid() ))

    addr = psutil.Process( os.getpid() ).memory_maps(False)[0].addr.split('-')[0]
    return addr

  def runTest(self):

    with Indicator(message=' Выполняется тестирование...'):
      log.info('  Запуск проверяющей утилиты...')
      self.assertTrue( 0 == X( self.tool ), 'Изоляция процессов не соблюдается, обнаружены изменения в переменных после работы дочернего процесса.')
      log.info('  Попытка дочернего процесса изменить данные родительского - провалена.')

    log.info( '  Проверка адресов новых процессов...')

    with Damper():
      with MP.Pool( processes=2 ) as pool:
        res = [ pool.apply_async(self.self_check, (logging.getLogger("rts").Q,) ) for _ in range(2) ]
        res = [ '0x' + w.get() for w in res ]

    self.assertTrue( res[0] == res[1], 'Новые процессы не имеют общий виртуальный адрес {} != {}, изоляция не доказана !'
                                       .format( res[0], res[1] ) )

    log.info('  Новые процессы имеют общий виртуальный адрес.')

    log.info('  Проверка уникальности имен существующих процессов в системе...')

    counter = Counter([''])
    procs   = {}
    with Indicator(message=' Выполняется проверка имён процессов...'):
      for proc in psutil.process_iter(): #  avoid shared pages with children
        try:

          counter[proc.pid] += 1

          if counter[proc.pid] <= 1:
            procs[proc.pid] = proc.name()
          else:
            self.assertTrue( False, 'Найдены совпадения имен процессов: {} и {} имеют общий физический адрес {}'\
                                    .format( proc.name(), procs[ proc.pid ], proc.pid ) )

        except IndexError:
          continue
        except ( psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess ):
          continue

    log.info('  Совпадений имен процессов не обнаружено.' )

  def tearDown(self):
    pass

class testKernelProtection(BaseTest):
  """ #538 """

  paxtests_dir  = '/usr/lib64/paxtest'
  kernel_params = [ 'CONFIG_STACKPROTECTOR', 'CONFIG_STACKPROTECTOR_STRONG' ]

  # 'gcc',  - missing in package

  # ( test_name, OK_state, description )
  paxtests = \
  [
    ( 'anonmap'         , 'Killed',    ' 1. Исполнение кода в анонимном участке памяти' ),
    ( 'execbss'         , 'Killed',    ' 2. Исполнение кода в участке памяти неинициализированных переменных(bss segment)' ),
    ( 'execdata'        , 'Killed',    ' 3. Исполнение кода в участке памяти статических, глобальных переменных(инициализированных, data segment)' ),
    ( 'execheap'        , 'Killed',    ' 4. Исполнение кода в области динамической памяти(heap area)' ),
    ( 'execstack'       , 'Killed',    ' 5. Исполнение кода в области стека(stack area)' ),
    ( 'shlibbss'        , 'Killed',    ' 6. Исполнение измененного кода динамической библиотеки(bss segment)' ),
    ( 'shlibdata'       , 'Killed',    ' 7. Исполнение измененного кода динамической библиотеки(data segment)' ),
    # ( 'mprotanon'       , 'Killed',    ' 8. Атака п.1 с попыткой отключения защиты ядра используя mprotect()' ),
    # ( 'mprotbss'        , 'Killed',    ' 9. Атака п.2 с попыткой отключения защиты ядра используя mprotect()' ),
    # ( 'mprotdata'       , 'Killed',    '10. Атака п.3 с попыткой отключения защиты ядра используя mprotect()' ),
    # ( 'mprotheap'       , 'Killed',    '11. Атака п.4 с попыткой отключения защиты ядра используя mprotect()' ),
    # ( 'mprotstack'      , 'Killed',    '12. Атака п.5 с попыткой отключения защиты ядра используя mprotect()' ),
    # ( 'mprotshbss'      , 'Killed',    '13. Атака п.6 с попыткой отключения защиты ядра используя mprotect()' ),
    # ( 'mprotshdata'     , 'Killed',    '14. Атака п.7 с попыткой отключения защиты ядра используя mprotect()' ),
    # ( 'writetext'       , 'Killed',    '15. Исполнение измененного кода хранившегося в приложении(code segment)' ),

    ( 'randamap'        , '(guessed)', '16. Кол-во случайных бит используемых при выделении анонимного участка в памяти' ),
    ( 'randheap1'       , '(guessed)', '17. Кол-во случайных бит используемых при выделении участка в динамической памяти(heap segment, ET_EXEC)' ),
    ( 'randheap2'       , '(guessed)', '18. Кол-во случайных бит используемых при выделении участка в динамической памяти для библиотек(ET_DYN)' ),
    # https://gist.github.com/thestinger/b43b460cfccfade51b5a2220a0550c35
    # ( 'randmain1'       , '(guessed)', '19. Кол-во случайных бит используемых при выделении участка в памяти для исполняемого файла(ET_EXEC)' ),
    ( 'randmain2'       , '(guessed)', '20. Кол-во случайных бит используемых при выделении участка в памяти для исполняемого файла(ET_DYN)' ),
    ( 'randshlib'       , '(guessed)', '21. Кол-во случайных бит используемых при выделении участка в памяти для библиотек' ),
    ( 'randvdso'        , '(guessed)', '22. Кол-во случайных бит используемых при выделении участка в памяти для "virtual dynamic shared object"' ),
    ( 'randstack1'      , '(guessed)', '23. Кол-во случайных бит используемых при выделении участка в стеке(stack segment, SEGMEXEC)' ),
    ( 'randstack2'      , '(guessed)', '24. Кол-во случайных бит используемых при выделении участка в стеке(stack segment, PAGEEXEC)' ),
    ( 'randarg1'        , '(guessed)', '25. Кол-во случайных бит используемых в п.23 для параметров и переменных окружения' ),
    ( 'randarg2'        , '(guessed)', '26. Кол-во случайных бит используемых в п.24 для параметров и переменных окружения' ),
    ( 'randshlibdelta1' , '(guessed)', '27. Кол-во случайных бит используемых в смещениях памяти для динамичеких библиотек(ET_EXEC)' ),
    ( 'randshlibdelta2' , '(guessed)', '28. Кол-во случайных бит используемых в смещениях памяти для динамичеких библиотек(ET_DYN)' ),
    ( 'randexhaust1'    , '(guessed)', '29. Кол-во случайных бит используемых при нехватке памяти' ),
    ( 'randexhaust2'    , '(guessed)', '30. Кол-во случайных бит используемых при нехватке памяти' ),

    ( 'rettofunc1'      , 'NULL',      '31. Подмена адреса возврата в функцию в стеке(strcpy)' ),
    ( 'rettofunc2'      , 'Killed',    '32. Подмена адреса возврата в функцию в стеке(strcpy, RANDEXEC)' ),
    ( 'rettofunc1x'     , 'NULL',      '33. Подмена адреса возврата в функцию в стеке(memcpy)' ),
    ( 'rettofunc2x'     , 'Killed',    '34. Подмена адреса возврата в функцию в стеке(memcpy, RANDEXEC)' )
  ]

  def setUp(self):
    super( testKernelProtection, self ).setUp()
    os.environ['PAXTEST_MODE'] = '1'

    if not 'LD_LIBRARY_PATH' in os.environ:
      os.environ['LD_LIBRARY_PATH']= self.paxtests_dir

    os.chdir( self.paxtests_dir ) # oh!

  def runTest(self):
    log.info( '   Проверка конфигурации ядра... ' )

    for p in self.kernel_params:

      err = kernelParamCheck( p )

      if   err == 1:
        log.error( 'Параметр [{}] ядра не установлен.'.format( p ) )
      elif err == 2:
        log.error( 'Параметр ядра [{}] не найден в текущей конфигурации.'.format( p ) )
      elif err == 0:
        log.info( '   Параметр ядра обнаружен и соответствует ожидаемому зачению.')

      self.assert_( err == 0, 'Проверка конфигурации ядра не пройдена.')

    with Indicator(' Выполнение pax-тестов, ожидайте...'):
      with Terminal() as term:
        term.error( 'Во время проведения тестов защиты ядра, произошла ошибка [{code}]: {msg}' )

        for test in self.paxtests:
          term.add( '{}/{}'.format( self.paxtests_dir, test[0] ) )

    completed = 0
    log.info( '\t{:^105} {:^11} '.format( 'Описание', 'Статус' ) )

    for i, record in enumerate( term.log[1:] ):
      res = record[1].strip('\n').strip('\t').split(':', 1)
      ans = 'НЕ пройден'

      if self.paxtests[i][1] in res[1]:
        ans = 'пройден'
        completed += 1

      log.info( '\t{:105} {:^11} '.format( self.paxtests[i][2], ans ) )

    self.assertTrue( completed == len( self.paxtests ), 'Не все pax-тесты были пройдены!' )


  def tearDown(self):
    del os.environ['PAXTEST_MODE']

    if 'LD_LIBRARY_PATH' in os.environ:
      if os.environ['LD_LIBRARY_PATH'] == self.paxtests_dir:
        del os.environ['LD_LIBRARY_PATH']

class testSecurity(BaseTest):
  """
  Тест поддержки работы средств защиты информации от НСД
  """

  def setUp(self):
    try:
        self.audit_fd = audit.audit_open()
    except OSError as e:
        log.exception(e.message)
        self.assertEqual(0, e.errno,
                          'Подключиться к подсистеме аудита не удалось: ' +
                          e.message)

  def runTest(self):

    #se_enforce( StreamToLog( log ) )

    self.assertEqual(selinux.is_selinux_enabled(), 1,
                      'SELinux выключен.')
    self.assertEqual(selinux.security_getenforce(), 1,
                      'SELinux не в режиме enforcing.')
    self.assertEqual(audit.audit_is_enabled(self.audit_fd), 1,
                      'Модуль Audit выключен.')


  def tearDown(self):

    #se_enforce( StreamToLog( log ) )

    try:
        audit.audit_close(self.audit_fd)
    except OSError as e:
        log.exception(e.message)

class testSupportIPv4Sec(seTestBase):
  """
  Проверка обеспечения передачи сетевых меток по протоколам
  IPSec (RFC 2401 - 2412)
  """

  se_lvl = 's0'

  server_host = '127.0.0.1'
  port        = '5004'
  server_util = '/usr/lib64/rosa-test-suite/getpeercon_server ' + port
  client_util = 'nc -4 -d {} {}'.format( server_host, port )

  def runTest(self):

    processEx('getpeercon_serv', kill=True)

    self.se_lvl = selinux.getcon_raw()[1].split(':')[3]

    log.info('Запускается проверка поддержки IPSec для протокола IPv4...')

    with Terminal( stream=sys.stderr ) as plan:

      plan.error('\nВо время настройки меток произошла ошибка [{code}]:\n  {msg}')

      plan.add('! fuser {}/tcp'.format( self.port ) )\
          .info('  Проверка порта {} на доступность...\n'.format( self.port ) )

      plan.add('netlabelctl cipsov4 add pass doi:1 tags:1')\
          .info('  Включение протокола маркирования пакетов cipsov4...\n')

      plan.add('netlabelctl map del default')
      plan.add('netlabelctl map add default protocol:cipsov4,1')

      plan.add('netlabelctl unlbl accept off')\
          .info('  Блокировка немаркированного трафика включена.\n')

      plan.add( self.server_util, separated=True, timeout=10 )\
          .info('  Запуск сервера {}:{} {}...\n'.format( self.server_host, self.port, self.server_util ))

      plan.add( self.client_util, separated=True, pause_before=5 )\
          .info('  Запуск клиента... [{}]\n'.format( selinux.getcon_raw()[1] ) )

    peer_l = None
    for line in plan.log:
      self.assert_( 'not available' not in line[1] and
                             'null' not in line[1] and
                       'NO_CONTEXT' not in line[1] and
                                    line[0] == 0 ,
                    'При проверке поддержки IPSec для протокола IPv4 произошла ошибка.\n  ' + line[1] )
      if '|' in line[1]:
        log.info( '  Обнаружены метки: {}'.format( line[1] ) )
        peer_l = line[1].replace('\t','').split('|')[1].replace(';','').split(':')[3]

    self.assertEqual( peer_l, self.se_lvl,
                      'Ошибка: MLS\MCS уровни отправителя и получателя трафика не совпадают! {} != {}\n'.format( peer_l, self.se_lvl ))
    log.info('  MLS\MCS уровни отправителя и получателя трафика совпадают!')
    log.info('Проверка поддержки IPSec для протокола IPv4 пройдена успешно.\n')


  def tearDown(self):

    processEx('getpeercon_serv', kill=True)

    with Terminal( stream=sys.stderr ) as plan_clean:
      plan_clean.add('netlabelctl unlbl accept on')
      plan_clean.add('netlabelctl map del default')
      plan_clean.add('netlabelctl map add default protocol:unlbl')
      plan_clean.add('netlabelctl cipsov4 del doi:1')

    for l in plan_clean.log:
      self.assertEqual( l[0], 0, 'Во время тестирования произошла ошибка:\n{}'.format( l[1] ) )

class testSupportIPv6Sec(seTestBase):
  """
  Проверка обеспечения передачи сетевых меток по протоколам
  IPSec (RFC 2401 - 2412)
  """

  se_lvl = 's0'

  server_host = '::1'
  port        = '5006'
  server_util = '/usr/lib64/rosa-test-suite/getpeercon_server ' + port
  client_util = 'nc -6 -d {} {}'.format( server_host, port )

  def runTest(self):

    log.info('Запускается проверка поддержки IPSec для протокола IPv6...')

    processEx('getpeercon_serv', kill=True) # serv! not server

    self.se_lvl = selinux.getcon_raw()[1].split(':')[3]

    with Terminal( stream=sys.stderr ) as plan:

      plan.add('! fuser {}/tcp'.format( self.port ) )\
          .info('  Проверка порта {} на доступность...\n'.format( self.port ) )

      plan.add('netlabelctl calipso add pass doi:2')\
          .info('  Включение протокола маркирования пакетов CALIPSO DOI:2\n')

      plan.add('netlabelctl map del default')
      plan.add('netlabelctl map add default address:0.0.0.0/0 protocol:unlbl')
      plan.add('netlabelctl map add default address:::0/0 protocol:unlbl')
      plan.add('netlabelctl map add default address:::1 protocol:calipso,2')

      plan.add( self.server_util, separated=True, timeout=10 )\
          .info('  Запуск сервера {}:{} {}...\n'.format( self.server_host, self.port, self.server_util ))

      plan.add( self.client_util, pause_before=5 )\
          .info('  Запуск клиента... [{}]\n'.format( selinux.getcon_raw()[1] ) )

      plan.error('\nВо время настройки меток произошла ошибка [{code}]:\n   {msg}')

    peer_l = None
    for line in plan.log:
      self.assert_( 'not available' not in line[1] and
                             'null' not in line[1] and
                       'NO_CONTEXT' not in line[1] and
                                    line[0] == 0 ,
                    'При проверке поддержки IPSec для протокола IPv6 произошла ошибка.\n  ' + line[1] )
      if '|' in line[1]:
        log.info( '  Обнаружены метки: {}'.format( line[1] ) )
        peer_l = line[1].replace('\t','').split('|')[1].replace(';','').split(':')[3]

    self.assertEqual( peer_l, self.se_lvl,
                      'Ошибка: MLS\MCS уровни отправителя и получателя трафика не совпадают! {} != {}\n'.format( peer_l, self.se_lvl ))

    log.info('  MLS\MCS уровни отправителя и получателя трафика совпадают!')
    log.info('Проверка поддержки IPSec для протокола IPv6 пройдена успешно.\n')


  def tearDown(self):

    processEx('getpeercon_serv', kill=True)

    with Terminal() as plan_clean:
      #plan_clean.add('netlabelctl unlbl accept on')
      plan_clean.add('netlabelctl map del default')
      plan_clean.add('netlabelctl map add default protocol:unlbl')
      plan_clean.add('netlabelctl calipso del doi:2')

    for l in plan_clean.log:
      self.assertEqual( l[0], 0, 'Во время отката временных изменений произошла ошибка:\n{}\nТребуется перезагрузка!'.format( l[1] ) )

class testDACMAC(seTestBase):
  """
  Тест дискреционного и мандатного принципов разграничения доступа
  """
  user             = '__default__'
  user_lvls_backup = ''

  se_user   = 'user_u'
  se_user_r = 'user_r'
  se_user_d = 'user_t'      # for processes
  se_user_t = 'user_home_t' # for files

  se_user_context = '{}:{}:{}:'.format( se_user, se_user_r, se_user_d )

  reachable_lvls  = selinux.getseuserbyname( se_user )[2].split('-')

  se_user_lvl_max = reachable_lvls[-1:][0]
  se_user_lvl_min = reachable_lvls[0]

  default_user   = User()
  rbac_file_path = os.path.expanduser( '~{}/rbac-self-test-'.format( default_user.name ) )
  rbacHelperPath = '/usr/lib64/rosa-test-suite/rbac-self-test-helper'

  def runTest(self):

    if X( 'semanage permissive -a rosatest_t' ):
      self.assertFalse(True, '  Неудачная попытка расширить права тестирующему пакету!')

    if X( 'semanage user aib_u -m -r "s0-s3"' ):
      self.assertFalse(True, '  Неудачная попытка изменить диапазон доступа пользователю aib_u!')

    if self.reachable_lvls[0] in self.reachable_lvls[-1:]:
      log.info( '  Для пользователя {} существует только уровень {}.'.format( self.se_user, self.reachable_lvls[0] )\
                + '\n  Изменение уровней доступа для пользователя {}...'.format( self.se_user ) )

      self.user_lvls_backup = selinux.getseuserbyname( self.se_user )[2]

      if X( 'semanage login {} -m -r "s0-s3"'.format( self.user ) ):
        self.assertFalse(True, '  Неудачная попытка изменить диапазон доступа пользователю user_u!')

      self.reachable_lvls  = selinux.getseuserbyname( self.se_user )[2].split('-')

      time.sleep(5)
      self.se_user_lvl_max = self.reachable_lvls[-1:][0]
      self.se_user_lvl_min = self.reachable_lvls[0]

      self.contextHigh = self.se_user_context + self.se_user_lvl_max
      self.contextLow  = self.se_user_context + self.se_user_lvl_min

      log.info('  Диапазон уровней доступа успешно изменен')

    se_enforce( StreamToLog( log ) )


    log.info('  Обнаружены следующие уровни: минимальный {}, максимальный {}.'.format( self.se_user_lvl_min, self.se_user_lvl_max ) )

    self.__mlsfile_test( usr_lvl= self.se_user_lvl_max, op='r', owner_lvl= self.se_user_lvl_min, expectfail=False )
    self.__mlsfile_test( usr_lvl= self.se_user_lvl_max, op='w', owner_lvl= self.se_user_lvl_min, expectfail=True  )

    self.__mlsfile_test( usr_lvl= self.se_user_lvl_min, op='r', owner_lvl= self.se_user_lvl_max, expectfail=True )
    self.__mlsfile_test( usr_lvl= self.se_user_lvl_min, op='w', owner_lvl= self.se_user_lvl_max, expectfail=True )

    self.__mlsfile_test( usr_lvl= self.se_user_lvl_max, op='r', owner_lvl= self.se_user_lvl_max, expectfail=False )
    self.__mlsfile_test( usr_lvl= self.se_user_lvl_max, op='w', owner_lvl= self.se_user_lvl_max, expectfail=False )

    self.__mlsfile_test( usr_lvl= self.se_user_lvl_min, op='r', owner_lvl= self.se_user_lvl_min, expectfail=False )
    self.__mlsfile_test( usr_lvl= self.se_user_lvl_min, op='w', owner_lvl= self.se_user_lvl_min, expectfail=False )


  def __mlsfile_test(self, op=False, usr_lvl='', owner_lvl='', expectfail=False):

      owner_con = self.se_user_context + owner_lvl
      usr_con   = self.se_user_context + usr_lvl

      if 'w' in op:
        testopname = 'write'
        testop = 'w'
      elif 'r' in op:
        testopname = 'read'
        testop = 'r'

      se_enforce( StreamToLog( log ) )
      log.info( 'Создание файла с контекстом [{}]'.format( self.rbac_file_path + owner_con ) )
      selinux.setexeccon(None)
      testrc = self.runcon(owner_con, self.rbacHelperPath, owner_con, User().name, 'w')
      se_enforce( StreamToLog( log ) )

      created = selinux.getfilecon_raw( '{}/rbac-self-test-{}'.format( self.default_user.home, owner_con ) )[1]
      log.info('\nСоздан файл с контекстом "{}" : {}'.format( created, self.rbac_file_path + owner_con ) )

      self.assertEqual(0, testrc, 'Не завершено создание файла с контекстом ' + owner_con)

      log.info('Пользователь с контекстом "{}" {} файл \n{}'.format( usr_con, 'читает' if 'r' in op else 'пишет в',
      'Произошел ожидаемый отказ в доступе!\n' if expectfail else '') )
      testrc = self.runcon(usr_con, self.rbacHelperPath, owner_con, self.default_user.name, testop)

      if testrc:
        # log.error('Утилита вернула: {}'.format( testrc ) )
        testrc = 1

      msg = '\nТест метки НЕ ПРОЙДЕН:\n[владелец] {}\n[пользователь] {} операция: {}\n'.format(owner_con, usr_con, testopname )

      if expectfail:
        testrc = 0 if testrc == 1 else 1
        msg += 'Ожидаемая ошибка не была получена!\n'

      if self.runcon(owner_con, self.rbacHelperPath, owner_con, self.default_user.name, 'd'):
        log.error('Не завершено удаление файла с контекстом ' + owner_con)

      if testrc:
          log.error(msg)
      self.assertEqual(0, testrc, msg)



  def tearDown(self):

    se_enforce( StreamToLog( log ) )

    with Terminal( stream=sys.stderr ) as restore:
      restore.error('\nВо время восстановления параметров безопасности произошла ошибка [{code}]:\n   {msg}')
      restore.add( 'semanage login {} -m -r "{}"'.format( self.user, self.user_lvls_backup ) )\
            .info('  Попытка восстановить диапазон доступа пользователя user_u...')
      restore.add('semanage user aib_u --modify --range "s0" -R "sysadm_r"', quiet=True, timeout=30)\
            .info('  Попытка восстановить диапазон доступа для пользователя aib_u...')
      restore.add('semanage permissive -d rosatest_t', quiet=True, timeout=30)\
            .info('  Отмена расширения прав для пакета тестирования')


    errcode = selinux.setexeccon(None)
    logIfError(errcode, 'Восстановить контекст безопасности не удалось.')

class testNetlabelTrafficAccessControl(seTestBase):
  """
  Проверка контроля доступа сетевого трафика с метками IPsec
  ОТМЕНЁН
  """

  user             = '__default__'
  user_lvls_backup = ''

  se_user   = 'user_u'
  se_user_r = 'user_r'
  se_user_d = 'user_t' # for processes
  se_user_t = 'home_t' # for files

  se_user_context = '{}:{}:{}:'.format( se_user, se_user_r, se_user_d )
  contextHigh     = se_user_context + 's3'
  contextLow      = se_user_context + 's0'

  server_tool = '/usr/lib64/rosa-test-suite/getpeercon_server'
  server_port = '22459'

  client_tool = 'nc -d -4'

  testuser = None

  def rotate_sec(self, _con, con_, fail=True):

    log.info('  Попытка обмена данными между клиентом и сервером:')

    _lvl = _con.split(':')[3]
    lvl_ = con_.split(':')[3]

    # sudo -u tester_a runcon user_u:user_r:user_t:s3 nc -l 23
    # sudo -u tester_a runcon user_u:user_r:user_t:s3 /usr/lib64/rosa-test-suite/netlabel_server
    # sudo -u tester_a runcon user_u:user_r:user_t:s3 /usr/lib64/rosa-test-suite/netlabel_client 127.0.0.1

    run_server = 'sudo -u {} runcon "{}" {} {}'          .format( self.testuser.name, con_, self.server_tool, self.server_port )
    run_client = 'sudo -u {} runcon "{}" {} 127.0.0.1 {}'.format( self.testuser.name, _con, self.client_tool, self.server_port  )

    with Terminal( StreamToLog( log ) ) as plan:

      plan.add( run_server, separated=True )\
          .info('    Запуск сервера: [{}]...\n'.format( con_ ) )
      plan.add( run_client, pause_before=3 )\
          .info('    Запуск клиента: [{}]...\n'.format( _con ) )
      plan.error('  {msg}')

    peer_context = None

    for l in plan.log:
      if 'NO_CONTEXT' in l:
        self.fail( '  Трафик не был помечен!' )
      if '|' in l[1]:
        peer_context = l[1].replace('\t','')

    if peer_context:
      log.info('  Сообщения [от сервера|от клиента]: {}'.format( peer_context ) )

    if fail:
      self.assertTrue( plan.fail[0] != 0, 'Ошибка выполнения {} -> {}:\n  Ожидаемый отказ не был получен!'.format( _lvl, lvl_ ) )
      log.info('  Ожидаемый отказ в доступе получен!')
    else:
      self.assertTrue( plan.fail[0] == 0, 'Ошибка выполнения {} -> {}'.format( _lvl, lvl_ ) )

    log.info('\n  OK \n')

  def runTest(self):

    self.testuser = User( 'tester', 'tester', home='/home/tester', se_user='user_u', se_range='s0-s3' )

    with Terminal( stream=sys.stderr ) as setup:
      setup.add('netlabelctl cipsov4 add pass doi:1 tags:1')\
           .info('  Включение протокола маркирования пакетов cipsov4...\n')
      setup.add('netlabelctl map del default')
      setup.add('netlabelctl map add default protocol:cipsov4,1')
      setup.add('netlabelctl unlbl accept off')\
           .info('  Блокировка немаркированного трафика включена.\n')
      setup.error('\nВо время настройки меток произошла ошибка [{code}]:\n   {msg}')

    with Indicator( ' Идет проверка меток трафика: ' ) as progress:

      progress.set_suffix( self.contextHigh + ' -> ' + self.contextHigh )
      self.rotate_sec( self.contextHigh, self.contextHigh, fail=False )

      progress.set_suffix( self.contextHigh + ' -> ' + self.contextLow )
      self.rotate_sec( self.contextHigh, self.contextLow,  fail=True  )

      progress.set_suffix( self.contextLow + ' -> ' + self.contextHigh )
      self.rotate_sec( self.contextLow,  self.contextHigh, fail=False  )

      progress.set_suffix( self.contextLow + ' -> ' + self.contextLow )
      self.rotate_sec( self.contextLow,  self.contextLow,  fail=False )


  def tearDown(self):

    with Terminal( stream=sys.stderr ) as restore:
      restore.error('\nВо время восстановления настроек сети произошла ошибка [{code}]:\n   {msg}')
      restore.add('netlabelctl unlbl accept on')\
             .info('  Восстановление исходных настроек сети...\n')
      restore.add('netlabelctl map del default')
      restore.add('netlabelctl map add default protocol:unlbl')
      restore.add('netlabelctl cipsov4 del doi:1')

    errcode = selinux.setexeccon(None)
    logIfError(errcode, '\nВосстановить контекст безопасности не удалось.')

class testUserDevice(BaseTest):
  """
  Проверка обеспечения изделием сопоставления пользователя с устройством
  """

  def create_block_dev(self, delete=False):
      cmd_pull_create = [
          'dd if=/dev/zero of=hdd_test.img bs=1M count=1',
          'mkfs -t ext4 hdd_test.img',
          'mkdir /mnt/hdd_test/',
          'mount -t auto -o loop hdd_test.img /mnt/hdd_test/',
          'mount -o remount,rw /mnt/hdd_test'
      ]
      cmd_pull_delete = [
          'umount /mnt/hdd_test/', # error
          'rm -rf /mnt/hdd_test/',
          'rm -f hdd_test.img'
      ]

      cmd_pull = cmd_pull_delete if delete==True else cmd_pull_create

      log.debug('  cmdpull:' )
      for cmd in cmd_pull:
        log.debug( '    ' + cmd )

      for cm in cmd_pull:
          if subprocess.call(cm, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT) != 0:
              if delete != True:
                  self.create_block_dev(delete=True)
              raise Exception('Command failed: {} !'.format(cm))


  def reloadRules(self):
      res = X('udevadm control --reload-rules', return_type=Return.process)
      self.assertEqual(res.returncode, 0, res.stdout.read())

  def settings(self):
      self.create_block_dev()
      self.udevRulefile = "/etc/udev/rules.d/rmdeviotest.rules"
      self.user = 'root' #testsConfig.getTestUser()

      try:
          f = open(self.udevRulefile, 'w')
          rules = 'ACTION==\"add|change\", SUBSYSTEM==\"block\", ' \
                  'KERNEL==\"loop*\", OWNER=\"%s\"\n' % self.user
          log.debug('\n  1164: {}'.format(rules))
          f.write(rules)
          f.close()
          self.reloadRules()
      except:
          self.reloadRules()
          self.assertTrue(False, "Ошибка создания правила udev.")

  def runTest(self):
      self.settings()
      errcode, output = getstatusoutput("ls -l /dev/loop0")
      listloop = output.split(' ')

      self.assertEqual(listloop[2], self.user, output)


  def tearDown(self):
      self.create_block_dev(delete=True)

class testAudit(BaseTest):
  """
  Тест: аудит системных событий
  №54
  """
  secret_dir        = Path( '/var/tmp/secret' )
  audit_log         = Path( '/var/log/audit/audit.log' )
  central_panel_log = Path( '/var/log/rosa-serverd.log' )
  rule_key          = 'kabOom'

  def checkLogAudit(self, logfile:Path):

    with open( logfile.as_posix(), "r" ) as f:
      content = f.readlines()

    # read, write, access, rule : 4 times rule_key
    count = 0
    for line in content:
      if self.rule_key in line:
        count += 1
    return count >= 3

  def runTest(self):

    with self.audit_log.open('w+') as f: pass
    with self.central_panel_log.open('w+') as f: pass

    log.info( '  Лог аудита очищен.')

    log.info( '  Создание секретной директории {}...'.format( self.secret_dir.as_posix() ) )
    if not self.secret_dir.exists():
      self.secret_dir.mkdir()

    code, out = X( 'auditctl -w {} -p rwa -k {}'.format( self.secret_dir.as_posix(), self.rule_key ), Return.codeout )

    self.assert_( code == 0, 'Не удалось добавить правило аудита [{}]: {}'.format( code, out ) )

    log.info( '  Добавлено правило аудита для директории {}'.format( self.secret_dir.as_posix() ) )

    mtime_auditlog_1          = self.audit_log.stat().st_mtime
    mtime_central_panel_log_1 = self.central_panel_log.stat().st_mtime
    log.info( "  Изменен журнал аудита(audit): {}".format( asctime( localtime( mtime_auditlog_1 ) ) ) )
    log.info( "  Изменен журнал аудита(central panel): {}".format( asctime( localtime( mtime_central_panel_log_1 ) ) ) )

    sleep(1)

    log.info( '  Осуществление доступа к секретной директории {}...'.format( self.secret_dir.as_posix() ) )

    with open( self.secret_dir.as_posix() + '/file.txt', 'w+' ) as f:
      f.write( 'w' )
    with open( self.secret_dir.as_posix() + '/file.txt', 'r' ) as f:
      f.read()

    os.sync()

    self.assertTrue( self.checkLogAudit( self.audit_log ) or self.checkLogAudit( self.central_panel_log ), "Сообщения audit не были зафиксированы." )

    mtime_auditlog_2          = self.audit_log.stat().st_mtime
    mtime_central_panel_log_2 = self.central_panel_log.stat().st_mtime
    log.info( "  Изменен журнал аудита(audit): {}".format( asctime( localtime( mtime_auditlog_2 ) ) ) )
    log.info( "  Изменен журнал аудита(central panel): {}".format( asctime( localtime( mtime_central_panel_log_2 ) ) ) )

    self.assert_( (mtime_auditlog_1 != mtime_auditlog_2) or (mtime_central_panel_log_1 != mtime_central_panel_log_2),
                  'Файл аудита не был изменен, возможно, не были обновлены inode attributes.' )


  def tearDown(self):
    code, out = X( 'auditctl -W {} -p rwa -k {}'.format( self.secret_dir.as_posix(), self.rule_key ), Return.codeout )
    self.assert_( code == 0, 'Не удалось удалить правило аудита [{}]: {}'.format( code, out ) )
    log.info( '  Удалено правило аудита для директории'.format( self.secret_dir.as_posix() ) )

    if self.secret_dir.exists():
      shutil.rmtree( self.secret_dir.as_posix() )

class testIntegrity(BaseTest):
  """
  Тест целостности средств защиты информации
  """
  testcfg = '/etc/rosa-test.cfg'

  def runTest(self):
    log.debug('  Создание тестового файла...')

    with open(self.testcfg, 'w') as f:
      f.write('test config')

    log.debug('  Инициализация базы данных AIDE ...')

    errcode, output = getstatusoutput('/usr/sbin/aide --init --limit /etc')
    self.assertEqual(errcode, 0, 'Проинициализировать базу данных AIDE не удалось.\n' + output)

    if os.path.isfile('/var/lib/aide/aide.db'):
      log.debug('  Удаление старой базы данных AIDE...')
      try:
        os.unlink('/var/lib/aide/aide.db')
      except OSError as e:
        self.assertEqual(e.errno, 0, 'Удалить старую базу данных AIDE не удалось. Ошибка %d: %s'
                                      % (e.errno, e.message))

    log.debug('  Замена базы данных AIDE...')
    try:
      shutil.move('/var/lib/aide/aide.db.new', '/var/lib/aide/aide.db')
    except OSError as e:
      self.assertEqual(e.errno, 0, 'Заменить базу данных AIDE не удалось. Ошибка %d: %s'
                                    % (e.errno, e.message))

    log.debug('  Изменение тестового файла...')

    with open(self.testcfg, 'w') as f:
      f.write('test config updated')

    log.debug('  Проверка базы данных AIDE ...')

    errcode, output = getstatusoutput('/usr/sbin/aide --check --limit /etc')
    self.assertEqual(errcode, 4, 'При проверке базы данных AIDE получен неверный код ошибки (%d).\n%s'
                                      % (errcode, output))

    #errcode, output = getstatusoutput('sync; echo 3 > /proc/sys/vm/drop_caches' + extraParam)

    changeDetected = False
    for line in output.split('\n'):
      if self.testcfg in line:
        changeDetected = True
        break

    self.assertTrue(changeDetected, 'Изменение файла %s не было выявлено. Сообщения программы проверки:\n%s'
                                      % (self.testcfg, output))

    log.info('Проверка целостности средств защиты информации пройдена успешно.')

  def tearDown(self):
    if os.path.isfile(self.testcfg):
      log.debug('  Удаление тестового файла...')
      os.remove(self.testcfg)

class testSecureRestore(BaseTest):
  """
  Проверка обеспечения изделием надежного восстановления
  """
  testfile = 'rosa-test-restore.txt'
  filepath = '/var/tmp/' + testfile
  tarpath = '/var/tmp/' + 'rosa-test-restore.tar'

  def runTest(self):

    #se_enforce( StreamToLog( log ) )

    log.info('Начинается проверка надёжного восстановления.')
    if os.path.isfile(self.filepath):
        log.debug('  Удаление ранее созданного тестового файла...')
        os.remove(self.filepath)
    log.debug('  Создание тестового файла...')
    with open(self.filepath, 'w') as f:
        f.write('test string')
    newcon = 'user_u:object_r:user_tmp_t:s0'
    log.debug('  Назначение контекста безопасности...')
    selinux.setfilecon(self.filepath, newcon)
    log.debug('  Текущий контекст файла {}: {}'.format( self.filepath, selinux.getfilecon_raw(self.filepath)[1] ))
    if os.path.isfile(self.tarpath):
        log.debug('  Удаление ранее созданного архива...')
        os.remove(self.tarpath)
    log.debug('  Создание архива с учетом контекста...')
    errcode, output = getstatusoutput('tar --selinux -cvf %s -C /var/tmp %s' %
                                      (self.tarpath, self.testfile))
    self.assertEqual(errcode, 0,
                      'Заархивировать тестовый файл не удалось.\n' + output)
    log.debug('  Удаление тестового файла...')
    os.remove(self.filepath)
    log.debug('  Восстановление файла из архива...')
    errcode, output = getstatusoutput('cd /var/tmp && tar --selinux -xvf %s' %
                                      self.tarpath)
    self.assertEqual(errcode, 0,
                      'Восстановить тестовый файл не удалось.\n' + output)
    log.debug('  Проверка контекста безопасности...')

    fcon = selinux.getfilecon_raw(self.filepath)[1]

    self.assertEqual(fcon, newcon, 'Контекст восстановленного файла (%s) '
                                    'отличается от ожидаемого (%s).' % (fcon, newcon))
    log.info('Проверка надёжного восстановления пройдена успешно.')

  def tearDown(self):

    #se_enforce( StreamToLog( log ) )

    if os.path.isfile(self.filepath):
        log.debug('  Удаление тестового файла...')
        os.remove(self.filepath)
    if os.path.isfile(self.tarpath):
        log.debug('  Удаление архива...')
        os.remove(self.tarpath)

class testServiceSftp(BaseTest):
    """
    Проверка наличия службы передачи файлов по протоколу SFTP
    """

    def runTest(self):
        self.assertTrue(os.path.exists("/etc/ssh/sshd_config"),
                        "Файл конфигурации службы ssh не найден.")
        errcode, output = getstatusoutput(r"grep -E '^Subsystem\s+sftp\s+' "
                                          "/etc/ssh/sshd_config")
        config_file_path = ''
        if errcode == 0:
            m = re.search(r'^Subsystem\s+sftp\s+([^\0]+)', output)
            if m:
                config_file_path = m.group(1).strip()
            if not os.path.exists(config_file_path):
                self.fail("Ошибка в тесте службы SFTP: файл %s не найден" %
                          config_file_path)
        else:
            self.fail("Подсистема sftp не настроена в конфигурации службы ssh.")

class testCyrillicSupport(BaseTest):
    """
    Проверка обеспечения изделием возможности ввода и вывода информации
    в символах кириллицы
    """
    class TestDialog(Gtk.MessageDialog):
      def __init__(self, parent):
        Gtk.Dialog.__init__(
          self,
          "Проверка ввода и вывода кириллицы",
          parent,
          0,
          (
            Gtk.STOCK_OK,
            Gtk.ResponseType.OK,
          ),
        )
        self.set_default_response( Gtk.ResponseType.OK )
        self.set_default_size(100, 200)
        self.set_resizable(False)

        self.label = Gtk.Label( 'В этом окне проверяется возможность ввода и '
                                'вывода информации в символах кириллицы.\n\n'
                                'Если вы читаете этот текст введите «да» и нажмите «OK».'
                              )
        self.label.set_margin_left(10)
        self.label.set_margin_right(10)
        self.label.set_margin_top(10)
        self.label.set_margin_bottom(10)
        self.label.set_justify(Gtk.Justification.CENTER)
        self.label.set_line_wrap(True)


        self.entry = Gtk.Entry()
        self.entry.set_activates_default(True)
        self.entry.set_max_length(10)
        self.entry.set_margin_left(10)
        self.entry.set_margin_right(10)

        box = self.get_content_area()
        box.add( self.label )
        box.add( self.entry )

        self.show_all()

    def runTest(self):

      log.debug("  Выводится диалоговое окно для проверки...")

      with Damper(sys.stderr):
        dialog = self.TestDialog(None)
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
          text = dialog.entry.get_text().strip().lower()
          log.info( "  Получен ответ от пользователя: {}".format( text ) )
        else:
          log.info('  Диалог был закрыт без ответа.')

        dialog.destroy()

      log.debug("  Диалоговое окно закрыто.")
      self.assertEqual(text, 'да', 'Ожидаемый текст не был введён.')

class testUnicode(BaseTest):
  """
  Проверка поддержки кодировки Unicode и средств локализации
  """

  command = 'locale -a | grep ru_RU.utf8'

  def setUp(self):
      super(testUnicode, self).setUp()

      self.pre_lang = os.environ['LANG']
      if 'LANGUAGE' in os.environ:
          self.pre_language = os.environ['LANGUAGE']

  def runTest(self):
      import gettext

      testStr = "The quick brown fox jumps over the lazy dog."

      log.debug("  Проверка локализации (ru_RU.utf8)...")
      os.environ['LANG'] = 'ru_RU.UTF-8'
      if 'LANGUAGE' in os.environ:
          os.environ['LANGUAGE'] = 'ru_RU.UTF-8:ru'
      mPath = os.path.dirname(os.path.realpath(__file__))
      gettext.install('test_l10n', os.path.join(mPath, 'test_data', 'l10n'))
      localizedBytes = bytearray(_(testStr), 'UTF-8')
      with open(os.path.join(mPath, 'test_data', 'l10n', 'test_utf8.txt'),
                'rb') as f:
          utf8RuBytes = f.read()
      self.assertEqual(localizedBytes, utf8RuBytes,
                        'Локализованная строка не совпадает с ожидаемой.')
      log.debug("  Проверка локализации (en_US.utf8)...")
      os.environ['LANG'] = 'en_US.UTF-8'
      if 'LANGUAGE' in os.environ:
          os.environ['LANGUAGE'] = 'en_US.UTF-8:en'
      gettext.install('test_l10n', os.path.join(mPath, 'test_data', 'l10n'))
      self.assertEqual(_(testStr), testStr,
                        'Строка изменена при переводе на исходный язык.')

      code, out = X( self.command, Return.codeout )

      self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

      for l in out.splitlines():
        log.info( '  {}'.format( l ) )

  def tearDown(self):
      os.environ['LANG'] = self.pre_lang
      if 'LANGUAGE' in os.environ:
          self.pre_language = os.environ['LANGUAGE']

class testUnicodeSupport(BaseTest):
    """
    Проверка возможности использования изделием кодировки Unicode
    (наличие шрифтов, содержащих требуемые блоки символов Unicode)
    """

    def runTest(self):
        log.debug("  Получение списка установленных шрифтов...")
        fclist = subprocess.Popen(['fc-list', '-v'], stdout=subprocess.PIPE)
        fontFamily = None
        charsetList = False
        fontBlocks = {0x04: 'Кириллица',
                      0x1e: 'Дополнительная расширенная латиница',
                      0x22: 'Математические операторы',
                      0x23: 'Разнообразные технические символы',
                      0x2b: 'Разнообразные символы и стрелки'}
        blocksFound = []
        while True:
            line = fclist.stdout.readline().decode("utf-8")
            if line:
                if re.search(r'^Pattern has \d+ elts', line):
                    fontFamily = None
                else:
                    if fontFamily is None:
                        fm = re.search(r'^\s+family:\s*"([^"]*)"', line)
                        if fm:
                            fontFamily = fm.group(1)
                            charsetList = False
                    else:
                        if re.search(r'^\s*charset:\s*$', line):
                            charsetList = True
                        else:
                            if charsetList:
                                csm = re.search(r'^\s*([0-9a-f]{4}):'
                                                r'\s+([0-9a-f]{8})\s+([0-9a-f]{8})\s+([0-9a-f]{8})\s+([0-9a-f]{8})'
                                                r'\s+([0-9a-f]{8})\s+([0-9a-f]{8})\s+([0-9a-f]{8})\s+([0-9a-f]{8})',
                                                line)
                                if csm:
                                    blockNum = int(csm.group(1), 16)
                                    if (blockNum in fontBlocks and
                                            not (blockNum in blocksFound)):
                                        blocksFound.append(blockNum)
                                else:
                                    charsetList = False
            else:
                break

        for k in fontBlocks:
            self.assertTrue(k in blocksFound,
                            'Ни в одном из установленных шрифтов нет'
                            ' блока символов Unicode «%s».' % fontBlocks[k])
        log.debug('  Установленные шрифты содержат'
                     ' требуемые блоки символов Unicode: \n    ' +
                     '\n    '.join(list(fontBlocks.values())) + '.')

class testMultiMonitor(BaseTest):
    """
    Проверка обеспечения изделием работы в многомониторном режиме
    """

    def runTest(self):
        log.debug('  Получение информации о подключённых мониторах '
                     'от расширения X RandR...')
        errcode, output = getstatusoutput('xrandr')
        self.assertEqual(errcode, 0, 'Не удалось получить информацию '
                                     'расширения X RandR.')
        outputs = []
        for line in output.split('\n'):
            cre = re.search(r'(.*?)\s+connected\s+', line)
            if cre:
                outputs.append(cre.group(1))
        self.assertTrue(outputs, 'Не удалось получить сведения '
                                 'ни об одном подключённом мониторе.')
        if len(outputs) > 1:
            log.debug('  К системе подключены мониторы: ' +
                         ', '.join(outputs) + '.')
        else:
            log.warning( '  В настоящее время к системе подключён один монитор.\n'
                         '  Подключите дополнительный монитор и проверьте работу '
                         'в многомониторном режиме\n'
                         '  после перезагрузки.')

class testRemoteDesktop(BaseTest):
    """
    Проверка возможности изделия поддерживать механизмы доступа
    к удаленному рабочему столу
    """
    vncserver = None
    client    = '/usr/lib64/rosa-test-suite/test_vnc'

    def runTest(self):
        log.debug('  Подготовка к запуску VNC-сервера...')
        password = '123321'
        port = 55990
        errcode, output = getstatusoutput('echo -e "%(pass)s\n%(pass)s" |'
                                          ' vncpasswd /var/tmp/vncpass' % {'pass': password})
        self.assertEqual(errcode, 0, 'Не удалось задать пароль'
                                     ' для подключения.\n(%s)' % output)
        log.debug('  Запуск VNC-сервера...')
        self.vncserver = subprocess.Popen(['x0vncserver',
                                           '--passwordFile=/var/tmp/vncpass',
                                           '--rfbport=%d' % port],
                                          stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        sleep(1)
        if self.vncserver.poll() is not None:
            self.fail('Ошибка при запуске VNC-сервера.\n' +
                      'Сообщения сервера:\n' +
                      self.vncserver.stderr.read())
        log.debug('  Проверка подключения VNC-клиента...')
        errcode, output = getstatusoutput('%s localhost:%d -password %s' %
                                          (self.client, port, password))
        self.assertEqual(errcode, 0,
                         'При проверке VNC-подключения произошла ошибка.\n' +
                         'Сообщения клиента:\n' + output)

    def tearDown(self):
        finishProcess(self.vncserver, 'VNC-сервера')

class testGuiDevices(BaseTest):
    """
    Проверка функционирования средств графического интерфейса пользователя
    """

    class GraphicsTest:
        def __init__(self):
            self.state = None
            self.width = 768
            self.height = 620
            self.win = Gtk.Window()
            self.win.connect("delete_event", self.close)
            self.win.set_default_size(self.width, self.height + 80)
            self.win.set_resizable(False)
            self.win.set_title('Проверка графического оборудования')
            vbox = Gtk.VBox(False, 0)
            self.win.add(vbox)

            self.drawing_area = Gtk.DrawingArea()
            self.drawing_area.set_size_request(self.width, self.height)
            self.drawing_area.connect('draw', self.draw)
            #self.pangolayout = self.drawing_area.create_pango_layout("")
            vbox.add(self.drawing_area)

            labelt = Gtk.Label(
                'В этом окне проверяются средства отображения графического '
                'интерфейса и граф. манипулятор. Если вы видите все 32 градации '
                'цветов, отображённые выше, выполните следующие действия '
                '(в противном случае закройте окно):')
            labelt.set_line_wrap(True)
            labelt.set_size_request(self.width, 40)
            labelt.set_max_width_chars(1)
            labelt.set_hexpand(True)
            vbox.add(labelt)

            hbox = Gtk.HBox(False, 0)
            self.labeld = Gtk.Label('')
            self.labeld.set_use_markup(True)
            hbox.pack_start(self.labeld, False, False, 0)
            vbox.add(hbox)
            self.setState(1)

            self.win.set_events(
                Gdk.EventMask.POINTER_MOTION_MASK |
                Gdk.EventMask.BUTTON_PRESS_MASK |
                Gdk.EventMask.SCROLL_MASK)

            self.win.connect('event', self.handler)
            self.win.show_all()

        def setState(self, state):
            sleep(0.2)
            if state == 1:
                self.labeld.set_label(
                    '<b>1. Щёлкните в окне левой кнопкой мыши.</b>')
            elif state == 2:
                self.labeld.set_label(
                    '<b>2. Переместите указатель мыши в окне.</b>')
            elif state == 3:
                self.labeld.set_label(
                    '<b>3. Щёлкните в окне правой кнопкой мыши.</b>')
            elif state == 4:
                self.labeld.set_label(
                    '<b>4. Прокрутите колёсико прокрутки.</b>')
            elif state == 5:
                self.labeld.set_label(
                    '<b>Проверка завершена.</b>')
            self.state = state

        def getState(self):
            return self.state

        def run(self):
            Gtk.main()

        def close(self, widget, event, data=None):
            self.win.destroy()
            Gtk.main_quit()
            return False

        def handler(self, widget, event):

            if self.state == 1:
                if event.type == Gdk.EventType.BUTTON_PRESS and event.button.button == 1:
                    self.setState(2)
            elif self.state == 2:
                if event.type == Gdk.EventType.MOTION_NOTIFY:
                    self.setState(3)
            elif self.state == 3:
                if event.type == Gdk.EventType.BUTTON_PRESS and event.button.button == 3:
                    self.setState(4)
            elif self.state == 4:
                if event.type == Gdk.EventType.SCROLL:
                    self.setState(5)
                    while Gtk.events_pending():
                        Gtk.main_iteration()
                    sleep(1)
                    self.close(self.win, event)

        def draw(self, area, cc):

            cc.rectangle(0,0, area.get_allocated_width(), area.get_allocated_height())
            cc.fill()

            CON = 65535

            colors = [ Gdk.RGBA(   0,   0, CON, 1 ),
                       Gdk.RGBA(   0, CON,   0, 1 ),
                       Gdk.RGBA( CON,   0,   0, 1 ),
                       Gdk.RGBA(   0, CON, CON, 1 ),
                       Gdk.RGBA( CON,   0, CON, 1 ),
                       Gdk.RGBA( CON, CON,   0, 1 ),
                       Gdk.RGBA( CON, CON, CON, 1 )
                      ]

            bh = self.height / len(colors)
            sh = 20
            numlevels = 32
            y = 20

            cc.set_line_width(14)
            cc.set_line_join(cairo.LINE_JOIN_MITER)
            for color in colors:
                for j in range(1, numlevels + 1):
                    cc.set_source_rgba(color.red,
                                       color.green,
                                       color.blue,
                                       color.alpha * j / numlevels
                                     )
                    cc.rectangle( (j - 1) * (self.width / numlevels),
                                  y,
                                  self.width / numlevels,
                                  bh - sh
                                )
                    cc.fill()
                y += bh

            layout = PangoCairo.create_layout(cc)
            font = Pango.font_description_from_string("Consolas 10")
            layout.set_font_description(font)
            cc.set_source_rgb(204,204,204) #grey80
            for j in range(1, numlevels + 1):
              layout.set_text(str(j),2)
              cc.move_to((j - 1) * (self.width / numlevels) + 4, 4)
              PangoCairo.show_layout(cc,layout)


    def runTest(self):
       #with Damper(sys.stderr):

        log.debug('  Вывод диалогового окна для проверки...')
        gt = self.GraphicsTest()
        gt.run()
        log.debug('  Диалоговое окно закрыто.')
        self.assertEqual(gt.state, 5, 'Пройдены не все этапы проверки.\n'
                                      'Последний этап: %d.' % gt.state)
        log.debug('  Пройдены все этапы проверки.')

class testPrintMarkers(BaseTest):
    """
    Проверка настройки вида маркеров документов
    """

    def setUp(self):
        self.tagsDir = '/etc/cups/tags'
        self.tagsBakDir = '/var/tmp/cups_tags.bak'
        shutil.rmtree(self.tagsBakDir, True)
        shutil.copytree(self.tagsDir, self.tagsBakDir)

    def runTest(self):
        mPath = os.path.dirname(os.path.realpath(__file__))
        log.debug('  Подстановка специальных тегов разметки...')
        self.copyFiles(os.path.join(mPath, 'test_data', 'print', 'tags'),
                       self.tagsDir)
        log.debug('  Печать документа...')
        errcode, output = getstatusoutput('lp "%s"' % os.path.join(mPath,
                                                                   'test_data', 'print',
                                                                   'printer_test0.pdf'))
        self.assertEqual(errcode, 0,
                         'При отправке документа на печать произошла ошибка.\n'
                         'Сообщения программы печати:\n' + output)
        sleep(5)  # Пауза для подготовки задания с подставленными тегами
        log.debug('  Печать завершена.')

    def copyFiles(self, src, dest):
        for sfile in os.listdir(src):
            shutil.copy2(os.path.join(src, sfile), dest)

    def tearDown(self):
        self.copyFiles(self.tagsBakDir, self.tagsDir)

class testPrinterDiscover(BaseTest):
    """
    Проверка режима автоматического поиска и настройки принтеров
    """

    def runTest(self):

        log.debug('  Получение сведений о подключенных USB-принтерах...')
        errcode, output = getstatusoutput('lpstat -v')
        self.assertEqual(errcode, 0, 'Получить сведения о подключенных '
                                     'принтерах не удалось.\n' + output)

        log.debug('Waiting the printers to complete their work..')

        waitForPrinterIdle(check_interval=1, waiting_timeout_sec=40)

        printersFound = 0
        lines = output.split('\n')
        for line in lines:
            usr = re.search(r'^\s*(устройство для|device for)\s+([^:]+):\s*'
                            '(usb://|hp:/usb/)', line)
            if usr:
                printersFound += 1
                printer = usr.group(2)
                log.debug('  Удаляется принтер %s...' % printer)
                errcode, output = getstatusoutput('lpadmin -x "%s"' % printer)
                self.assertEqual(errcode, 0,
                                 ('Удалить подключённый принтер "%s" не удалось.\n' % printer) +
                                 output)

        if not printersFound:
            log.warning('При запуске теста в системе не выявлено'
                           'ни одного подключенного USB-принтера.')
        else:
            log.debug('  При запуске теста выявлено USB-принтеров: %d.',
                         printersFound)
        try:
            os.remove('/var/run/udev-configure-printer/usb-uris')
        except OSError:
            pass
        log.debug('  Перезапуск службы cups...')
        errcode, output = getstatusoutput('systemctl restart cups')
        self.assertEqual(errcode, 0, 'Перезапустить службу cups не удалось.\n' +
                         output)
        sleep(1.5)

        usb_device_reset()

        log.debug('  Инициирование операции добавления USB-принтеров...')
        udeverr, udevexe = getstatusoutput('udevadm trigger --action=add '
                                          '--subsystem-match=usb --attr-match=bInterfaceClass=07 '
                                          '--attr-match=bInterfaceSubClass=01 --verbose')

        # udevadm control --reload-rules
        errcode, output = getstatusoutput('systemctl restart cups')
        self.assertEqual(errcode, 0, 'Перезапустить службу cups не удалось.\n' +
                         output)

        log.debug('  udevadm: OUT: %s  ERR: %s' % (udevexe, udeverr))

        log.debug('  Получение сведений об обнаруженных USB-принтерах...')

        printersDetected = 0
        if udevexe and not udeverr:
            printersDetected = len(udevexe.splitlines())

        # for t in xrange(1, 60):
        #     printersDetected = 0
        #     errcode, output = getstatusoutput('lpstat -v')
        #     lines = output.split('\n')
        #     for line in lines:
        #         usr = re.search(r'^\s*(устройство для|device for)\s+([^:]+):\s*'
        #                         '(usb://|hp:/usb/)', line)
        #         if usr:
        #             printersDetected += 1
        #     if (printersFound == printersDetected or
        #             (printersFound == 0 and printersDetected > 0)):
        #         break
            sleep(0.5)

        self.assertNotEqual(printersDetected, 0, 'В процессе проверки'
                                                 ' не удалось обнаружить ни одного принтера.')
        log.debug('  В процессе проверки обнаружено принтеров: %d.' %
                     printersDetected)

class testPrintAttributes(seTestBase):
    """
    Проверка возможности предоставления программного интерфейса для
    автоматического заполнения учетных атрибутов документа при выводе
    на печатающее устройство
    """

    def runTest(self):
        contextS0 = self.selinux_user + ':' + self.selinux_role + ':' + \
                    self.program_type + ':' + 's0'
        contextS1 = self.selinux_user + ':' + self.selinux_role + ':' + \
                    self.program_type + ':' + 's1'

        mPath = os.path.dirname(os.path.realpath(__file__))

        log.debug('   Настройка печати маркера...')
        confstring = "\nClassification mandatory # rosa-test-suite\n"
        cupspath = "/etc/cups/cupsd.conf"
        confexists = False
        errcode, username = getstatusoutput("getent passwd `cat /proc/self/loginuid ` | cut -d: -f1")
        self.assertEqual(errcode, 0, 'Не удалось получить имя пользователя.\n' +
                         username)
        f = open(cupspath, "a+")
        fstring = f.readlines()
        for line in fstring:
            if "rosa-test-suite" in line:
                confexists = True
                break
        if not confexists:
            f.write(confstring)
        f.close()
        log.debug('  Перезапуск службы cups...')
        errcode, output = getstatusoutput('killall -9 cupsd')
        errcode = selinux.setexeccon("system_u:system_r:cupsd_t:s0-s1")
        self.assertEqual(errcode, 0, 'Не удалось включить контекст'
                                     ' безопасности s0.')
        errcode, output = getstatusoutput('/usr/sbin/cupsd')
        self.assertEqual(errcode, 0, 'Перезапустить службу cups не удалось.\n' +
                         output)
        sleep(2)
        log.debug('   Добавление принтера cups-pdf...')
        errcode, output = getstatusoutput('lpadmin -p cups-pdf -v cups-pdf:/ -E -P /usr/share/cups/model/CUPS-PDF.ppd')
        self.assertEqual(errcode, 0, 'Добавить принтер не удалось.\n' + output)

        errcode = selinux.setexeccon(contextS0)
        self.assertEqual(errcode, 0, 'Не удалось включить контекст'
                                     ' безопасности s0.')
        log.debug('  Печать документа на уровне безопасности 0...')
        errcode, output = getstatusoutput(
            'lpr -U "%s" -P cups-pdf "%s"' % (username, os.path.join(mPath, 'test_data', 'print', 'markertest0.ps')))
        self.assertEqual(errcode, 0,
                         'При отправке документа на печать произошла ошибка.\n'
                         'Сообщения программы печати:\n' + output)

        errcode = selinux.setexeccon(contextS1)
        self.assertEqual(errcode, 0, 'Не удалось включить контекст'
                                     ' безопасности s1.')
        log.debug('  Печать документа на уровне безопасности 1...')
        errcode, output = getstatusoutput(
            'lpr -U "%s" -P cups-pdf "%s"' % (username, os.path.join(mPath, 'test_data', 'print', 'markertest1.ps')))
        self.assertEqual(errcode, 0,
                         'При отправке документа на печать произошла ошибка.\n'
                         'Сообщения программы печати:\n' + output)

    def tearDown(self):
        errcode = selinux.setexeccon("\n")
        logIfError(errcode, 'Восстановить контекст безопасности не удалось.')

class testSupportNtpService(BaseTest):
    """
    Проверка обеспечения поддержки системы сетевого времени
    по протоколу NTP (RFC 1305)
    """
    #service = 'ntpd'

    def runTest(self):
        #self.runTestPrepare()
        log.debug('  Запускается программа синхронизации времени по NTP...')
        errcode, output = getstatusoutput('ntpdate -q 127.0.0.1')
        lines = output.split('\n')
        srvFound = False
        for line in lines:
            srv = re.search(r'^server\s+127\.0\.0\.1,\s+stratum ([0-9]+)', line)
            if srv and int(srv.group(1)) > 0:
                # Если сервер недоступен, возвращается stratum 0
                srvFound = True
                log.debug('  От сервера получены данные:\n    ' + line)
                break
        self.assertTrue(srvFound, 'Получить данные от локального сервера'
                                  ' времени NTP не удалось.\n' +
                        output)

class testSupportSshService(BaseTest):
  """
  Проверка обеспечения удаленного входа пользователя в ОС
  по протоколу SSH (RFC 4251)
  """

  def init(self):

    self.service      = 'sshd'
    self.userName     = 'sshtester'
    self.userPassword = 'PAssw0rd'

    self.u = User( name=self.userName, password=self.userPassword, home='/home/' + self.userName )

    self.testFile      = self.u.home + '/test.file'
    self.cmdCreateFile = 'touch "{}"; sync'.format( self.testFile )

  def runTest(self):
    self.init()

    log.info('  Перезапуск сервера sshd...')
    if X('systemctl restart sshd.service'):
      self.assert_(False, 'Перезапустить sshd не удалось!')

    if os.path.isfile( self.testFile ):
      os.remove( self.testFile )

    log.debug( '  Выполняется тестовое подключение SSH...' )

    with Indicator( ' Выполняется тестовое подключение...' ):
      try:
        ssh = paramiko.SSHClient()
        ssh.load_system_host_keys()
        ssh.set_missing_host_key_policy( paramiko.WarningPolicy() )

        try:
          ssh.connect('localhost', username=self.u.name, password=self.userPassword )

        except NoValidConnectionsError as e:
          self.fail( 'При подключении произошла ошибка: {}'.format( e ) )

        stdin, stdout, stderr = ssh.exec_command( self.cmdCreateFile )
        log.info( '  Создание файла...{}'.format( stderr.read().decode('UTF-8') ) )

      except paramiko.SSHException as e:
        log.error( 'ssh failed: {}'.format( e ) )
        self.fail( 'Установить SSH-подключение не удалось: {}.'.format( e ) )

      time.sleep(5) # time to cache sync
    log.debug( '  Тестовое подключение установлено. \n  Команда создания проверочного файла исполнена.' )

    self.assertTrue( os.path.isfile( self.testFile ) , 'Проверочный файл не обнаружен.' )

    log.info( 'Проверка удалённого входа по протоколу SSH пройдена успешно.' )

  def tearDown(self):
    if os.path.isfile( self.testFile ):
      log.debug('  Удаляется тестовый файл...')
      os.remove( self.testFile )

class testLftpWithSftp(BaseTest):
  """
    Проверка возможности передачи файлов средствами lftp и sftp
  """
  #sys.path.append('/modules/utils.py')
  #from modules.utils import Terminal as X

  localRoot     = '/var/tmp'
  localDirName  = 'sftpserver'
  localFileName = 'welcome.txt'
  testContent   = 'hello, lftp!'
  user          = 'sftpuser'
  password      = 'topsecret'

  localDirPath  = '{}/{}'.format( localRoot, localDirName )
  localFilePath = '{}/{}'.format( localDirPath, localFileName )

  testDirPath  = '/home/{}'.format( user )
  testFilePath = '{}/{}/{}'.format( testDirPath, localDirName, localFileName )

  def runTest(self):
    #self.sftpTest = testSupportSshService()
    #self.sftpTest.prepareSshService()

    if os.path.isdir( self.localDirPath ):
      log.debug('  Удаление старой тестовой директории...')
      shutil.rmtree( self.localDirPath )

    log.debug('  Создание тестовой директории и файла...')
    os.mkdir( self.localDirPath )
    with open( self.localFilePath , 'w') as f:
        f.write( self.testContent )

    log.debug('  Создание тестового пользователя \'{}\'...'.format( self.user ) )
    U = User( name=self.user, password=self.password )

    log.debug('  Загрузка тестовой директории {} с SFTP-сервера...'.format( self.localDirName ) )

    res = X( "lftp -c 'open -u {u},{p} sftp://127.0.0.1{lr}; mirror --verbose -c -P5 {ld} {td}/' " \
             .format( u=self.user, p=self.password, lr=self.localRoot, ld=self.localDirName, td=self.testDirPath ),
             return_type=Return.process )

    self.assertEqual( res.returncode, 0, 'Получить директорию с SFTP-сервера не удалось.\n' + res.stdout.read() )

    log.debug('  Выполняется чтение {} полученного с SFTP-сервера...'.format( self.testFilePath ) )
    with open( self.testFilePath, 'r') as f:
      res = f.readlines()[0]

    self.assertEqual( self.testContent, res, 'Содержимое тестового файла отличается от исходного ("{}" != "{}").' \
                      .format( self.testContent, res ) )


  def tearDown(self):

    if os.path.isdir( self.localDirPath ):
      log.debug('  Удаление тестовой директории...')
      shutil.rmtree( self.localDirPath )

class testDesktopThemes(BaseTest):
  """
  Тест графического интерфейса KDE
  """

  def runTest(self):

    log.info( 'Начинается проверка возможности унификации меню, основных диалогов, иконок и горячих клавиш.' )

    log.info( '  Создание тестового пользователя "rosa-test-user"...' )
    u = User( name='rosa-test-user', password='rosa-test-user', home='/home/rosa-test-user', se_user='user_u', se_range='s0-s3' )

    with ConRun( u.uid, u.gid, memory_bytes=3000 ) as mem:
      guitests.testDesktopThemes()
      X( 'sync' )

    if mem[0] != '0':
      log.info( '  \n'.join( mem ) )
      self.fail( 'Во время проверки произошла ошибка : {}'.format( mem ) )

class testSupportSnmpRmonService(BaseTest):
  """
  Проверка обеспечения поддержки средств сетевого мониторинга по протоколам
  SNMP (RFC 1157) и RMON (RFC 1757)
  """
  server = "127.0.0.1"
  client = '/usr/lib64/rosa-test-suite/test_snmp'

  new_config = '/usr/share/rosa-test-suite/test_data/snmpd.conf'
  old_config = '/etc/snmp/snmpd.conf'

  def runTest(self):

    log.info('  Сохранение существующей конфигурации snmpd, создание новой...')
    fileBackup( self.old_config, replacement=self.new_config )

    #self.startService("snmpd")

    log.info('  Перезапуск сервера snmpd...')
    if X('systemctl restart snmpd.service'):
      self.assert_(False, 'Перезапустить snmpd не удалось!')


    log.info('  Запускается проверка поддержки SNMP (с сервером {})...'.format( self.server ) )
    errcode, output = getstatusoutput('{} {}'.format( self.client, self.server ) )

    self.assertEqual( errcode, 0, 'При проверке поддержки SNMP произошла ошибка [{}]: {}'.format( errcode, output ) )

    log.info( 'Проверка поддержки SNMP пройдена успешно.\n  Сообщение проверяющей утилиты:' )
    for l in output.splitlines(): log.info( '    ' + l )

  def tearDown(self):

    log.info('  Восстановление предыдущей конфигурации snmpd...')
    fileBackup( self.old_config, restore=True )

class testSupportDnsService(BaseTest):
  """ #455 """

  def runTest(self):

    errcode, output = getstatusoutput( 'dig localhost.localdomain @127.0.0.1 +short' )

    self.assertEqual( errcode, 0, 'При проверке поддержки DNS произошла ошибка.\n{}'.format( output ) )
    self.assertEqual( output.strip(), '127.0.0.1', 'При проверке поддержки DNS получен неверный ответ.\n'
                                      .format( output ) )

    log.debug( '  Имя localhost.localdomain преобразовано в адрес: {}'.format( output ) )
    errcode, output = getstatusoutput( 'dig ptr 1.0.0.127.in-addr.arpa @127.0.0.1 +short' )

    self.assertEqual( errcode, 0, 'При проверке поддержки DNS произошла ошибка.\n{}'.format( output ) )
    self.assertTrue( output.startswith( 'localhost' ), 'При проверке поддержки DNS получен неверный ответ.\n'
                                                       .format( output ) )

    log.debug( '  Адрес 127.0.0.1 преобразован в имя: {}'.format( output ) )

class testSupportLdapService(BaseTest):
  """
  Проверка обеспечения поддержки службы каталогов LDAP
  """

  service      = 'ldap'
  slapdConf    = '/etc/openldap/slapd.conf'
  slapdConfBak = '/etc/openldap/slapd.conf.backup'
  tool         = 'slaptest'
  configCopied = False

  def runTest(self):
      if self.service:
        errcode, output = getstatusoutput('service %s status' % self.service)
        if errcode == 0:
          log.debug('  Служба %s была запущена до начала проверки.' % self.service)
        elif errcode == 3:
          log.debug('  Подготавливается конфигурация службы %s...' % self.service)
          # Обход проблемы с /etc/pki/tls/private/ldap.pem
          # (этот файл содержит только закрытый ключ)
          shutil.copy2(self.slapdConf, self.slapdConfBak)
          with open(self.slapdConf, 'r') as conf:
            contents = conf.read()
          with open(self.slapdConf, 'w') as conf:
            for line in contents.split('\n'):
              if re.search(r'^\s*(TLSCertificateFile|TLSCertificateKeyFile|TLSCACertificateFile\s+)', line):
                line = '# ' + line

              conf.write(line)
              conf.write('\n')
          self.configCopied = True

          log.debug('  Запускается служба %s...' % self.service)
          errcode, output = getstatusoutput('service %s start' % self.service)

          self.assertEqual(errcode, 0, 'Запустить службу %s не удалось.\n%s' % (self.service, output))
          self.serviceStarted = True
          sleep(2)
        else:
          self.fail('Служба %s находится в неизвестном состоянии (%d).' % (self.service, errcode))

      log.info( '  Запускается проверка поддержки LDAP...' )
      errcode, output = getstatusoutput('ldapsearch -LLL -x -b "" -s base namingContexts supportedLDAPVersion')
      self.assertEqual(errcode, 0, 'При проверке поддержки LDAP произошла ошибка.\n' + output )

      ncFound = False
      vFound = False
      for line in output.split('\n'):
        ncre = re.search(r'^namingContexts:\s+(.*)$', line)
        if ncre:
          log.debug('  Получен контекст именования LDAP: %s' % ncre.group(1))
          ncFound = True
        vre = re.search(r'^supportedLDAPVersion:\s+(.*)$', line)
        if vre:
          log.debug('  Получена версия LDAP: %s' % vre.group(1))
          vFound = True

      self.assertTrue(ncFound, 'Не удалось получить контекст именования '
                                'от службы LDAP.\nПолучен ответ:\n' + output)
      self.assertTrue(vFound, 'Не удалось получить номер версии '
                              'от службы LDAP.\nПолучен ответ:\n' + output)

      code, out = X( self.tool, Return.codeout )

      for l in out.splitlines():
        log.info( '  {}'.format( l ) )

      self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

      log.info('Проверка поддержки LDAP пройдена успешно.')

  def tearDown(self):
    if self.configCopied:
      log.debug('  Восстанавливается исходная конфигурация.')
      with open(self.slapdConfBak, 'r') as confbak:
        confContents = confbak.read()
      with open(self.slapdConf, 'w') as conf:
        conf.write(confContents)

class testLdap(BaseTest):
  """
  Проверка обеспечения изделием централизованного хранения учетных данных
  пользователей в локальной вычислительной сети
  """
  service = 'ldap'
  serviceStopped = False
  ldapDbDir = '/var/lib/ldap'
  ldapDbBakDir = '/var/lib/ldap/bak'
  ldapDbMoved = False
  slapdConf = '/etc/openldap/slapd.conf'
  slapdConfBak = '/etc/openldap/slapd.conf.backup'
  sysconfLdap = '/etc/sysconfig/ldap'
  sysconfLdapBak = '/etc/sysconfig/ldap.backup'
  configCopied = False
  sysconfigCopied = False

  @staticmethod
  def san(text:str, indent=2, words=('by',)):
    """ multiline-string sanation works as opposed to textwrap.dedent() """
    res = ''
    text = text.strip('\n')
    buf = ''
    for s in text.splitlines():
        buf = s.strip(' ').strip('\t') + '\n'
        if buf.startswith(words):
          res += (' ' * indent) + buf
        else:
          res += buf
    return res

  def runTest(self):
    log.debug( '  Проверка состояния службы {}...'.format( self.service ) )
    errcode, output = getstatusoutput( 'service {} status'.format( self.service ) )

    if errcode == 0:
      self.serviceStopped = True
      log.debug( '  Служба {} была запущена до начала проверки, она будет остановлена.'.format( self.service ) )
      errcode, output = getstatusoutput( 'service {} stop'.format( self.service ) )

      self.assert_( errcode == 0, 'Остановить службу {} не удалось.\n{}'.format( self.service, output ) )

      sleep(1)

    self.prepareConfig()
    log.debug( '  Перенос исходной базы данных LDAP...' )
    shutil.rmtree( self.ldapDbBakDir, True )
    os.mkdir( self.ldapDbBakDir )

    for dbfile in os.listdir( self.ldapDbDir ):
      if not os.path.isfile( os.path.join( self.ldapDbDir, dbfile ) ):
        continue
      os.rename( os.path.join( self.ldapDbDir, dbfile ),
                 os.path.join(self.ldapDbBakDir, dbfile ) )

    self.ldapDbMoved = True
    log.debug( '  Запуск службы {}...'.format( self.service ) )
    errcode, output = getstatusoutput( 'service {} start'.format( self.service ) )

    self.assert_( errcode == 0, 'Запустить службу {} не удалось: {}'.format( self.service, output ) )

    sleep(1)
    log.debug('  Запрос контекста именования...')

    errcode, output = getstatusoutput( 'ldapsearch -LLL -x -b "" -s base namingContexts supportedLDAPVersion' )

    self.assertEqual( errcode, 0, 'При запросе контекста именования произошла ошибка.\n' + output )

    nc = None
    for line in output.split('\n'):
      ncre = re.search(r'^namingContexts:\s+(.*)$', line)
      if ncre:
        nc = ncre.group(1).strip()
        break
    if nc is None:
      self.fail( 'Получить контекст именования не удалось.' )
    log.debug( 'Получен контекст именования: {}'.format( nc ) )
    if self.setAccess:
      self.setAccess()
    log.debug( '  Наполнение каталога...' )

    cmd = '''
            ldapadd -Y EXTERNAL -H ldapi:/// <<EOS
            dn: {c}
            o: LDAP testing directory
            description: Root entry for the test directory.
            objectClass: top
            objectclass: dcObject
            objectclass: organization

            # Admin user.
            dn: cn=admin,{c}
            objectClass: simpleSecurityObject
            objectClass: organizationalRole
            cn: admin
            description: LDAP administrator
            userPassword: 5ecretP@ssw0rd

            dn: ou=Group,{c}
            objectClass: organizationalUnit
            ou: Group

            dn: cn=testers,ou=Group,{c}
            objectClass: posixGroup
            cn: testers
            gidNumber: 10000

            dn: ou=People,{c}
            objectClass: organizationalUnit
            ou: People

            dn: uid=rosa-test-user,ou=People,{c}
            objectClass: inetOrgPerson
            objectClass: posixAccount
            objectClass: shadowAccount
            uid: rosa-test-user
            sn: Tester
            givenName: Tester
            cn: Rosa Tester
            displayName: Rosa Tester
            uidNumber: 1000
            gidNumber: 10000
            userPassword: password
            gecos: Rosa Tester
            loginShell: /bin/bash
            homeDirectory: /home/rosa-test-user
            shadowExpire: -1
            shadowFlag: 0
            shadowWarning: 7
            shadowMin: 8
            shadowMax: 999999
            shadowLastChange: 10877
            mail: rosa-test-user@no.mail
            mobile: +7 xxx xxx xx xx
            title: System Tester
            initials: RT
            EOS
            exit $?
          '''.format( c=nc )

    errcode, output = getstatusoutput( self.san( cmd ) )

    self.assert_( errcode == 0, 'Добавить в каталог данные не удалось.\n' + output )

    log.debug( '  Проверка подключения пользователя...' )
    errcode, output = getstatusoutput( 'ldapwhoami -D "uid=rosa-test-user,ou=People,{}" -w password'.format( nc ) )

    self.assert_( errcode == 0, 'Пользователь не смог подключиться к хранилищу LDAP.\n' + output )

    log.debug( '  Смена пароля пользователя...' )
    errcode, output = getstatusoutput( 'ldappasswd -Y EXTERNAL -H ldapi:/// "uid=rosa-test-user,ou=People,{}" -s newP@ssw0rd'.format( nc ) )

    self.assert_( errcode == 0, 'Сменить пароль пользователя не удалось.\n' + output )

    log.debug( '  Проверка альтернативного подключения с новым паролем...' )


    l = ldap.initialize( 'ldap://localhost/' )
    l.simple_bind_s( 'uid=rosa-test-user,ou=People,{}'.format( nc ), 'newP@ssw0rd' )
    l.unbind_s()
    log.info( 'Проверка поддержки централизованного хранилища учётных данных пройдена успешно.' )

  def prepareConfig(self):
    log.debug('  Подготовка конфигурации sysconf/ldap...')
    shutil.copy2(self.sysconfLdap, self.sysconfLdapBak)
    with open(self.sysconfLdap, 'r') as conf:
      contents = conf.read()
    with open(self.sysconfLdap, 'w+') as conf:
      for line in contents.split('\n'):
        if re.search(r'^#\s*SLAPD URL', line):
          line = line + '\n' + '\nSLAPDURLLIST="ldap:/// ldaps:/// ldapi:///"\n'
        if re.search(r'^\s*SLAPDURLLIST=', line):
          line = '#' + line
        conf.write(line)
        conf.write('\n')
    self.sysconfigCopied = True

    log.debug('  Подготовка конфигурации openldap/slapd.conf...')
    shutil.copy2(self.slapdConf, self.slapdConfBak)
    with open(self.slapdConf, 'r') as conf:
      contents = conf.read()
    with open(self.slapdConf, 'w+') as conf:
      for line in contents.split('\n'):
        if re.search(r'^\s*(TLSCertificateFile|'
                      r'TLSCertificateKeyFile|'
                      r'TLSCACertificateFile\s+)', line):
          line = '# ' + line
        if re.search(r'^# Global ACL definitions', line):
          line = line + '\n' \
                      + self.san(
                                  '''
                                    access to *
                                      by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" manage
                                      by self write
                                      by * read

                                    access to attr=userPassword
                                      by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" write
                                      by self write
                                      by anonymous auth
                                      by * none
                                  ''')
        conf.write(line)
        conf.write('\n')
    self.configCopied = True

  def setAccess(self):
    pass

  def tearDown(self):
    log.debug('  Остановка службы LDAP...')
    errcode, output = getstatusoutput( 'service {} stop'.format( self.service ) )
    if self.ldapDbMoved:
      log.debug( '  Восстановление исходной базы данных LDAP...' )

      for dbfile in os.listdir( self.ldapDbDir ):
        if not os.path.isfile( os.path.join( self.ldapDbDir, dbfile ) ):
          continue
        os.remove(os.path.join(self.ldapDbDir, dbfile))

      for dbfile in os.listdir(self.ldapDbBakDir):
        os.rename(os.path.join(self.ldapDbBakDir, dbfile),
                  os.path.join(self.ldapDbDir, dbfile))

      os.rmdir(self.ldapDbBakDir)
    self.restoreConfig()
    if self.serviceStopped:
      log.debug( '  Запуск службы {}...'.format( self.service ) )
      errcode, output = getstatusoutput( 'service {} start'.format( self.service ) )

      self.assert_( errcode == 0, 'Запустить службу {} не удалось.\n'.format( self.service, output ) )

  def restoreConfig(self):
    if self.sysconfigCopied:
      log.debug('  Восстановление конфигурации sysconf/ldap.')
      with open(self.sysconfLdapBak, 'r') as confbak:
        confContents = confbak.read()
      with open(self.sysconfLdap, 'w+') as conf:
        conf.write(confContents)
    if self.configCopied:
      log.debug('  Восстановление конфигурации openldap/slapd.conf.')
      with open(self.slapdConfBak, 'r') as confbak:
        confContents = confbak.read()
      with open(self.slapdConf, 'w+') as conf:
        conf.write(confContents)

class testSupportDhcpService(BaseTest):
    """
    Проверка обеспечения взаимодействия компьютеров в сети по протоколам
    DHCP и BOOTP (RFC 2131, 2132)
    """
    dhcpServer = None
    dhcpClient = None
    dummyLoaded = False

    def runTest(self):

        toggleRosaDeviceManager()

        log.debug('  Назначение IP-адреса...')
        errcode = subprocess.call( 'ip link add name dummy0 type dummy', shell=True )
        errcode = subprocess.call( 'ip address add 10.255.0.1/24 dev dummy0', shell=True )
        errcode = subprocess.call( 'ip link set dev dummy0 up', shell=True )
        self.assertEqual(errcode, 0, 'Назначить IP-адрес фиктивному сетевому интерфейсу (dummy0) не удалось.\n')

        open('/var/tmp/dhcp.server.lease', 'w').close()
        with open('/var/tmp/dhcp.test.conf', 'w') as f:
            f.write('subnet 10.255.0.0 netmask 255.255.255.0 {range dynamic-bootp 10.255.0.101 10.255.0.101;}')

        log.debug('  Запуск DHCP-сервера...')
        self.dhcpServer = subprocess.Popen(['dhcpd', '-d', '-f',
                                            '-lf', '/var/tmp/dhcp.server.lease',
                                            '-cf', '/var/tmp/dhcp.test.conf',
                                            'dummy0'],
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.PIPE)
        sleep(1)
        if self.dhcpServer.poll() is not None:
            self.fail('Ошибка при запуске DHCP-сервера.\n Сообщения сервера:\n' +
                      self.dhcpServer.stderr.read())

        #errcode, output = getstatusoutput('killall dhclient')

        log.debug('  Сброс аренды DHCP-клиента...')
        errcode, output = getstatusoutput('dhclient -r dummy0')
        self.assertEqual(errcode, 0, 'Сбросить аренду DHCP-клиента не удалось.\n' + output)

        log.debug('  Запуск DHCP-клиента...')
        self.dhcpClient = subprocess.Popen(['dhclient', '-d', '-1', 'dummy0'],
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.PIPE)

        log.info( '  Сброс адреса интерфейса dummy0.' )
        if x( 'ip address flush dummy0' ):
          self.fail( 'Не удалось сбросить адрес интерфейса dummy0.' )

        with Indicator( ' Ожидание смены ip сервером DHCP:' ) as indicator:
          wait = 20
          errcode = 0
          gotip = False
          while not (gotip) and wait > 1:
              if self.dhcpClient.poll() is not None:
                  break
              sleep(1)
              wait -= 1
              ip = get_ip_address('dummy0')
              indicator.set_suffix( ' осталось {} сек, текущий ip {}'.format( wait, ip ) )
              log.debug('  sec {} | ip: {}'.format(wait, ip))
              if ip == '10.255.0.101':
                  gotip = True

        if self.dhcpClient.poll() is None:
            try:
                self.dhcpClient.terminate()
            except OSError:
                pass
        output = self.dhcpClient.communicate()[1]
        errcode = self.dhcpClient.returncode
        self.assertTrue( gotip, 'Ожидаемый IP-адрес не был получен.\n'
                               'Сообщения DHCP-клиента:\n' + output.decode("utf-8") )

        log.info('\n  IP-адрес получен. Сообщения DHCP-клиента:\n' + output.decode("utf-8") )
        log.info('Проверка поддержки DHCP пройдена успешно.')

    def tearDown(self):
        finishProcess(self.dhcpServer, 'DHCP-сервера')
        toggleRosaDeviceManager()

class testSupportNfsService(BaseTest):
  """
  Проверка обеспечения поддержки сетевой файловой системы
  """
  service = 'nfs'
  nfsExported = False
  nfsMounted = False

  export_dir = Path('/var/tmp/exported')
  import_dir = Path('/var/tmp/imported')

  def prepareNfsService(self):
    log.debug('  Подготовка экспортируемого каталога...')
    shutil.rmtree( self.export_dir.as_posix(), True )
    self.export_dir.mkdir( 0o777, True, True )
    os.sync()

    log.debug('  Экспорт каталога...')
    errcode, output = getstatusoutput( 'exportfs -o fsid=0,rw,no_root_squash localhost:{}'.format( self.export_dir.as_posix() ) )
    self.assertEqual(errcode, 0, 'Экспортировать каталог не удалось.\n' + output)
    self.nfsExported = True

    log.debug('  Подготовка каталога монтирования...')
    shutil.rmtree( self.import_dir.as_posix() , True)
    self.import_dir.mkdir( 0o777, True, True )
    os.sync()

    # log.debug('  Запуск сервиса nfs-server...')
    # code, out = X( 'systemctl start nfs-server', Return.codeout )
    # self.assert_( code == 0, 'При старте сервиса nfs-server произошла ошибка [{}]:{}'.format( code, out  ) )

    log.debug('  Монтирование каталога NFS...')
    errcode, output = getstatusoutput( 'mount.nfs4 localhost:/ {}'.format( self.import_dir.as_posix() ) )
    self.assertEqual(errcode, 0, 'Смонтировать каталог NFS не удалось.\n' + output)
    self.nfsMounted = True

  def runTest(self):
    self.prepareNfsService()
    log.debug('  Запись пробного файла в смонтированный каталог...')
    with open( '{}/test_file'.format( self.import_dir.as_posix() ), 'w'):
      pass

    log.debug('  Проверка наличия файла в целевом каталоге NFS...')
    self.assertTrue(os.path.isfile('{}/test_file'.format( self.export_dir.as_posix() ) ),
                    'Проверочный файл не найден в целевом каталоге NFS.')
    log.info('Проверка поддержки NFS пройдена успешно.')

  def tearDown(self):
    if self.nfsMounted:
      log.debug('  Размонтирование каталога...')
      errcode, output = getstatusoutput( 'umount {}'.format( self.import_dir.as_posix() ) )
      if errcode != 0:
        log.info('Размонтировать каталог не удалось.\n' + output)
    if self.nfsExported:
      log.debug('  Отмена экспорта каталога...')
      errcode, output = getstatusoutput('exportfs -u 127.0.0.1:{}'.format( self.export_dir.as_posix() ) )
      if errcode != 0:
        log.info('Отменить экспорт каталога не удалось.\n' + output)

    try:
      shutil.rmtree( self.export_dir.as_posix() )
      shutil.rmtree( self.import_dir.as_posix() )

    except OSError as e:

      if e.__class__.__name__ == 'OSError' and e.errno == 116:
        log.info( '  Дескриптор директории импорта устарел, удаление не требуется.')

      else:
        raise e

class testSupportFileTransferServices(BaseTest):
    """
    Проверка обеспечения передачи файлов по протоколам TFTP, FTP
    (RFC 1350, 959, 1094, 913)
    """
    removeTestFiles = []
    turnOffTftp = False
    stopVsftpd = False
    nfsSX = False

    content = 'Test file content :)'

    def runTest(self):
        log.info('Начинается проверка поддержки TFTP...')
        log.debug('  Получение текущего состояния службы tftp...')
        errcode, output = getstatusoutput('chkconfig --list tftp')

        self.assert_( errcode == 0, 'Получить состояние службы tftp не удалось.\n' + output )

        tftpIniState = None
        for line in output.split('\n'):
            tsre = re.search(r'^tftp\s+(.*)$', line)
            if tsre:
                tss = tsre.group(1).strip().lower()
                if tss == 'off' or tss == 'выкл':
                    tftpIniState = 'off'
                    break
                elif tss == 'on' or tss == 'вкл':
                    tftpIniState = 'on'
                    break
        if not (tftpIniState):
            self.fail('Определить состояние службы tftp не удалось.\n' + output)
        if tftpIniState == 'off':
            log.debug('  Включение службы tftp...')
            errcode, output = getstatusoutput('chkconfig tftp on')
            self.assertEqual(errcode, 0, 'Включить службу tftp '
                                         'не удалось.\n' + output)
            self.turnOffTftp = True

        log.debug('  Создание тестового файла...')
        with open('/var/lib/tftpboot/test_file', 'w') as f:
            f.write( self.content )
        f.close()
        self.removeTestFiles.append('/var/lib/tftpboot/test_file')
        log.debug('  Загрузка тестового файла с TFTP-сервера...')

        errcode, output = getstatusoutput('curl -m 5 -s -S tftp://127.0.0.1/test_file')

        self.assertEqual(errcode, 0, 'Получить тестовый файл с TFTP-сервера '
                                     'не удалось.\n' + output)
        self.assertEqual(self.content, output, 'Содержимое тестового файла '
                                              'отличается от исходного ("%s" != "%s").' % (self.content, output))

        log.info('Начинается проверка поддержки FTP...')
        log.debug('  Создание тестового файла...')
        with open('/var/ftp/pub/test_file', 'w') as f:
            f.write(self.content)
        self.removeTestFiles.append('/var/ftp/pub/test_file')
        log.debug('  Загрузка тестового файла с FTP-сервера...')
        errcode, output = getstatusoutput('curl -m 3 -s -S '
                                          'ftp://127.0.0.1/pub/test_file')
        self.assertEqual(errcode, 0, 'Получить тестовый файл с FTP-сервера '
                                     'не удалось.\n' + output)
        self.assertEqual(self.content, output, 'Содержимое тестового файла '
                                              'отличается от исходного ("%s" != "%s").' % (self.content, output))

        log.info('Проверка передачи файлов по протоколам TFTP, FTP пройдена успешно.')

    def tearDown(self):
        if self.turnOffTftp:
            log.debug('  Выключение службы tftp...')
            errcode, output = getstatusoutput('chkconfig tftp off')
            if errcode != 0:
                log.info('Выключить службу tftp не удалось.\n' + output)

        if self.removeTestFiles:
            log.debug('  Удаление тестовых файлов...')
            for tf in self.removeTestFiles:
                try:
                    os.remove(tf)
                except OSError as e:
                    log.exception(e.message)
        pass

class testInteractiveInterrupt(BaseTest):
  """
  Проверка возможности прерывания выполняющегося процесса для всех функций
  интерактивного интерфейса
  """

  def runTest(self):

    tool  = '/usr/lib64/rosa-test-suite/xtool'

    log.info('Начинается проверка прерывания процесса...')

    u = User()

    with Damper():
      with ConRun( u.uid, u.gid, memory_bytes=3000, wait=False) as mem:

        log.debug('  Запускается программа, имитирующая длительный '
                      'процесс...')
        pid = []
        P = MP.Process( target=guitests.testHangingApp, daemon=True )
        P.start()

    with Terminal( StreamToLog( log ) ) as term:
      term.error( 'Во время работы утилиты для прерывания произошла ошибка [{code}]: {msg}\n' )

      term.add( '{} "Проверка прерывания процесса" -t 15000 -p'.format( tool ) )\
        .info( '  Обнаружение pid программы имитирующей длительный процесс...\n')
      term.add( '{} "Проверка прерывания процесса" -t 15000'.format( tool ), separated=True )\
        .info( '  Запускается программа принудительного прерывания процесса...\n' )
      term.add( '{} "Предупреждение" -t 15000 -k Return'.format( tool ), separated=True )\
        .info( '  Подтверждение закрытия процесса...\n')

    #term.print_log()
    errcode = term.fail[0]
    self.assertEqual( errcode, 0, 'Принудительное прерывание не выполнено.\n'
                                  'Сообщения программы:\n' + term.fail[1] )

    pid = int( term.log[1][1].strip(';') )
    if pid != 0:
      errcode = waitProcess( pid, kill=True, timeout=20, end_pause=2 )

    self.assertNotEqual( errcode, 1, 'Процесс {} превысил свое время выполнения! Корректное прерывание не выполнено.\n'.format( pid ) )
    self.assertNotEqual( errcode, 2, 'Процесс {} уничтожен по истечению времени! Корректное прерывание не выполнено.\n'.format( pid ) )

    log.info('Проверка прерывания процесса пройдена успешно.')

class testSupportOfficeDocuments(BaseTest):
  """
  Проверка возможности создания, редактирования, сохранения и просмотра
  документов в форматах TXT, ODF, DOC, DOCX, RTF, XLS, XLSX, ODS, ODP,
  PPT и PPTX
  """

  def runTest(self):
    log.info('  Создание тестового пользователя "rosa-test-user"...')
    u = User( name='rosa-test-user', password='rosa-test-user', home='/home/rosa-test-user', se_user='user_u', se_range='s0-s3' )

    pid = processEx('at-spi-bus-launcher')
    if pid > 0:
      X( 'kill -15 {}'.format( pid ), return_type=Return.stdout )

    with ConRun( u.uid, u.gid ) as mem:
      X( '/usr/lib64/at-spi-bus-launcher &' )
      guitests.testSupportOfficeDocuments()

    self.assertEqual(mem[0], '0', 'Проверка офисных приложений не пройдена.')

class testSupportPdfDocuments(BaseTest):
  """
  Проверка возможности просмотра документов в формате PDF
  """

  def runTest(self):

    log.info('Начинается проверка просмотра документов в формате PDF.')

    log.info('  Создание тестового пользователя "rosa-test-user"...')
    u = User( name='rosa-test-user', password='rosa-test-user', home='/home/rosa-test-user', se_user='user_u', se_range='s0-s3' )

    processEx('okular', kill=True )
    waitCPU()

    with ConRun( u.uid, u.gid, memory_bytes=3000 ) as mem:
      guitests.testSupportPdfDocuments()

    self.assertEqual(mem[0], '0', 'Проверка просмотра PDF не пройдена')

  def tearDown(self):
    processEx('okular', kill=True )

class testCheckingForBrowser(BaseTest):
  """
  Проверка на наличие программы просмотра html-страниц (firefox)
  """

  def runTest(self):
    log.info('Начинается проверка наличия программы обработки гипертекстовых страниц (браузера)')

    log.info('  Создание тестового пользователя "rosa-test-user"...')
    u = User( name='rosa-test-user', password='rosa-test-user', home='/home/rosa-test-user', se_user='user_u', se_range='s0-s3' )

    processEx('chrome', kill=True )
    waitCPU()

    with ConRun( u.uid, u.gid, memory_bytes=3000 ) as mem:
      guitests.testCheckingForBrowser()

    self.assertEqual( mem[0], '0', 'Проверка наличия программы обработки гипертекстовых страниц (браузера) не пройдена! ')

  def tearDown(self):
    processEx('chrome', kill=True )

class testSupportMultimediaFiles(BaseTest):
  """
  Проверка возможности воспроизведения мультимедийных (аудио- и видео-) файлов
  основных форматов
  """
  def runTest(self):
    log.info('Начинается проверка воспроизведения мультимедийных файлов.')

    log.info('  Создание тестового пользователя "rosa-test-user"...')
    u = User( name='rosa-test-user', password='rosa-test-user', home='/home/rosa-test-user', se_user='user_u', se_range='s0-s3' )

    self.noroot = User()
    with ConRun( self.noroot.uid, self.noroot.gid ):
      with Terminal() as T:
        T.add('pulseaudio -k')

    waitCPU()
    with ConRun( u.uid, u.gid, memory_bytes=3000 ) as mem:
      guitests.testSupportMultimediaFiles()

    self.assertEqual (mem[0], '0', 'Проверка воспроизведения мультимедиа не пройдена!')


  def tearDown(self):
    if hasattr( self, 'noroot'):
      with ConRun( self.noroot.uid, self.noroot.gid, output=[1] ):
        with Terminal() as T:
          T.add('pulseaudio --start', ignore_output=True)

class testSupportGraphicFiles(BaseTest):
    """
    Проверка возможности просмотра, базового редактирования и сохранения
    графических файлов в форматах BMP, JPEG, PNG и GIF
    """

    def runTest(self):
      # log.info('Начинается проверка работы с графическими файлами.')

      # log.info( '  Создание тестового пользователя "rosa-test-user"...' )
      # u = User( name='rosa-test-user', password='rosa-test-user', home='/home/rosa-test-user', se_user='user_u', se_range='s0-s3' )

     #@with ConRun( u.uid, u.gid, memory_bytes=30000 ) as mem:
      code = guitests.testSupportGraphicFiles( '/var/tmp' )

      # if mem[0] != '0':
      #   log.info( '  '.join( mem ) )
      #   self.fail( 'Проверка работы с графическими файлами не пройдена.\n' )

      self.assert_( code == 0, 'Проверка работы с графическими файлами не пройдена.\n' )


    def tearDown(self):
      shutil.rmtree( '/var/tmp/images', ignore_errors=True )
      processEx( 'kolourpaint', kill=True )

class testSupportEmail(BaseTest):
  """
  Проверка возможности использования электронной почты
  """

  def runTest(self):

    log.info( 'Начинается проверка работы с электронной почтой.' )

    log.info( '  Создание тестового пользователя "rosa-test-user"...' )
    u = User( name='rosa-test-user', password='rosa-test-user', home='/home/rosa-test-user', se_user='user_u', se_range='s0-s3' )

    with ConRun( u.uid, u.gid, memory_bytes=3000 ) as mem:
      guitests.testSupportEmail()
      X( 'sync' )

    if mem[0] != '0':
      log.info( '  \n'.join( mem ) )
      self.fail( 'Проверка работы с электронной почтой не пройдена!' )

class testSupportCalendar(BaseTest):
  """
  Проверка возможности использования календаря
  """

  def runTest(self):

    log.info( 'Начинается проверка работы с календарем' )

    log.info( '  Создание тестового пользователя "rosa-test-user"...' )
    u = User( name='rosa-test-user', password='rosa-test-user', home='/home/rosa-test-user', se_user='user_u', se_range='s0-s3' )

    with ConRun( u.uid, u.gid, memory_bytes=3000 ) as mem:
      guitests.testSupportCalendar()
      X('sync')

    if mem[0] != '0':
      log.info( '  \n'.join( mem ) )
      self.fail( 'Проверка работы с календарем не пройдена!' )

class testUrpmiCustomCurl(BaseTest):
    """
    Проверка работы urpmi с произвольным Curl - в частности,
    с Curl с поддержкой шифрования по ГОСТ
    """

    def runTest(self):
        log.info('Начинается проверка работы urpmi с произвольным '
                    'Curl.')
        # Запускаем urpmi, выставив downloader в curl_gost, а curl_gost_path - в фэйковый curl,
        # в роли которого заиспользуем собственно rosa-test-suite.py
        # Делаем вид, что хотим установить пакет из сети - http://fake_host/fake.rpm
        # (чтобы urpmi попробовал вызвать curl и скачать пакет)
        # Указываем '--debug', чтобы urpmi печатал, как именно вызывается curl,
        # и проверяем, что вызывается наш rosa-test-suite.py
        errcode, output = getstatusoutput("urpmi --auto --debug "
                                          "--downloader=curl_gost "
                                          "--curl_gost_path=/usr/share/rosa-test-suite/rosa-test-suite.py "
                                          "--test http://fake_host/fake.rpm 2>&1 "
                                          "| grep -q '/usr/share/rosa-test-suite/rosa-test-suite.py' ")
        self.assertEqual(errcode, 0, 'Проверка работы urpmi с произвольным Curl '
                                     'не пройдена.\n')

        # То же самое для urpmi.addmedia - проверим, что при попытке добавить
        # удаленный репозиторий urpmi.addmedia вызовет наш rosa-test-suite.py
        # при попытке закачать метаданные
        errcode, output = getstatusoutput("urpmi.addmedia --auto --debug "
                                          "--downloader=curl_gost "
                                          "--curl_gost_path=/usr/share/rosa-test-suite/rosa-test-suite.py "
                                          "fake_repo http://fake_repo 2>&1 "
                                          "| grep -q '/usr/share/rosa-test-suite/rosa-test-suite.py' ")
        self.assertEqual(errcode, 0, 'Проверка работы urpmi с произвольным Curl '
                                     'не пройдена.\n')
        log.info('Проверка работы urpmi с произвольным Curl пройдена успешно.')

class testCupsFilters(BaseTest):
  """
  Проверка работоспособности фильтров Cups
  """
    # TODO:
    # try to test these ones, as well as other files in /usr/lib/cups/filter/ installed by cups itself:
    #
    # /usr/lib/cups/filter/commandtoescpx
    # /usr/lib/cups/filter/commandtopclx
    # /usr/lib/cups/filter/gstopxl
    # /usr/lib/cups/filter/gstoraster
    # /usr/lib/cups/filter/urftopdf
    # /usr/lib/cups/filter/rastertoescpx
    # /usr/lib/cups/filter/pdftoijs
    # /usr/lib/cups/filter/pdftoippprinter
    # /usr/lib/cups/filter/pdftoopvp
    # /usr/lib/cups/filter/textonly

  def setUp(self):
    super(testCupsFilters, self).setUp()

    mPath = os.path.dirname(os.path.realpath(__file__))
    self.pdf = os.path.join(mPath, 'test_data', 'document.pdf')
    self.ps = os.path.join(mPath, 'test_data', 'document.ps')
    self.raster = os.path.join(mPath, 'test_data', 'document.raster')
    self.txt = os.path.join(mPath, 'test_data', 'document.ps')
    self.image = os.path.join(mPath, 'test_data', 'images', 'test.png')
    self.banner = os.path.join(mPath, 'test_data', 'print', 'standard.banner')

  def runTest(self):
    log.info('Начинается проверка работоспособности фильтров Cups.')

    #Опции - произвольные, на работоспособность фильтра это влиять не должно
    with Terminal( StreamToLog( log ) ) as t:
      t.error('  Во время тестирования cups-фильтров произошла ошибка [{code}]: {msg}.')
      t.add( '/usr/lib/cups/filter/pdftops 1 2 3 1 5 {}'.format( self.pdf ), ignore_output=True)\
        .info('  PDF -> PS\n')
      t.add( '/usr/lib/cups/filter/pdftopdf 1 2 3 1 5 {}'.format( self.pdf ) , ignore_output=True)\
        .info('  PDF -> PDF\n')
      t.add( '/usr/lib/cups/filter/pdftoraster 1 2 3 1 5 {}'.format( self.pdf ), ignore_output=True)\
        .info('  PDF -> RASTER\n')
      t.add( '/usr/lib/cups/filter/imagetops 1 2 3 1 5 {}'.format( self.image ), ignore_output=True)\
        .info('  IMAGE -> PS\n')
      t.add( '/usr/lib/cups/filter/imagetopdf 1 2 3 1 5 {}'.format( self.image ), ignore_output=True)\
        .info('  IMAGE -> PDF\n')
      t.add( '/usr/lib/cups/filter/imagetoraster 1 2 3 1 5 {}'.format( self.image ), ignore_output=True)\
        .info('  IMAGE -> RASTER\n')
      t.add( '/usr/lib/cups/filter/bannertopdf 1 2 3 1 5 {}'.format( self.banner ), ignore_output=True)\
        .info('  BANNER -> PDF\n')
      t.add( '/usr/lib/cups/filter/rastertopdf 1 2 3 1 5 {}'.format( self.raster ), ignore_output=True)\
        .info('  RASTER -> PDF\n')
      t.add( '/usr/lib/cups/filter/rastertopclx 1 2 3 1 5 {}'.format( self.raster ), ignore_output=True)\
        .info('  RASTER -> PCLX\n')
      t.add( '/usr/lib/cups/filter/pstotiff 1 2 3 1 5 {}'.format( self.ps ), ignore_output=True)\
        .info('  PS -> TIFF\n')
      t.add( '/usr/lib/cups/filter/texttops 1 2 3 1 5 {}'.format( self.txt ), ignore_output=True)\
        .info('  TEXT -> PS\n')
      t.add( '/usr/lib/cups/filter/texttopdf 1 2 3 1 5 {}'.format( self.txt ), ignore_output=True)\
        .info('  TEXT -> PDF\n')

    self.assertEqual( t.fail[0], 0, 'Произошла ошибка:{}'.format( t.fail[1] ) )

    log.info('Проверка работоспособности фильтров Cups пройдена успешно.')

  def __del__(self):
    pass

class testAutologinApmdz(BaseTest):
    """
    Проверка PAM-модуля автовхода с устройством АПМДЗ
    """

    def runTest(self):
        from modules.pexpect import run
        log.info('Начинается проверка работы модуля автовхода.')
        pamModule = 'pam_apmdz.so'
        kdePamCfg = '/etc/pam.d/kde'
        self.assertTrue(os.path.exists(kdePamCfg),
                        'Не найден файл конфигурации %s.' % kdePamCfg)
        with open(kdePamCfg, 'r') as f:
            pamConfig = f.read()
            modFound = None
            for line in pamConfig.split('\n'):
                modFound = re.search( r'\s+' + pamModule.replace('.', '\\.') + r'\s*(.*)$', line )
                if modFound:
                    modParams = modFound.group(1)
                    break
            self.assertTrue( modFound is not None, 'В файле конфигурации %s не подключен модуль %s.'
                            (kdePamCfg, pamModule))
        pamModulePath = '/lib/security/' + pamModule
        if not (os.path.exists(pamModulePath)):
            pamModulePath = '/lib64/security/' + pamModule
            self.assertTrue(os.path.exists(pamModulePath),
                            'Модуль %s в системе не найден.' % pamModule)
        errcode, output = getstatusoutput("%s %s" % (pamModulePath, modParams))
        self.assertEqual(errcode, 0,
                         'При обращении к PAM-модулю %s произошла ошибка.\n%s' %
                         (pamModulePath, output))
        username = output
        log.info('PAM-модуль возвратил имя пользователя: %s.' % output)
        log.info('Выполняется тестирование аутентификации...')
        (output, errcode) = run('pamtester kde "%s" authenticate' % username,
                                withexitstatus=True,
                                events={'(?i)password:': '\x04\n'})
        self.assertEqual(errcode, 0,
                         ('Аутентификация с модулем автовхода не выполнена '
                          '(код состояния %d).\n' % errcode) +
                         'Сообщения программы проверки:\n' + output)

        log.info('Проверка работы PAM-модуля автовхода '
                    'с устройством АПМДЗ пройдена успешно.')

class testSupportWeb(BaseTest):
    """
    Проверка возможности работы в сети по протоколам HTTP, HTTPS
    """
    httpt = None
    httpst = None
    ftpTestClass = None

    class HttpTestRequestHandler(http.server.BaseHTTPRequestHandler):
        proto = 'HTTP'

        def do_GET(self):
            global web_request_served
            self.send_response( 200 )
            self.send_header( 'Content-type', 'text-html' )
            self.end_headers()
            self.wfile.write( ( '<html><head><title>{} Test Page : TEST PASSED</title></head>'
                                '<body>It works (with {}).</body></html>'
                                .format( self.proto, self.proto )
                              ).encode() )
            web_request_served[self.proto] = True

        def log_message(self, format, *args):
            return

    class HttpsTestRequestHandler(HttpTestRequestHandler):
        proto = 'HTTPS'

    class HThread(threading.Thread):
        def __init__(self, ws, proto):
            global web_request_served
            threading.Thread.__init__(self)
            self.daemon = True
            self.ws = ws
            self.proto = proto
            web_request_served[self.proto] = False

        def run(self):
            global web_request_served
            while not (web_request_served[self.proto]):
                self.ws.handle_request()

    def runTest(self):
        global web_request_served
        log.info( 'Начинается проверка работы с веб-протоколами.' )

        httpd  = http.server.HTTPServer( ('localhost', 50080), testSupportWeb.HttpTestRequestHandler)
        httpsd = http.server.HTTPServer( ('localhost', 50443), testSupportWeb.HttpsTestRequestHandler )

        mPath = os.path.dirname( os.path.realpath(__file__) )
        cert = os.path.join( mPath, 'test_data', 'web', 'localhost.pem' )
        httpsd.socket = ssl.wrap_socket( httpsd.socket, server_side=True, certfile=cert, cert_reqs=ssl.CERT_NONE )
        web_request_served = {}
        self.httpt  = testSupportWeb.HThread( httpd, 'HTTP' )
        self.httpt.start()
        self.httpst = testSupportWeb.HThread( httpsd, 'HTTPS' )
        self.httpst.start()

        log.info( 'Начинается проверка наличия программы обработки гипертекстовых страниц (браузера)' )

        log.info( '  Создание тестового пользователя "rosa-test-user"...' )
        u = User( 'rosa-test-user', password='rosa-test-user', home='/home/rosa-test-user',
                  se_user='user_u', se_range='s0-s3' )

        processEx('chrome', kill=True )

        with ConRun( u.uid, u.gid, memory_bytes=3000 ) as mem:
          guitests.testSupportWeb()

        self.assertEqual( mem[0], '0', 'Проверка работы с веб-протоколами не пройдена! ' )


    def tearDown(self):
      if self.httpt and self.httpt.isAlive():
        try:
            self.httpt._Thread__stop()
        except:
            pass
      if self.httpst and self.httpst.isAlive():
        try:
            self.httpst._Thread__stop()
        except:
            pass
      if self.ftpTestClass:
        self.ftpTestClass.tearDown()

      processEx( 'chrome', kill=True )

class testStress(BaseTest):

  CPU_CORES = psutil.cpu_count()

  @staticmethod
  def payload_cpu():
    X( 'stress --cpu {} --timeout 20'.format( testStress.CPU_CORES ) )

  @staticmethod
  def payload_ram():
    X( 'stress --vm {} --vm-bytes 500M --vm-keep --timeout 20'.format( testStress.CPU_CORES ) )

  @staticmethod
  def payload_io():
    X( 'stress --io {} --timeout 20'.format( testStress.CPU_CORES ) )

  @staticmethod
  def payload_hdd():
    X( 'stress --hdd {} --timeout 20'.format( testStress.CPU_CORES ) )

  @staticmethod
  def sleep_guard():

    u = User()
    os.setgid( u.gid ); os.setuid( u.uid );

    sg = SuspendGuard( 'rosa-test-suite', 'stress test' )
    sg.block()

  def Go(self, payload, stop_event:Event, wait=True):

    if stop_event.is_set():
      return 0

    with Damper():
      process = MP.Process( target=payload )
      process.daemon = True
      process.start()

      if wait:
        while process.is_alive() and not stop_event.is_set():
          sleep(1)

        if process.is_alive() and stop_event.is_set():
          process.terminate()
          return 0
      else:
        return process

    return process.exitcode

  # runs from rosa-test-suite.py
  def make_hud_and_run():
    with Terminal() as term:
      term.add( 'tmux start-server' )
      term.add( 'tmux new-session -d -s "STRESSHUD"' )
      term.add( 'tmux splitw -h -p 75')
      term.add( 'tmux selectp -t 1' )
      term.add( 'tmux send-keys "htop" Enter' )
      term.add( 'tmux selectp -t 0' )

      term.add( 'tmux send-keys "rosa-test-suite 122" Enter' )
      term.add( 'tmux attach-session -t "STRESSHUD"' )

  def runTest(self):

    log.info( '  Начато нагрузочное тестирование.' )
    stop = Event()

    log.info( '  Отсчет до приостановки сеанса отменён.' )
    sg = self.Go( self.sleep_guard, stop, wait=False)

    desc = ' Идет нагрузочное тестирование [{type}] :'
    keyboard.add_hotkey('ctrl+space, esc', lambda x: x.set(), args=(stop,), suppress=True, timeout=2 )

    print( 'Внимание! Запущено нагрузочное тестирование.' )
    print( 'Каждые 20 секунд производится смена типа нагрузки [CPU, RAM, IO, HDD].' )
    print( 'Для завершения тестирование нажмите ctrl+space, затем esc.' )

    with Indicator( ' Старт нагрузочного тестирования', ' прошло {elaps}') as indicator:

      while not stop.is_set() and not sleep(1):

        indicator.set_message( desc.format( type='CPU' ) )
        cpu = self.Go( self.payload_cpu, stop )
        self.assert_( cpu == 0, ' Во время нагрузочного тестирования CPU произошла ошибка.' )

        indicator.set_message( desc.format( type='RAM' ) )
        ram = self.Go( self.payload_ram, stop )
        self.assert_( ram == 0, ' Во время нагрузочного тестирования RAM произошла ошибка.' )

        indicator.set_message( desc.format( type='IO' ) )
        io = self.Go( self.payload_io, stop )
        self.assert_( io == 0, ' Во время нагрузочного тестирования IO произошла ошибка.' )

        indicator.set_message( desc.format( type='HDD' ) )
        hdd = self.Go( self.payload_hdd, stop )
        self.assert_( hdd == 0, ' Во время нагрузочного тестирования HDD произошла ошибка.' )

    print( 'Нагрузочное тестирование завершено пользователем.' )
    log.info( '  Нагрузочное тестирование завершено.' )
    log.info( '  Отсчет до приостановки сеанса возобновлён.' )
    sg.terminate()

    sleep(3)

class testFsProgs(BaseTest): pass
class testATM(BaseTest): pass
class testServiceRouting(BaseTest): pass
class testSupportMPLS(BaseTest): pass
class testX25FrameRelayHDLC(BaseTest): pass
class testCLI(BaseTest): pass
class testServiceSmartCard(BaseTest): pass
class testServiceVconsole(BaseTest): pass
class testWiFi(BaseTest): pass
class testMail(BaseTest): pass
class testSPI(BaseTest): pass
class testFilemanager(BaseTest): pass
class testImageConverter(BaseTest): pass
class testCoreutils(BaseTest): pass
class testStorageManage(BaseTest): pass
class testUserAccountsManage(BaseTest): pass
class testProcStat(BaseTest): pass
class testPackageManager(BaseTest): pass
class testHardwareInfo(BaseTest): pass
class testOSConfigTools(BaseTest): pass
class testDhcpNfsEtc(BaseTest): pass
class testRdp(BaseTest): pass
class testSLAB(BaseTest): pass
class testServiceInit(BaseTest): pass
class testServiceCron(BaseTest): pass
class testServiceAtd(BaseTest): pass
class testServiceCups(BaseTest): pass
class testServiceSshd(BaseTest): pass
class testServiceVncServer(BaseTest): pass
class testServiceLdapClient(BaseTest): pass
class testServiceLocalMsgBus(BaseTest): pass
class testServiceNtpd(BaseTest): pass
class testServiceNfsExtAttr(BaseTest): pass
class testServiceVsftpd(BaseTest): pass
class testServiceSnmp(BaseTest): pass
class testServiceRsync(BaseTest): pass
class testServiceSyslog(BaseTest): pass
class testServiceTMask(BaseTest): pass
class testServicePcscd(BaseTest): pass
class testInstalledOfficeWriter(BaseTest): pass
class testInstalledOfficeCalc(BaseTest): pass
class testInstalledOfficeImpress(BaseTest): pass
class testInstalledOfficeDraw(BaseTest): pass
class testInstalledScanApp(BaseTest): pass
class testInstalledMediaPlayer(BaseTest): pass
class testInstalledPdfViewerConverter(BaseTest): pass
class testInstalledArk(BaseTest): pass
class testInstalledJavaRE(BaseTest): pass
class testInstalledWine(BaseTest): pass
class testInstalledMailClient(BaseTest): pass
class testInstalledBackup(BaseTest): pass
class testInstalledCalculator(BaseTest): pass
class testInstalledConsole(BaseTest): pass
class testInstalledFileManager(BaseTest): pass
class testInstalledControlCenter(BaseTest): pass
class testInstalledXmlTool(BaseTest): pass
class testDiagnosticSupport(BaseTest): pass
class testMailServer(BaseTest): pass


if __name__ == '__main__':
    #whatlibs()
    pass
