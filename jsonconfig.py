#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

import logging
from collections import OrderedDict
import json
import os
from pathlib import PosixPath as Path
import shutil

import dbus
from modules.kplpack.utils import Return, colorCon as I, kernelParamCheck, packageCheck, x

logger = logging.getLogger( "rts.configurator" )

class Test(object):

  def __init__(self, id=-1, name='', category='', description=''):
    self.id          = id
    self.name        = name
    self.category    = category
    self.description = description

    self.kconfig = []
    self.kmodule = []
    self.service = []
    self.package = []
    self.dmesg   = []

    self.suite    = ''
    self.enabled  = False

  def check_kconfig(self) -> int:
    """ returns the number of the parameter that caused the error """

    for i,p in enumerate( self.kconfig ):
      if kernelParamCheck( p ) > 0:
        return i
    return -1

  def check_kmodule(self) -> int:
    """ returns the number of the kernel modules not found """

    for i,km in enumerate( self.kmodule ):
      if 'auto:' in km:
        mod = km.split('auto:')[1].strip()
      else:
        mod = km

      code = x( 'modprobe -R {}'.format( mod ) )

      if code != 0:
        return i

    return -1

  def check_service(self) -> int:
    """ returns the number of the service not found """

    for i,s in enumerate( self.service ):

      if 'dbus:' in s:
        svc = s.split('dbus:')[1].strip()

        bus   = dbus.SystemBus()
        bus_o = bus.get_object('org.freedesktop.DBus', '/org/freedesktop/DBus')
        
        services_list = list( map( str, bus_o.ListNames( dbus_interface='org.freedesktop.DBus' ) ) )

        if svc not in services_list:
          return i

      else:
        if 'auto:' in s:
          svc = s.split('auto:')[1].strip()
        else:
          svc = s

          if 0 != x( 'systemctl list-unit-files | grep {}'.format( svc ) ):
            return i
          
    return -1

  def check_package(self) -> int:
    """ returns the number of the packege not found """

    pkgs = []
    for i,pkg in enumerate( self.package ):
      pkgs = pkg.split( '|' )
        
      res = 0
      for p in pkgs:
        res += packageCheck( p )

      if res == 0 : return i

    return -1

  def check_dmesg(self, msg:str='') -> int:
    """ returns the number of the message not found"""

    code, out = x( 'dmesg', Return.codeout )

    if code == 0:

      if len( msg ) > 0:
        if msg not in out:
          return 1

      else:
        for i,m in enumerate( self.dmesg ):
          if m not in out:
            return i
    else:
      print( 'check_dmesg: error {} - {}'.format( code, out ) )
    
    return -1

  def clear_dmesg(self) -> bool:
    code, out = x( 'dmesg -c', Return.codeout )

    if code != 0:
      print( 'clear_dmesg: error {} - {}'.format( code, out ) )
      return False
    
    return True

  def run_kmodules(self) -> int:
    """ returns the number of the kernel module that caused the error """

    for i,km in enumerate( self.kmodule ):

      if 'auto:' in km:
        mod = km.split('auto:')[1].strip()

        # disable module if it is in use
        if x( 'lsmod | grep {}'.format( mod ) ) == 0:
          self.rem_kmodule( i )

        code, out = x( 'modprobe {}'.format( mod ), Return.codeout )

        if code != 0:
          print( 'run_kmodules: modprobe error {} - {}'.format( code, out ) )
          return i
      
    return -1

  def rem_kmodules(self) -> int:
    """ returns the number of the kernel module that caused the error """

    for i,km in enumerate( self.kmodule ):
      if 'auto:' in km:
        mod = km.split('auto:')[1].strip()

        code, out = x( 'rmmod {}'.format( mod ), Return.codeout )

        if code != 0:
          print( 'run_kmodules: rmmod error {} - {}'.format( code, out ) )
          return i
      
    return -1

  def run_kmodule(self, index:int, params:str=''):
    """ returns tuple (err, output) """
    
    if len(self.kmodule) == 0:
      return ( 1, 'Empty kmodules list!') 

    if index > len( self.kmodule ) -1 or index < 0:
      return ( 1, 'Index out of range!' )

    if len( params ) > 0:
      return x( 'modprobe {} {}'.format( self.kmodule[index], params ), Return.codeout )
    else:
      return x( 'modprobe {}'.format( self.kmodule[index] ), Return.codeout )

  def rem_kmodule(self, index:int):
    """ returns tuple (err, output) """

    if len(self.kmodule) == 0:
      return ( 1, 'Empty kmodules list!') 

    if index > len( self.kmodule ) -1 or index < 0:
      return ( 1, 'Index out of range!' )

    return x( 'rmmod {}'.format( self.kmodule[index] ), Return.codeout )

  def run_services(self):
    """ returns the number of the service that caused the error """

    for i,s in enumerate( self.service ):

      if 'auto:' in s:
        svc = s.split('auto:')[1].strip()

        code, out = x( 'systemctl start {}'.format( svc ), Return.codeout )

        if code != 0:
          print( 'run_services: systemctl error {} - {}'.format( code, out ) )
          return i
      
    return -1
  
  def stop_services(self):
    """ returns the number of the service that caused the error """

    for i,s in enumerate( self.service ):

      if 'auto:' in s:
        svc = s.split('auto:')[1].strip()

        code, out = x( 'systemctl stop {}'.format( svc ), Return.codeout )

        if code != 0:
          print( 'run_services: systemctl error {} - {}'.format( code, out ) )
          return i
      
    return -1

  def enable(self):
    self.enabled = True
    print( 'Тест {} c номером {} включен в общий запуск!'.format( self.name, self.id ) )
  
  def disable(self):
    self.enabled = False
    print( 'Тест {} c номером {} исключен из общего запуска!'.format( self.name, self.id ) )

class Suite(object):

  def __init__(self, name=''):
    self.name     = name
    self.tests    = []
    self.excludes = []

  def __iter__(self):
    return iter( self.getReadyTests() )

  def __txt_range_to_list(self, ListOfRanges):
    rng = set()

    for grp in ListOfRanges:
      
      if len(grp) <= 0:
        continue

      if '-' not in grp:
        rng.add( int(grp) )
        continue

      a,b = grp.split( '-' )
      a = int(a)
      b = int(b)

      if a > b:
        print( 'SKIPPING: left bound  > right bound {} > {}'.format( a, b ) )
        continue

      for i in range( a, b + 1 ):
        rng.add( i )

    rng = list(rng)
    rng.sort()

    return rng

  def __list_to_txt_range(self, ListOfTests):
    
    if len(ListOfTests) <= 0:
      return []
    if len(ListOfTests) == 1:
      return [ "{}".format( ListOfTests[0] ) ]

    groups = []
    beg    = ListOfTests[0]

    for i,t in enumerate( ListOfTests ):
      if i == 0: continue
      
      if t - ListOfTests[i-1] != 1:
        groups.append( [beg,ListOfTests[i-1]] )
        beg = t
      
      if i == len(ListOfTests) -1:
        groups.append( [beg,t] )

    rng = []
    for g in groups:
      if g[0] != g[1]:
        rng.append( '{}-{}'.format( g[0], g[1] ) )
      else:
        rng.append( '{}'.format( g[0] ) )

    return rng

  def setTests(self, ListOfRanges):
    self.tests = self.__txt_range_to_list( ListOfRanges )
    self.__list_to_txt_range(self.tests)
  
  def setExcludes(self, ListOfRanges):
    self.excludes = self.__txt_range_to_list( ListOfRanges )

  def getTests(self):
    return self.__list_to_txt_range( self.tests )

  def getExcludes(self):
    return self.__list_to_txt_range( self.excludes )

  def getReadyTests(self):
    return list( set( self.tests ) - set( self.excludes ) )

class RTSConfig(object):

  def __init__(self, path:str):
    self.json = None

    self.suites = []
    self.tests  = []

    self.project = ''
    self.version = ''

    self.path = Path( path )
    self.str  = ''
  
  def __repr__(self) -> str:
    return self.str
  
  def __str__(self) -> str:
    return self.str

  def __format_config(self):

    formated  = '{\n'
    formated += '  "project" : "{}",\n'.format( self.project )
    formated += '  "version" : "{}",\n\n'.format( self.version )
    formated += '  "authors" : \n  {\n'
    
    for a in self.json['authors']:
      formated += '    "{}" : {{\n'.format( a )
      formated += '    "from" : {},\n'.format( self.json['authors'][a]['from'] )

      #for m in self.json['authors'][a]['e-mail']:
      formated += '    "e-mail" : [ "{}" ] }},\n\n'.format( '", "'.join(self.json['authors'][a]['e-mail']) )

    formated = formated[:-3] # remove coma
    formated += '\n  },\n\n'

    formated += '  "suites" : \n  {\n'

    for s in self.suites:
      formated += '    "{}" : \n'.format( s.name )
      formated += '    {\n'
      formated += '      "tests" : [ "{}" ],\n'.format( '", "'.join( s.getTests() ) )
      formated += '      "excludes" : [ "{}" ]\n'.format( '", "'.join( s.getExcludes() ) )
      formated += '    },\n\n'
    
    formated = formated[:-3] + '\n  },\n\n'

    formated += '  "tests" :\n  [\n'

    for t in self.tests:
      formated += '    {\n'
      formated += '      "id" : {},\n'.format( t.id )
      formated += '      "name" : "{}",\n'.format( t.name )
      formated += '      "category" : "{}",\n'.format( t.category )
      formated += '      "description" : "{}",\n\n'.format( t.description )

      if len( t.kconfig ) > 0:
        formated += '      "kconfig" : [ "{}" ],\n'.format( '", "'.join( t.kconfig ) )
      if len( t.kmodule ) > 0:
        formated += '      "kmodule" : [ "{}" ],\n'.format( '", "'.join( t.kmodule ) )
      if len( t.service ) > 0:
        formated += '      "service" : [ "{}" ],\n'.format( '", "'.join( t.service ) )
      if len( t.package ) > 0:
        formated += '      "package" : [ "{}" ],\n'.format( '", "'.join( t.package ) )
      if len( t.dmesg ) > 0:
        formated += '      "dmesg" : [ "{}" ],\n'.format( '", "'.join( t.dmesg ) )

      if not formated.endswith('\n\n'):
        formated += '\n'
      formated += '      "enabled" : {}\n'.format( 'true' if t.enabled else 'false' )
      formated += '    },\n'
    
    formated = formated[:-2] + '\n'
    formated += '  ]\n}'
    
    self.str = formated

  def __empty_list_sanitize(self, lst:list):
    for i,e in enumerate( lst ):
      if type(e) is int and e < 0:
        lst.pop( i )
      if type(e) is str and e == '':
        lst.pop( i )
    return lst

  def print_tests(self):
    print( I( '{:^8}{:<10}{:^{width}}', 16, 244 ).format(' НОМЕР ', 'Вкл/Выкл', 'ОПИСАНИЕ', width=shutil.get_terminal_size()[0] -18 ) )
    for t in self.tests:
      status = I( '[ Вкл  ] ', 34 ) if t.enabled else I( '[ Выкл ] ', 124 )
      print( '{:^8}{:<10}{:<{width}}'.format( t.id, status, t.description, width=shutil.get_terminal_size()[0] -17 ) )

  def get_actual_suite(self) -> Suite:
    """ returns None if no test suite was found 
        for the current platform or an error occurred
    """

    code, out = x( 'lsb_release -d -s', Return.codeout )

    if code != 0:
      print( 'get_actual_suite: error {} - {}'.format( code, out ) )
      return None
    
    distr = out.strip('"')

    for s in self.suites:
      if s.name in distr:
        return s
    
    return None

  def get_ready_tests(self) -> list:

    suite = self.get_actual_suite()
    ready = []

    for test in self.tests:
      if test.enabled and test.id in suite.tests and test.id not in suite.excludes:
        ready.append( test )

    return ready

  def load(self, path=''):

    P = Path( path ) if path != '' else self.path

    with P.open( 'r', encoding='utf-8') as f:
      self.json = json.load( f, object_pairs_hook=OrderedDict )

    self.project = self.json['project']
    self.version = self.json['version']

    for s in self.json['suites']:
      suite = Suite(s)
      suite.setTests(    self.__empty_list_sanitize( self.json['suites'][s]['tests']    ) )
      suite.setExcludes( self.__empty_list_sanitize( self.json['suites'][s]['excludes'] ) )
      self.suites.append( suite )

    for t in self.json['tests']:
      test = Test()
      test.id          = t['id']
      test.name        = t['name']
      test.category    = t['category']
      test.description = t['description']

      if "kconfig" in t:
        test.kconfig = self.__empty_list_sanitize( t['kconfig'] )
      if "kmodule" in t:
        test.kmodule = self.__empty_list_sanitize( t['kmodule'] )
      if "service" in t:
        test.service = self.__empty_list_sanitize( t['service'] )
      if "dmesg" in t:
        test.dmesg   = self.__empty_list_sanitize( t['dmesg'] )
      if "package" in t:
        test.package = self.__empty_list_sanitize( t['package'] )

      test.enabled = t['enabled']
      self.tests.append( test )

  def save(self, path=''):
    
    P = Path( path ) if path != '' else self.path

    self.__format_config()
    with P.open( 'w+', encoding='utf-8' ) as f:
      f.write( self.str )

  def test(self, id:int) -> Test:
    """ return Test object with id """
    for test in self.tests:
      if test.id == id:
        return test


def test():
  os.system('clear')
  
  cfg = RTSConfig( '/PROJECTS/rosa-test-suite-dev/config.json' )
  cfg.load()
  #cfg.tests[1].dmesg[0]='FOO PASS'
  cfg.tests[1].disable()
  cfg.save()

  #print(cfg.suites[0].tests)
  print()
  #print(cfg.suites[0].excludes)
  cfg.print_tests()

  print(cfg.tests[1].check_kconfig() )
  print(cfg.tests[1].check_kmodule() )
  print(cfg.tests[1].clear_dmesg()   )
  print(cfg.tests[1].run_kmodules()  )
  print(cfg.tests[1].check_dmesg()   )
  print(cfg.suites[1].excludes       )

  print(cfg.get_actual_suite().name)

  print( cfg.tests[100].package )
  print( cfg.tests[100].check_package() )

  print( cfg.tests[54].name            )
  print( cfg.tests[54].service         )
  print( cfg.tests[54].check_service() )
  
  print( cfg.test(36).name )
  print( cfg.test(36).package )


if __name__ == "__main__":
  test()