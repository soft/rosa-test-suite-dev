#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@:::::::::@@@@@@@@@@@::::::@@@@@@@@@@@::::@@@@@@@@@@@@@:@@@@@@@@@   │
# │   @@@:------------::@@@@:----------::@@@@:--------::@@@@@@@:---:@@@@@@@   │
# │   @@@:---@@@@@@@@:--:@@---:@@@@@@@---:@@---:@@@@@:@@@@@@@@:-----@@@@@@@   │
# │   @@@:---@@@@@@@@:--:@:--:@@@@@@@@:---@@---:@@@@@@@@@@@@@@--:@:--@@@@@@   │
# │   @@@:---@@@@@@@:---@@:--:@@@@@@@@:---@@@:------:::@@@@@@---@@@--:@@@@@   │
# │   @@@:-----------::@@@:--:@@@@@@@@:---@@@@@@:::::---@@@@:--:@@@:--:@@@@   │
# │   @@@:---@@@@@:--:@@@@:--:@@@@@@@@:--:@@@@@@@@@@@:--:@@:-----------:@@@   │
# │   @@@:---@@@@@@:---:@@@:--:::@@@::--:@@::-:::@@::---@@:--:@@@@@@@---:@@   │
# │   @@@:::-@@@@@@@@::::@@@@:::-----::@@@@@@:::----::@@@::::@@@@@@@@:::-@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │                                                                           │
# │                              ROSA Test Suite                              │
# │                                                                           │
# │   Copyright © 2020 LLC "NTC IT ROSA"                                      │
# │   License: GPLv2                                                          │
# │   Authors:                                                                │
# │       Arthur Yalaletdinov <a.yalaletdinov@ntcit-rosa.ru> 2013             │
# │       Michael Mosolov     <m.mosolov@rosalinux.ru>       2019             │
# │                                                                           │
# │   This program is free software; you can redistribute it and/or modify    │
# │   it under the terms of the GNU General Public License as published by    │
# │   the Free Software Foundation; either version 2, or (at your option)     │
# │   any later version.                                                      │
# │                                                                           │
# │   This program is distributed in the hope that it will be useful,         │
# │   but WITHOUT ANY WARRANTY; without even the implied warranty of          │
# │   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            │
# │   GNU General Public License for more details                             │
# │                                                                           │
# │   You should have received a copy of the GNU General Public License       │
# │   License along with this program; if not, write to the                   │
# │   Free Software Foundation, Inc.,                                         │
# │   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.           │
# │                                                                           │
# ╘═══════════════════════════════════════════════════════════════════════════╛

import argparse
import datetime
import logging
import os  # stat, pwd, grp, subprocess, commands
import re
import shutil
import subprocess
from subprocess import Popen
import sys
import tempfile
from time import sleep
import unittest
import datetime
import warnings
from logging.handlers import QueueHandler, QueueListener
from multiprocessing import Manager, Process, get_start_method, set_start_method

import selinux

import tests
import jsonconfig

from modules.kplpack.utils import Damper, Indicator, Return, cls, etherdog
from modules.kplpack.utils import colorCon as I  # , Profiler
from modules.kplpack.utils import timerBlockCountDown, x
from tests import testStress

warnings.filterwarnings("ignore", category=DeprecationWarning)

def initLogger(debug=False):
    dbgformat = '%(message)s'
    timeformat = '%H:%M:%S'
    lvl = logging.DEBUG
    log.setLevel(lvl)
    formatter = logging.Formatter(dbgformat, timeformat)

    if debug:
        sHandler = logging.StreamHandler(sys.stderr)
        sHandler.setFormatter(formatter)
        sHandler.setLevel(lvl)
        log.addHandler(sHandler)

    fHandler = logging.FileHandler( log_path, 'w' )
    fHandler.setLevel( logging.DEBUG )
    fHandler.setFormatter( formatter )

    # setup multiprocessing logger
    with Damper(): # without this line: GTK WARNING???
      log.M  = Manager()
      log.Q  = log.M.Queue(-1)
      log.QL = QueueListener( log.Q, fHandler )
      log.QL.start()

    log.addHandler( QueueHandler( log.Q ) )
    #log.addHandler(fHandler)

def doTest(testname:str):
    try:
      suite = unittest.TestLoader().loadTestsFromName( testname, tests )
    except AttributeError:
      raise NameError( 'Тест «{}» отсутствует.'.format( testname ) )
    result = unittest.TestResult()
    suite.run( result )

    if not result.wasSuccessful():
      msg = ''
      for fail in result.failures:
        msg += fail[1] + '\n'
      for error in result.errors:
        msg += error[1] + '\n'

      raise RuntimeError( msg )
    else:
      log.info( '\n' + I( 'Тест успешно пройден.', 10 ).center( 65, ' ' ) + '\n'*5 )

def runTest(test:jsonconfig.Test, screen_log):

  def makeReadable(msg):
    ret = msg
    match = re.search('AssertionError:.+', msg)

    if match is not None:
      ret = match.group()
      ret = ret.replace('AssertionError:', '').strip('\n').strip(' ').split(' : ')
      ret = ret[1] if len( ret ) > 1 else ret[0]

    return '\t' + ret

  errcode = 0

  if (screen_log is not None):
    screen_log.put( '$#{}'.format( test.id ) )

  point = 'Тест {} - {}'.format( test.id, test.name ).center(50,' ')
  top_border    = '┌' + '─' * len(point) + '┐'
  bottom_border = '└' + '─' * len(point) + '┘'

  log.info( '\n' + top_border )
  log.info( '│' + point + '│' )
  log.info( bottom_border     )
  log.info( '\n  ' + test.description + '\n' )

  try:
    #log.info( 'Запуск теста «{}»...'.format( test.name ) )

    tests.CURRTEST = test
    doTest( 'test' + tests.CURRTEST.name )

  except NameError as e:
    msg = str(e)
    log.info( I( msg, 9 ) )

    if (screen_log is not None):
      screen_log.put( '[{}]\t Тест с именем {} отсутствует.\n'.format( test.id, test.name ) )

    errcode = 1

  except RuntimeError as e:
    msg = I( str(e), 9 )
    log.info( I( 'Тест {} ({}) не пройден: {}', 9 ).format( test.name, test.id, msg ) )

    if (screen_log is not None):
      screen_log.put( '[{}]\t Тест «{}» не пройден:\n\t{}\n'.format( test.id, test.description, makeReadable(msg) ) )

    errcode = 1

  if (screen_log is not None):
    screen_log.put( '$%' )

  return errcode

def runTestByNum(test: jsonconfig.Test):

  log.info( 'Тестирование начато: {}.'.format( datetime.datetime.now().strftime( '%d.%m.%Y %H:%M:%S' ) ) )

  if not test:
    print('Некорректно задан номер теста (={}). Тест с заданным номером отсутствует.'.format ( test.id ), file=sys.stderr)
    log.error('Некорректно задан номер теста (={}). Тест с заданным номером отсутствует.'.format( test.id ) )
    return 1

  with Damper():
    err = runTest( test, None )

  log.info( 'Тестирование завершено: {}.'.format( datetime.datetime.now().strftime( '%d.%m.%Y %H:%M:%S' ) ) )

  return err

def runAllTests(config:jsonconfig.RTSConfig):
  with Damper():

    tests  = config.get_ready_tests()
    total  = len( tests )

    timerBlockCountDown( 'Пожалуйста, не прерывайте тестирование, это может привести к ошибкам!\
                         \nХорошего дня!', 'Оповещение:', unit='сек', waiting=5 )

    M = Manager()
    Q = M.Queue( 1 )
    progress_bar = Process( target=tqdm_spawn, args=( Q, total ), daemon=True )
    progress_bar.start()

    # https://i.imgur.com/nMlycfb.png python3.8+

    failed = 0

    for test in tests:

      os.sync()

      if os.fork() == 0:
        os._exit( runTest( test, Q ) )
      else:
        failed += os.WEXITSTATUS( os.wait()[1] )

    Q.join()

    if progress_bar.join(timeout=1.0) is None:
      progress_bar.terminate()

    print( 'Пройдено тестов: {}, не пройдено: {}.'.format( total - failed, failed ) )
    print( 'Дата проведения: ' + datetime.datetime.now().strftime( '%H:%M %d.%m.%Y ' ) )
    print( '\nРезультаты тестирования: /var/log/rosa-test-suite-{}.log'.format( __version__ ) )
    print( 'Для просмотра результатов воспользуйтесь: rosa-test-suite -s' )
    print( '                                     или: rosa-test-suite -a номер_теста.' )
    print( '\nРекомендуется перезагрузить систему!\n' )

def tqdm_spawn( Q, total:int ):
  """
  '$#N' - set current test number(N)
  '$%'  - increment progress
  '*'   - print text
  """
  from tqdm import tqdm

  _rbar = '{l_bar}{bar}| {n_fmt}/{total_fmt} [ Прошло: {elapsed} Осталось: ~{remaining} ]'

  _tqdm = tqdm( total=total, file=sys.stdout, desc='Тестирование [0]', bar_format=_rbar, lock_args=(True,), ascii=True )

  pos  = 0
  try:
    while pos < total:
      data = Q.get()

      if str(data).startswith('$#'):
        _tqdm.set_description( 'Тестирование [{}]'.format( int(data[2:]) ), True )
        Q.task_done()
      elif str(data).startswith('$%'):
        _tqdm.update();
        pos += 1
        Q.task_done()
      else:
        _tqdm.write( data )
        Q.task_done()

    _tqdm.set_description( 'Тестирование завершено', True )

  except KeyboardInterrupt: pass
  except Exception as e:
    print( 'tqdm_spawn: {}'.format( str(e) ) )
  finally:
    _tqdm.close()

def see_log( num:int=-1 ):

  global __CONF__

  tests = [n.id for n in __CONF__.tests]

  if num < 0:
    p = subprocess.run( [ 'less', '-P', 'Для выхода нажмите "q"',
                          '/var/log/{}-{}.log'.format( __project__, __version__ ) ], start_new_session=True )

  elif num in tests:
    p = subprocess.run( [ 'less', '-P', 'Для выхода нажмите "q"', '-p', 'Тест {}'.format( num ),
                          '/var/log/{}-{}.log'.format( __project__, __version__ ) ], start_new_session=True )
  else:
    print( 'Тест с номером {} отсутствует в наборе для текущей системы.'.format( num ) )

def main(cmdline):

  cls()

  ret    = 0
  config = __CONF__

  # list
  if cmdline.need_list:
    print( I( '{:^8}{:<10}{:^{width}}', 16, 244 ).format(' НОМЕР ', 'Вкл/Выкл', 'ОПИСАНИЕ', width=shutil.get_terminal_size()[0] -18 ) )
    for t in config.tests:
      status = I( '[ Вкл  ] ', 34 ) if t.enabled else I( '[ Выкл ] ', 124 )
      print( '{:^8}{:<10}{:<{width}}'.format( t.id, status, t.description, width=shutil.get_terminal_size()[0] -17 ) )

    log.QL.stop()
    logging.shutdown()

    return ret

  # run
  try:

    if cmdline.number > -1:

      if cmdline.need_off:
        config.test( cmdline.number ).disable()
        config.save()
      elif cmdline.need_on:
        config.test( cmdline.number ).enable()
        config.save()
      else:
        ret = runTestByNum( config.test( cmdline.number ) )

    else:
      log.info( 'Тестирование начато: {}.'.format(datetime.datetime.now().strftime( '%d.%m.%Y %H:%M:%S' ) ) )
      runAllTests(config)
      log.info( 'Тестирование завершено: {}.'.format( datetime.datetime.now().strftime( '%d.%m.%Y %H:%M:%S' ) ) )

  except RuntimeError as e:
    log.error( str(e) )
    print( str(e), file=sys.stderr )
    ret = 1

  except KeyboardInterrupt:
    log.error( 'Выполнение прервано пользователем.' )
    print( '\nВыполнение прервано пользователем!\n'\
           'Необходимо ручное удаление отладочной информации, в противном случае, '\
           'отсутствие ошибок при повторном тестировании не гарантируется!', file=sys.stderr )
    ret = 1
  finally:
    log.QL.stop()
    logging.shutdown()

    if cmdline.need_see: # see log
      see_log()

  return ret

def system_check():
  """
      file types:
        c - %config configuration file.
        d - %doc documentation file.
        g - %ghost file (i.e. the file contents are not included in the package payload).
        l - %license license file.
        r - %readme readme file.

      file status:
        S - file Size differs
        M - Mode differs (includes permissions and file type)
        5 - digest (formerly MD5 sum) differs
        D - Device major/minor number mismatch
        L - readLink(2) path mismatch
        U - User ownership differs
        G - Group ownership differs
        T - mTime differs
        P - caPabilities differ
  """

  with Indicator( ' Ожидайте, идет проверка системы: ' ) as status:

    ok_states = [ '.......T.', '.........' ]
    results   = tempfile.SpooledTemporaryFile( max_size=1024, mode='bw+' )
    cols      = shutil.get_terminal_size()[0] - 40

    process = Popen( 'rpm -Va', shell=True, stdout=subprocess.PIPE, bufsize=1 )

    # if code != 0:
    #   print( 'Во время проверки установленной системы произошла ошибка [{}]: {}'.format( code, out ) )
    #   exit( code )

    for line in iter( process.stdout.readline, b''):

      inf   = line.decode('UTF-8').split()
      flags = inf[0]
      ftype = inf[1] if len(inf) > 2 else None
      path  = inf[1] if len(inf) < 3 else inf[2]

      if cols > 40:
        status.set_suffix( '{: <{width}}'.format( path[:cols], width=cols ) )

      if ftype is not None or \
        flags in ok_states:
        continue

      results.write( line )

  process.wait()
  results.seek(0)
  print( results.read().decode('UTF-8') )
  results.close()

  return process.returncode

def helpmsg():
  print( '\n  {} [{}] - набор тестов операционной системы ROSA'.format(I("rosa-test-suite", 39), __version__ ) )

  message = \
  """
  Использование:
    rosa-test-suite [параметры] {N}

    Если не указан {N}, последовательно будут выполнены все тесты.

  Пример:
    rosa-test-suite -s 112

  Параметры:
    -h --help    Показать это сообщение и завершить работу.

    -с {N}         Запуск теста под номером {N}.
    -n {N}
    --number {N}

    -l --list    Вывести список всех тестов с описанием.
    --off        Отключить тест под номером {N}.
    --on         Включить тест под номером {N}.
    -s --see     Открыть лог после выполнения теста. Если {N} не указан,
                 будет открыт лог без выполнения теста.

    -a {N}         Открыть лог к тесту {N} без его выполнения.
    --see-at {N}

    -v           Показать версию пакета.
    --version

    --config {P}   Указать путь {P} к файлу конфигурации.

  Служебный функционал:
    --check      Запустить базовую проверку системы (rpm -Va).
    --etherdog   Запустить непрерывный  процесс  мониторинга  сетевого
                 соединения. При потере соединения, будут перезапущены
                 все сетевые интерфейсы.
    --stress     Запустить непрерывное нагрузочное тестирование.

  """.format( N=I('N',29), P=I('P',89) )

  print( message )
  print( 'kernelplv@gmail.com // m.mosolov@rosalinux.ru // www.rosalinux.ru 2020©' )

if __name__ == "__main__":

  cls()

  if ( os.getuid() != 0 ):
    print("Тесты должны быть запущены с правами суперпользователя!")
    exit(0)

  if get_start_method( allow_none=True ) is None:
    set_start_method('spawn')

  parser = argparse.ArgumentParser( add_help=False, usage=argparse.SUPPRESS )

  parser.add_argument( 'number', nargs='?', type=int, default=-1 )
  parser.add_argument( '-h', '--help', action='store_true' )
  parser.add_argument( '-c', '-n', '--number', type=int, default=-1, metavar='N' )
  parser.add_argument( '-l', '--list', dest='need_list', action='store_true', default=False )
  parser.add_argument( '--off', dest='need_off', action='store_true', default=False )
  parser.add_argument( '--on', dest='need_on', action='store_true', default=False )
  parser.add_argument( '-s', '--see', dest='need_see', action='store_true', default=False )
  parser.add_argument( '-a', '--see-at', dest='need_see_at', action='store_true', default=False )
  parser.add_argument( '-v', '--version', dest='need_version', action='store_true', default=False )
  parser.add_argument( '--check', dest='need_check', action='store_true', default=False )
  parser.add_argument( '--etherdog', dest='need_etherdog', action='store_true', default=False )
  parser.add_argument( '--config', metavar='path_to_config', dest='config', action='store',
                       default='/usr/share/rosa-test-suite/config.json' )
  parser.add_argument( '--stress', dest='stress', action='store_true', default=False )

  cmdline = parser.parse_args( sys.argv[1:] )

  global __project__
  global __version__
  global __CONF__
  global log
  global log_path

  __CONF__ = jsonconfig.RTSConfig( cmdline.config )
  __CONF__.load()

  __project__ = __CONF__.project
  __version__ = __CONF__.version

  if cmdline.help:
    helpmsg()
    exit(0)

  if cmdline.need_version:
    print( __version__ )
    exit(0)

  if cmdline.need_see and cmdline.number == -1:
    see_log()
    exit(0)

  if cmdline.need_see_at:
    see_log( cmdline.number )
    exit(0)

  if cmdline.number == -1 or __CONF__.test( cmdline.number ).category != 'security':
    if selinux.security_getenforce():
      print("Тесты общей категории должны быть запущены в режиме SELinux: Permissive [ setenforce 0 ].\n")
      exit(0)
  else:
    if not selinux.security_getenforce():
      print("Тесты категории security должны быть запущены в режиме SELinux: Enforcing [ setenforce 1 ].\n")
      exit(0)

  if cmdline.need_check:
    exit( system_check() )

  if cmdline.need_etherdog:
    exit( etherdog() )

  if cmdline.stress:
    testStress.make_hud_and_run()
    exit(0)

  log      = logging.getLogger('rts')
  log_path = '/var/log/{}'.format( __project__ + '-' + __version__ + '.log' )
  initLogger()

  # stress test
  if cmdline.number == 122:
    main( cmdline )
    x( 'tmux kill-session -t "STRESSHUD"' )

  else:
    sys.exit( main( cmdline ) )
