#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cgi
import cgitb
import psycopg2

cgitb.enable()

form = cgi.FieldStorage()

print("Content-Type: text/html; charset=utf-8")
print("")

try:
  #conn = psycopg2.connect(dbname='testdb', user='tester', password='TopSecret', host='localhost')
  #http://localhost/?db=testdb&u=tester&p=TopSecret&h=localhost&f0=hello+tester!&f1=time+to+done+your+work&f2=Bye!
  conn = psycopg2.connect(dbname=form['db'].value, user=form['u'].value, password=form['p'].value, host=form['h'].value)
except Exception as e:
  print("<p>Произошла ошибка при подключении к базе данных PostgreSQL.<br>{}</p>".format( e ))
  exit(1)

cur = conn.cursor()

cur.execute('SELECT * FROM testtable')
records = cur.fetchall()

cur.close()
conn.close()

print("<p>Тестовая страница для сервера Apache.</p>")
print("Получение таблицы с сервера PostgreSQL:<br>")
print("Запрос: SELECT * FROM testdb:<br><br>")

print('<table cellspaceing="2" cellpadding="5" border="2">')
print( "<tr><td>Id</td><td><center>Message</center></td><td><center>Check status</center></td></tr>" )

check = True

query_args = []
for k in form.keys():
  if k in ['db','u','p','h']:
    continue
  else:
    query_args.append( form[k].value )

#print("<p>{}</p>".format(query_args))

for r in records: # row constructing
  print("<tr>")
  print("<td>{}</td>".format( r[0] ))
  print("<td>{}</td>".format( r[1] ))

  if r[1] in query_args:
    print("<td>OK!</td>")
  else:
    check = False
    print("<td>WRONG!</td>")

  print("</tr>")

print("</table>")

if check:
  print("<title>TEST PASSED!</title>")
  print("<p>Тест успешной пройден. С сервера PostgreSQL получены верные данные.</p>")
else:
  print("<title>TEST FAILED!</title>")
  print("<p>Тест провален. С сервера PostgreSQL получены не верные данные.</p>")
