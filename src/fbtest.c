#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <limits.h>
#include <fcntl.h>
#include <directfb.h>
#include <directfb_keynames.h>

#define DFBCHECK(x...) {										\
	DFBResult err = x;											\
																\
	if (err != DFB_OK) {										\
		fprintf( stderr, "%s <%d>:\n\t", __FILE__, __LINE__ );	\
		DirectFBErrorFatal( #x, err );							\
	}															\
}

typedef struct _DeviceInfo DeviceInfo;

struct _DeviceInfo
{
	DFBInputDeviceID			device_id;
	DFBInputDeviceDescription	desc;
	DeviceInfo					*next;
};

static DFBEnumerationResult enum_input_device(DFBInputDeviceID device_id, DFBInputDeviceDescription desc, void *data )
{
	DeviceInfo **devices = data;
	DeviceInfo *device;

	device = malloc( sizeof(DeviceInfo) );

	device->device_id = device_id;
	device->desc      = desc;
	device->next      = *devices;

	*devices = device;
	return DFENUM_OK;
}

int main( int argc, char *argv[] )
{
	static IDirectFB* dfb = NULL;
	DeviceInfo* devices;
	if (argc < 2)
	{
	  fprintf( stderr, "Error : Filename not specified\n" );
	  exit(1);
	}
	DFBCHECK(DirectFBInit (&argc, &argv));
	DFBCHECK(DirectFBCreate (&dfb));
	DFBCHECK(dfb->EnumInputDevices( dfb, enum_input_device, &devices ));
	int ret = dfb->SetVideoMode(dfb, 800, 600, 2);
	dfb->Release (dfb);
	return ret;
}
