/*
 ┌───────────────────────────────────────────────────────────────────────────┐
 │                                                                           │
 │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
 │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
 │   @@@@@:::::::::@@@@@@@@@@@::::::@@@@@@@@@@@::::@@@@@@@@@@@@@:@@@@@@@@@   │
 │   @@@:------------::@@@@:----------::@@@@:--------::@@@@@@@:---:@@@@@@@   │
 │   @@@:---@@@@@@@@:--:@@---:@@@@@@@---:@@---:@@@@@:@@@@@@@@:-----@@@@@@@   │
 │   @@@:---@@@@@@@@:--:@:--:@@@@@@@@:---@@---:@@@@@@@@@@@@@@--:@:--@@@@@@   │
 │   @@@:---@@@@@@@:---@@:--:@@@@@@@@:---@@@:------:::@@@@@@---@@@--:@@@@@   │
 │   @@@:-----------::@@@:--:@@@@@@@@:---@@@@@@:::::---@@@@:--:@@@:--:@@@@   │
 │   @@@:---@@@@@:--:@@@@:--:@@@@@@@@:--:@@@@@@@@@@@:--:@@:-----------:@@@   │
 │   @@@:---@@@@@@:---:@@@:--:::@@@::--:@@::-:::@@::---@@:--:@@@@@@@---:@@   │
 │   @@@:::-@@@@@@@@::::@@@@:::-----::@@@@@@:::----::@@@::::@@@@@@@@:::-@@   │
 │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
 │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
 │                                                                           │
 │                  Additional tools for ROSA Test Suite                     │
 │                                                                           │
 │   Copyright © 2020 LLC "NTC IT ROSA"                                      │
 │   License: GPLv3                                                          │
 │   Authors:                                                                │
 │       Michael Mosolov     <m.mosolov@rosalinux.ru>       2020             │
 │                                                                           │
 │   This program is free software; you can redistribute it and/or modify    │
 │   it under the terms of the GNU General Public License as published by    │
 │   the Free Software Foundation; either version 3, or (at your option)     │
 │   any later version.                                                      │
 │                                                                           │
 │   This program is distributed in the hope that it will be useful,         │
 │   but WITHOUT ANY WARRANTY; without even the implied warranty of          │
 │   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            │
 │   GNU General Public License for more details                             │
 │                                                                           │
 │   You should have received a copy of the GNU General Public License       │
 │   License along with this program; if not, write to the                   │
 │   Free Software Foundation, Inc.,                                         │
 │   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.           │
 │                                                                           │
 ╘═══════════════════════════════════════════════════════════════════════════╛
 ┌───────────────────────────────────────────────────────────────────────────┐
 │  This program is trying to check the isolation of a child process.        │
 ╘═══════════════════════════════════════════════════════════════════════════╛
 */



#include <iostream>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define PROJECT_NAME "test-project"

using namespace std;

const uint64_t PARENT = 55776284;
const uint64_t CHILD  = 33490092;

uint64_t          m1;
volatile uint64_t m2;
uint64_t          m3 = PARENT;
volatile uint64_t m4 = PARENT;

int main( ) 
{
  uint64_t                  m5 ;
  volatile uint64_t        *m6 = &m2; //reinterpret_cast<uint64_t *>( PARENT );
  static uint64_t          *m7 = &m1; //reinterpret_cast<uint64_t *>( PARENT );
  static volatile uint64_t  m8 = PARENT;

  int status;

  *m6 = PARENT;
  *m7 = PARENT;
   m5 = PARENT;

  int pid = fork();
  
  if ( pid == 0 )
  {
    m3 = m4 = m5 = m8 = CHILD;
    *m6 = *m7 = CHILD;

    exit(0);
  }

  if ( pid < 0 ) {
    cerr << "fork failed" << endl;
    return 1;
  }

  while( ! waitpid( pid, &status, 0 ) );

  if ( (  m1 != PARENT ) ||
       (  m2 != PARENT ) ||
       (  m3 != PARENT ) ||
       (  m4 != PARENT ) || 
       (  m5 != PARENT ) ||
       ( *m6 != PARENT ) ||
       ( *m7 != PARENT ) ||
       (  m8 != PARENT )    ) 
  {

    cout << "FAIL" << endl;
    return 1;
  }

  cout << "PASS" << endl;

  return 0;
}