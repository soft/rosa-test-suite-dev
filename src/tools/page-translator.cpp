// ┌───────────────────────────────────────────────────────────────────────────┐
// │  This program translates a virtual address into a physical one.           │
// │                                                                           │
// │  thank you:                                                               │
// │   https://github.com/cirosantilli/linux-kernel-module-cheat               │
// │   https://github.com/ccurtsinger/get-pfn/blob/master/get-pfn.c            │
// │                                                                           │
// │  from man proc 5:                                                         │
// │    The /proc/[pid]/pagemap file is present only if the                    │
// │    CONFIG_PROC_PAGE_MONITOR kernel configuration option is enabled!       │
// ╘═══════════════════════════════════════════════════════════════════════════╛


#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#define PROJECT_NAME "page-translator"


#include <unistd.h>
#include <fcntl.h>

using namespace std;

struct PagemapEntry
{
  uint_fast64_t pfn        : 55;
  bool          soft_dirty : 1;
  bool          exclusive  : 1;
  uint_fast8_t  padding    : 4;
  bool          file_page  : 1;
  bool          swapped    : 1;
  bool          present    : 1;
};

uint_fast64_t getPhysicalAddress( std::string pagefile_path, uint_fast64_t address ) 
{
  int pagemap_fd = open( pagefile_path.c_str(), O_RDONLY );

  if ( pagemap_fd == -1 ) {
    cout << "Failed to open pagemap  " << pagefile_path << "." << endl;
    return {};
  }

  size_t page_size   = getpagesize();
  size_t page_index  = address / page_size;
  size_t page_offset = address % page_size;
  PagemapEntry P     = {0,0,0,0,0,0,0};

  if ( lseek( pagemap_fd, page_index * sizeof P, SEEK_SET ) == -1 ) {
    cout << "Failed to seek to requested entry in pagemap." << endl;
    return 0;
  }

  if ( read( pagemap_fd, &P, sizeof P ) != sizeof P ) {
    cout << "Failed to reed pagemap entry." << endl;
    return 0;
  }

  close( pagemap_fd );

  if ( P.present ) {
    cout << boolalpha << P.file_page << "|";
    return P.pfn * page_size + page_offset;
  }
  else cout << "|";

  return 0;
}

int main( int argc, char **argv ) 
{
  if ( argc != 3 ) {
   cout << "usage:   page-translator pid virtual_address"    << endl
        << "example: page-translator 14837 0x7f1804000000" << endl
        << "return:  \"file_or_shared(bool) | address\"" << endl
        << "current args: " << argc << endl;
    return 1;
  }
  
  stringstream pagefile_path;
  pagefile_path << "/proc/" << argv[1] << "/pagemap";

  stringstream read_hex( argv[2] );
  uint_fast64_t address = 0;
  read_hex >> hex >> address;

  uint_fast64_t E = getPhysicalAddress( pagefile_path.str(), address );

  if ( E == 0 )
    cout << "Physical address " << hex << address << " not found or direct." << endl;
  else
  {
    cout << hex << E << endl;
    return 0;
  }

  return 1;
}