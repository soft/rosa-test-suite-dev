/*
 *
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/types.h>

MODULE_LICENSE("GPL v2");

#define MODULE_NAME "memfreetest: "

#define MARK 0x1337beef
#define NUM_TESTS 5

size_t bufSize = 32;

static void mark_buf(void *pDest, int len)
{
	unsigned int *ptr = pDest;
	int i;
	for (i = 0; i < len / sizeof(*ptr); i++)
		ptr[i] = MARK;
}

static int find_mark(void *pDest, int len)
{
	unsigned int *ptr = pDest;
	int i;
	for (i = 0; i < len / sizeof(*ptr); i++)
		if (ptr[i] == MARK)
			return 1;
	return 0;
}

static int start_test_memfree(void)
{
	int n;
	void *pBuf;
	printk(MODULE_NAME "running test\n");

	for (n = NUM_TESTS; n > 0; n--)
	{
		pBuf = kmalloc(n * bufSize, GFP_USER);
		if (!pBuf) break;
		mark_buf(pBuf, n * bufSize);
		kfree(pBuf);

		if ( find_mark(pBuf, n * bufSize) )
		{
			printk(MODULE_NAME "test failed\n");
			break;
		}
	}
	if ( n == 0 )
		printk(MODULE_NAME "test passed\n");
	return 0;
}

static void stop_test_memfree(void)
{
	printk(MODULE_NAME "exit\n");
}

module_init(start_test_memfree);
module_exit(stop_test_memfree);
