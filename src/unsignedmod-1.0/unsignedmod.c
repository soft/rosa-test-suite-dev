#include <linux/module.h>

MODULE_LICENSE("GPL v3");

#define MODULE_NAME "unsignedmod: "

static int start_test(void)
{
	printk(MODULE_NAME "test failed!\n");

	return 0;
}

static void stop_test(void)
{
	printk(MODULE_NAME "exit\n");
}

module_init(start_test);
module_exit(stop_test);
