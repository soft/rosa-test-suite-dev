#include <atspi/atspi.h>

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "spi_utils.h"

#define die(format, ...) {fprintf(stderr, format"\n", ##__VA_ARGS__); finalize(); return 1;}

enum {
    application,
    frame,
    docFrame,
    menubar,
    mFile,
    miFileExit,

    last_accessible
} accessibles;

AtspiAccessible *ac[last_accessible];

enum {
    appFrameStr,

    last_accessible_string
} accessible_strings;

gchar *acs[last_accessible_string];

void finalize() {
    int i;
    for (i = 0; i < last_accessible; i++) {
        if (ac[i]) g_object_unref(ac[i]);
    }

    for (i = 0; i < last_accessible_string; i++) {
        if (acs[i]) g_free(acs[i]);
    }

    int leak = atspi_exit();
    //fprintf(stderr, "RefCount Leak:%d\n", leak);
}

int main(int argc, char **argv) {
    if (argc != 3) {
        log("Internal Error! Invalid parameters.");
        return 1;
    }
    int result = atspi_init();
    if (result) {
        log("Is your machine a11y enabled?\n");
        return result;
    }
    memset(ac, 0, sizeof(ac));
    memset(acs, 0, sizeof(acs));

    char *target = argv[1];

    if (!(ac[application]   = findApplicationByName(target)))                               die("Target application (%s) not found.", target);
    if (!(ac[frame]         = findElement(ac[application], false, "frame", NULL, NULL)))    die("Application frame not found.");
    if (!(acs[appFrameStr]  = atspi_accessible_get_name(ac[frame], NULL)))                  die("Unable to get the application frame name.");
    if (strcmp(acs[appFrameStr], argv[2]) == 0) {
        result = 0;
    } else {
        log("Invalid application caption '%s'.", acs[appFrameStr]);
        result = 1;
    }

    if (!(ac[menubar]       = findElement(ac[application], true, "menu bar", "", NULL)))    die("Application menu bar not found.");
    if (!(ac[mFile]         = findElement(ac[menubar], true, "menu", "File", "Файл")))      die("File menu not found.");
    if (!(doAction(ac[mFile], "click")))                                                    die("Could not click File menu.");
    if (!(ac[miFileExit]    = findElementOrWait(ac[mFile], false,
                                "menu item", "Exit", "Выход", 500)))
        if (!(ac[miFileExit]    = findElementOrWait(ac[mFile], false,
                           "menu item", "Exit LibreOffice", "Выйти из LibreOffice", 500)))  die("File->Exit menu item not found.");
    if (!(doAction(ac[miFileExit], "click")))                                               die("Could not click File->Exit menu item.");
    finalize();
    return result;
}
