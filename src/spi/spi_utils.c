#include "spi_utils.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>

int ends_with(const char *str, const char *suffix)
{
    if (!str || !suffix)
        return 0;
    size_t lenstr = strlen(str);
    size_t lensuffix = strlen(suffix);
    if (lensuffix >  lenstr)
        return 0;
    return strncmp(str + lenstr - lensuffix, suffix, lensuffix) == 0;
}

AtspiAccessible* findApplicationByName(const char *targetName) {
    AtspiAccessible* desktop = atspi_get_desktop(0);
    int t = atspi_get_desktop_count();
    AtspiAccessible* app = NULL;
    int i = 0;
    if (desktop) {
        int waitms = 5000;
        while (waitms > 0) {
            int appcnt = atspi_accessible_get_child_count(desktop, NULL);
            for (i = 0; i < appcnt; i++) {
                AtspiAccessible* capp = atspi_accessible_get_child_at_index(desktop, i, NULL);
                if (capp) {
                    gchar *cappName = atspi_accessible_get_name(capp, NULL);
                    if (cappName) {
                        int cmp = strcmp(targetName, cappName);
                        g_free(cappName);
                        if (cmp == 0) {
                            app = capp;
                            break;
                        }
                    }
                    g_object_unref(capp);
                }
            }
            if (app) break;
            usleep(100 * 1000);
            waitms -= 100;
        }
        g_object_unref(desktop);
    }
    return app;
}

AtspiAccessible *findElementOrWait(AtspiAccessible *parent, bool recursive, const char *targetRoleName, const char *targetName, const char *targetAltName, int waitms) {
    AtspiAccessible *result;
    result = findElement(parent, recursive, targetRoleName, targetName, targetAltName);
    while (!result && waitms > 0) {
        int delay = waitms;
        if (delay > 100) delay = 100;
        usleep(delay * 1000);
        waitms -= delay;
        result = findElement(parent, recursive, targetRoleName, targetName, targetAltName);
    }
    return result;
}

AtspiAccessible *findElement(AtspiAccessible *parent, bool recursive, const char *targetRoleName, const char *targetName, const char *targetAltName) {
    AtspiAccessible *found = NULL;
    AtspiAccessible *child;
    int i;
    int n_children = atspi_accessible_get_child_count(parent, NULL);
    for (i = 0; i < n_children; i++) {
        if (i > 100) break; //Don't check many children such as Calc sheet cells.
        child = atspi_accessible_get_child_at_index(parent, i, NULL);
        if (child) {
            gchar *cRoleName = atspi_accessible_get_role_name(child, NULL);
            //printf("Role:%s \n", cRoleName);
            if (strcmp(targetRoleName, cRoleName) == 0) {
                if (targetName == NULL) {
                    found = child;
                } else {
                    gchar *cName = atspi_accessible_get_name(child, NULL);
                    //printf("Name:%s\n", cName);
                    if (cName) {
                        if ((strcmp(targetName, cName) == 0) ||
                            ((targetAltName != NULL) && (strcmp(targetAltName, cName) == 0))) {
                            found = child;
                        }
                        g_free(cName);
                    }
                }
            }
            g_free(cRoleName);
            if (found) break;
            if (recursive) {
                found = findElement(child, recursive, targetRoleName, targetName, targetAltName);
            }
            g_object_unref(child);
        }
        if (found) break;
    }
    return found;
}

bool doAction(AtspiAccessible *element, const char *action) {
    AtspiAction *accaction = atspi_accessible_get_action(element);
    bool result = false;
    if (accaction) {
        int i;
        int na = atspi_action_get_n_actions(accaction, NULL);
        int ci = -1;
        if (na == 1)
            ci = 0;
        else for (i = na - 1; i >= 0; i--) {
            gchar *aName = atspi_action_get_action_name(accaction, i, NULL);
            if (!aName) continue;
            int cmp = strcmp(action, aName);
            g_free(aName);
            if (cmp == 0) {
                ci = i;
                break;
            }
        }
        if (ci >= 0)
            result = atspi_action_do_action(accaction, ci, NULL);
        g_object_unref(accaction);
    } else {
        fprintf(stderr, "\tTarget element has no AtspiAccessibleAction interface.\n");
    }
    return result;
}

void print_accessible_tree(AtspiAccessible *accessible, char *prefix) {
    int n_children;
    int i;
    char *name;
    char *role_name;
    char *parent_name = NULL;
    char *parent_role = NULL;
    char child_prefix[100];
    AtspiAccessible *child;
    AtspiAccessible *parent;

    strncpy(child_prefix, prefix, 98);
    strcat(child_prefix, "*");
    parent = atspi_accessible_get_parent(accessible, NULL);
    if (parent) {
        parent_name = atspi_accessible_get_name(parent, NULL);
        parent_role = atspi_accessible_get_role_name(parent, NULL);
        g_object_unref(parent);
    }
    name = atspi_accessible_get_name(accessible, NULL);
    role_name = atspi_accessible_get_role_name(accessible, NULL);
    fprintf (stdout, "%sAtspiAccessible [%s] \"%s\"; parent [%s] %s.\n",
         prefix, role_name, name, parent_role, parent_name);
    g_free(name);
    g_free(role_name);
    g_free(parent_name);
    g_free(parent_role);
    n_children = atspi_accessible_get_child_count(accessible, NULL);
    for (i = 0; i < n_children; ++i) {
        if (i > 100) return; //Don't check many children such as Calc sheet cells.
        child = atspi_accessible_get_child_at_index(accessible, i, NULL);
        print_accessible_tree(child, child_prefix);
        g_object_unref(child);
    }
}
