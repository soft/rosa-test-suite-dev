#include <atspi/atspi.h>

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "spi_utils.h"

int main(int argc, char **argv) {
    int result = atspi_init();
    if (result) {
        log("Is your machine a11y enabled?\n");
        return result;
    }
    result = 1;

    static const char target[] = "Firefox";
    AtspiAccessible* application = findApplicationByName(target);
    if (!application) log("Target application (%s) not found.", target);
    else {
//        print_accessible_tree(application, "*");
        AtspiAccessible *menubar = findElement(application, true, "menu bar", "Application", NULL);
        if (!menubar) log("Application menu bar not found.");
        else {
            AtspiAccessible *mFile = findElement(menubar, false, "menu", "File", "Файл");
            if (!mFile) log("File menu not found.");
            else {
                if (!doAction(mFile, "click")) log("Failed to select File menu.");
                else {
                    usleep(500 * 1000);
                    AtspiAccessible *mFileExit = findElement(mFile, false, "menu item", "Exit", "Выход");
                    if (!mFileExit) log("Failed to find File->Exit menu item.");
                    else {
                        if (!doAction(mFileExit, "click")) log("Failed to select File->Exit menu item.");
                        else {
                            fprintf(stderr, "ok!");
                            result = 0;
                        }
                        g_object_unref(mFileExit);
                    }
                }
                g_object_unref(mFile);
            }
            g_object_unref(menubar);
        }
        g_object_unref(application);
    }

    int leak = atspi_exit();
    //fprintf(stderr, "RefCount Leak:%d\n", leak);
    return result;
}
