#ifndef SPI_UTILS_H
#define SPI_UTILS_H

#include <atspi/atspi.h>
#include <stdbool.h>

#define log(fmt, ...) fprintf(stderr, fmt"\n", ##__VA_ARGS__)


int ends_with(const char *str, const char *suffix);

AtspiAccessible *findApplicationByName(const char *app_name);
AtspiAccessible *findElement(AtspiAccessible *parent, bool recursive, const char *targetRoleName, const char *targetName, const char *targetAltName);
AtspiAccessible *findElementOrWait(AtspiAccessible *parent, bool recursive, const char *targetRoleName, const char *targetName, const char *targetAltName, int waitms);

bool doAction(AtspiAccessible *element, const char *action);

void print_accessible_tree(AtspiAccessible *accessible, char *prefix);

#endif

