#include <atspi/atspi.h>

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "spi_utils.h"

#define die(format, ...) {fprintf(stderr, format"\n", ##__VA_ARGS__); finalize(); return 1;}

enum {
    application,
    frame,
    docPresent,
    sTitle1,
    paragraph,
    pet,
    menubar,
    mFile,
    miFileSaveAs,
    miFileExit,
    dSave,
    dSaveFileName,
    sfet,
    btnSave,
    dQuestion1,
    btnYes1,
    dQuestionF,
    btnUseFormat,

    dlgSelectTmp,
    btnTmpCancel,

    last_accessible
} accessibles;

AtspiAccessible *ac[last_accessible];

enum {
    appFrameStr,

    last_accessible_string
} accessible_strings;

gchar *acs[last_accessible_string];

void finalize() {
    int i;
    for (i = 0; i < last_accessible; i++) {
        if (ac[i]) g_object_unref(ac[i]);
    }

    for (i = 0; i < last_accessible_string; i++) {
        if (acs[i]) g_free(acs[i]);
    }

    int leak = atspi_exit();
    //fprintf(stderr, "RefCount Leak:%d\n", leak);
}

int main(int argc, char **argv) {
    if (argc != 3) {
        log("Internal Error! Invalid parameters.");
        return 1;
    }
    int result = atspi_init();
    if (result) {
        log("Is your machine a11y enabled?\n");
        return result;
    }
    memset(ac, 0, sizeof(ac));
    memset(acs, 0, sizeof(acs));

    static const char target[] = "soffice";
    char *destDoc = argv[1]; //"~/Пробный документ.txt";
    char *docContents = argv[2]; //"Пробный документ в формате TXT";

    if (!(ac[application]   = findApplicationByName(target)))                               die("Target application (%s) not found.", target);
    //print_accessible_tree(ac[application], "Select a Template");
    
    if (!(ac[frame]         = findElement(ac[application], false, "frame", NULL, NULL)))    die("Application frame not found.");
    if (!(acs[appFrameStr]  = atspi_accessible_get_name(ac[frame], NULL)))                  die("Unable to get the application frame name.");
    if (!ends_with(acs[appFrameStr], "LibreOffice Impress"))                                die("Invalid application (%s) found.", acs[appFrameStr]);

    // close "Select a Template" dialog  Выйти из LibreOfice
    if (!(ac[dlgSelectTmp] = findElement(ac[application], true, "dialog", "Select a Template", "Выберите шаблон"))) die("Dialog not found!" );
    if (!(ac[btnTmpCancel] = findElement(ac[dlgSelectTmp], true, "push button", "Cancel", "Отменить")))            die("Cancel button not found!");
    if (!(doAction(ac[btnTmpCancel], "click")))                                                                    die("Could not click Cancel in dialog!.");
    // ~

    if (!(ac[docPresent]    = findElement(ac[frame], true,
                                "document presentation", NULL, NULL)))                      die("Unable to get the document presentation.");
    if (!(ac[sTitle1]      = findElement(ac[application], true,
                               "panel",  "PresentationTitle ", "Заглавие презентации ")))   die("Unable to get the presentation title shape.");

    if (!(ac[paragraph]     = findElement(ac[sTitle1], false,
                                "paragraph", NULL, NULL)))                                  die("Unable to get a title paragraph.");
    if (!(ac[pet]           = (AtspiAccessible *)atspi_accessible_get_editable_text(
                                ac[paragraph])))                                            die("Unable to get editable text.");
    if (!atspi_editable_text_set_text_contents(
                                (AtspiEditableText *)ac[pet], docContents, NULL))           die("Unable to change the document text.");

    usleep(100 * 1000);
    if (!(ac[menubar]       = findElement(ac[application], true, "menu bar", "", NULL)))    die("Application menu bar not found.");
    if (!(ac[mFile]         = findElement(ac[menubar], false, "menu", "File", "Файл")))     die("File menu not found.");
    if (!(doAction(ac[mFile], "click")))                                                    die("Could not click File menu.");
    if (!(ac[miFileSaveAs]  = findElementOrWait(ac[mFile], false,
                                "menu item", "Save As...", "Сохранить как...", 500)))       die("File->Save as... menu item not found.");
    if (!(doAction(ac[miFileSaveAs], "click")))                                             die("Could not click File->Save as... menu item.");
    if (!(ac[dSave]         = findElementOrWait(ac[application], true,
                                "dialog", "Save", "Сохранить", 5000)))                      die("Save dialog not found.");
    if (!(ac[dSaveFileName] = findElement(ac[dSave], true, "text", NULL, NULL)))            die("Input field not found in the Save dialog.");
    if (!(ac[sfet]          = (AtspiAccessible *)atspi_accessible_get_editable_text(
                                ac[dSaveFileName])))                                        die("Unable to get editable text for a filename.");
    if (!atspi_editable_text_set_text_contents(
                                (AtspiEditableText *)ac[sfet], destDoc, NULL))              die("Unable to change filename.");
    if (!(ac[btnSave]       = findElement(ac[dSave], true,
                                "push button", "Save", "Сохранить")))                       die("Save button not found in the Save dialog.");
    if (!(doAction(ac[btnSave], "click")))                                                  die("Could not click File->Save as... menu item.");
    if ((ac[dQuestion1]    = findElementOrWait(ac[application], true,
                                "alert", "Question", "Вопрос", 500))) { //Possible Overwrite confirmation
        if (!(ac[btnYes1]  = findElement(ac[dQuestion1], true,
                                "push button", "Yes", "Да")))                               die("Yes button not found in the Confirmation dialog.")
        if (!(doAction(ac[btnYes1], "click")))                                              die("Could not click Yes button.");
    }
    if ((ac[dQuestionF]    = findElementOrWait(ac[application], true,
                                "alert", "Confirm File Format", "Подтверждение формата файла", 500))) { //Possible Format confirmation
        //Use the first button
        if (!(ac[btnUseFormat] = findElement(ac[dQuestionF], true,
                                   "push button", NULL, NULL)))                             die("Confirmation button not found.")
        if (!(doAction(ac[btnUseFormat], "click")))                                         die("Could not click 'Use format' button.");
    }

    usleep(500 * 1000);
    if (!(doAction(ac[mFile], "click")))                                                    die("Could not click File menu.");
    if (!(ac[miFileExit]   = findElementOrWait(ac[mFile], false,
                            "menu item", "Exit LibreOffice", "Выйти из LibreOffice", 500))) die("File->Exit menu item not found.");
    usleep(300 * 1000);
    if (!(doAction(ac[miFileExit], "click")))                                               die("Could not click File->Exit LibreOffice menu item.");
    finalize();
    return 0;
}
