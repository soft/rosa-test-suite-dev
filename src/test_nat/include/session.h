#pragma once

#include <boost/asio.hpp>
#include <iostream>
#include <array>
#include <string>
#include <algorithm>
#include <system_error>

namespace ao = boost::asio;
using ao::ip::tcp;

class session
        : public std::enable_shared_from_this<session>
{
    tcp::socket socket_;
    static const int BUFF_SIZE = 64;
    std::array<char, BUFF_SIZE> data_;

public:
    session(tcp::socket socket)
        : socket_( std::move(socket) )
    {}

    ~session() { std::cout << "Server: session closed." << std::endl; }


    void start()
    {
        do_read();
    }

private:
    void do_read()
    {
        std::cout << "Server: reading.." << std::endl;
        auto self(shared_from_this());

        socket_.async_read_some( ao::buffer(data_),
        [this, self] (const boost::system::error_code &e, std::size_t len )
        {
            if ( !e )
            {
                std::string answer(data_.begin(), data_.end());
                if ( std::string::npos != answer.find("stop-good") )
                {
                    std::cout << "Server: get !stop code!" << std::endl;
                    socket_.close();
                    socket_.get_io_service().stop();

                }
                if ( std::string::npos != answer.find("stop-wrong") )
                {
                    std::cout << "Server: get !stop code!" << std::endl;
                    socket_.close();
                    socket_.get_io_service().stop();
                    throw( static_cast<size_t>(1) );
                }

                do_write(len);
            }
            else 
                std::cout << "ERROR: " << e << std::endl;
        });
    }

    void do_write( std::size_t len )
    {
        std::cout << "Server: read done: "
                  << std::string(data_.begin(), data_.begin()+len)
                  << "\nServer: writing.." << std::endl;

        auto self(shared_from_this());

        ao::async_write(socket_, ao::buffer(data_, len),
        [this, self] (const boost::system::error_code &e, std::size_t)
        {
            if ( !e )
                do_read();
            else 
                std::cout << "ERROR: " << e << std::endl;
        });
    }
};
