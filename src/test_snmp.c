#include <net-snmp-config.h>
#include <net-snmp-includes.h>
#include <mib_api.h>
#include <string.h>


static char *get_obj_string(netsnmp_session *session, const char *objid) {
    oid anOID[MAX_OID_LEN];
    size_t anOID_len;
    netsnmp_pdu *pdu;
    netsnmp_pdu *response;
    netsnmp_variable_list *vars;

    char *result = NULL;
    int status;

    pdu = snmp_pdu_create(SNMP_MSG_GET);
    if (!pdu) {
        fprintf(stderr, "Couldn't create a PDU for SNMP_GET.\n");
        return NULL;
    }

    anOID_len = MAX_OID_LEN;
    status = read_objid(objid, anOID, &anOID_len);
    if (status == 1) {
        snmp_add_null_var(pdu, anOID, anOID_len);
        status = snmp_synch_response(session, pdu, &response);
        if (status == STAT_SUCCESS && response->errstat == SNMP_ERR_NOERROR) {
            for (vars = response->variables; vars; vars = vars->next_variable) {
                if (vars->type == ASN_OCTET_STR) {
                    result = (char *)malloc(1 + vars->val_len);
                    memcpy(result, vars->val.string, vars->val_len);
                    result[vars->val_len] = '\0';
                    break;
                } else if (vars->type == ASN_TIMETICKS) {
                    result = (char *)malloc(12);
                    sprintf(result, "%lu", *vars->val.integer);
                    break;
                }
            }
        } else {
            fprintf(stderr, "\n");
            if (status == STAT_SUCCESS)
                fprintf(stderr, "Error in packet.\nReason: %s\n", snmp_errstring(response->errstat));
            else if (status == STAT_TIMEOUT)
                fprintf(stderr, "Timeout: No response from %s.\n", session->peername);
            else
                snmp_sess_perror("snmptester", session);
        }
        snmp_free_pdu(response);
    } else {
        fprintf(stderr, "Couldn't resolve textual OID '%s' (%d).\n", objid, status);
    }
    return result;
}

int main(int argc, char ** argv)
{
    netsnmp_session session, *ss;

    int status;
    int result = 1;
    oid rmOID[MAX_OID_LEN];
    size_t rmOID_len;

    if (argc < 2) {
        fprintf(stderr, "Invalid parameters (target hostname needed).\n");
        exit(1);
    }

    init_snmp("snmptester");
    snmp_sess_init(&session);
    session.peername = strdup(argv[1]);
    session.version = SNMP_VERSION_2c;
    session.community = (unsigned char *)"public";
    session.community_len = strlen((char *)session.community);

    SOCK_STARTUP;
    ss = snmp_open(&session);
    if (ss) {
        char *sysName = get_obj_string(ss, "SNMPv2-MIB::sysName.0");
        if (sysName) {
            printf("Target system name: %s\n", sysName);
            char *sysUpTime0 = get_obj_string(ss, "SNMPv2-MIB::sysUpTime.0");
            if (sysUpTime0) {
                printf("System uptime(0): %s\n", sysUpTime0);
                usleep(300*1000);
                char *sysUpTime1 = get_obj_string(ss, "SNMPv2-MIB::sysUpTime.0");
                if (sysUpTime1) {
                    printf("System uptime(1): %s\n", sysUpTime1);
                    if (strcmp(sysUpTime0, sysUpTime1) != 0) {
                        result = 0;
                    } else {
                        printf("System uptime not changed after 0.3 sec!\n");
                    }
                    free(sysUpTime1);
                }
                free(sysUpTime0);
            } else {
                fprintf(stderr, "Couldn't get sysUpTime(0).\n");
            }
            free(sysName);
        } else {
            fprintf(stderr, "Couldn't get sysName.\n");
        }
        snmp_close(ss);
    } else {
        fprintf(stderr, "Couldn't open an SNMP session.\n");
    }
    if (result == 0) {
        printf("Checking RMON-MIB support...\n");
        rmOID_len = MAX_OID_LEN;
        status = read_objid("RMON-MIB::statistics", rmOID, &rmOID_len);
        if (status == 1) {
            printf("RMON OID parsed successfully.\n");
        } else {
            fprintf(stderr, "Couldn't parse RMON OID (RMON::statistics).\n");
            result = 2;
        }
    }
    SOCK_CLEANUP;

    return result;
}
