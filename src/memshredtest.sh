#!/bin/bash
# Тест проверки очистки памяти
tmpdir=/tmp
tim=$tmpdir/tmpimage.img
mnt=$tmpdir/mnt/tmptestimage

mkdir -p $mnt

msg() { echo "MSG: $1"; }
clean() { umount $mnt;}
error() { echo -e "ERROR: $1"; exit 1; }
assertret() {
	if [ $? != 0 ];
    	then  clean ; error $1 ;
    fi; }

# создание образа диска
dd if=/dev/zero of=$tim bs=1M count=1
if [ $? != 0 ]; then error "не получилось создать $tim"; fi;
mke2fs -b 1024 -t ext2 -F $tim
assertret then error "не получилось создать ext2 на $tim"; 

# монтирование образа
mount -o loop -t ext2 $tim $mnt
assertret "не получилось смонтировать образ"

# создание файла с случайным содержимым
dd if=/dev/urandom of=$mnt/testfile ibs=1K bs=1K count=1
assertret "не получилось создать testfile"

sync
# проверка содержимого файла(что оно не забито нулями)
notzerrofound=0
while IFS= read -r -n1 char
do
    if [ $char != "\0" ];
    	then notzerrofound=1;
    	break; 
    fi;
done < "$mnt/testfile"

if [ notzerrofound == 0 ];
	then error "/dev/random возвратил одни нули";
fi

# принудительный сброс кэша
sync

# получение первого(и единственного) блока
startblk=$(echo "stat testfile" | debugfs $tim | sed -n "/BLOCK/{n;p;}" | sed 's|\(.*\):\(.*\)|\2|g')
msg "startblock=$startblk"

# проверка что в полученном блоке находится тот самый файл
dd if=$tim of=$tmpdir/restored_orblock skip=$startblk ibs=1K bs=1K count=1
r=$(diff $mnt/testfile $tmpdir/restored_orblock)
if [ ! -z "$r" ];
   then error "неверный startblk: $r";
fi


# удаление файла
shred --zero $mnt/testfile
assertret "ошибка выполнения shred"

# восстановление блока, удаленного утилитой shred
dd if=$tim of=$tmpdir/restored_block skip=$startblk ibs=1K  bs=1K count=1

sync

# проверка что содержимое восстановленного блока нулевое
while IFS= read -r -n1 char
do
    if [ "$char" = "\0" ];
    	then error "память не очищена";
    	break; 
    fi;
done < "$tmpdir/restored_block"

clean

msg "!Test passed!"

exit 0;
