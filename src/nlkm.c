#include <linux/module.h>
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>

#define NETLINK_USER 31

struct sock *nl_sk = NULL;

MODULE_LICENSE("GPL v2");

#define MODULE_NAME "netlinktest: "

static void clbkrcv(struct sk_buff *skb)
{
    struct nlmsghdr *nlh;
    int pid;
    struct sk_buff *skb_out;
    int msg_size;
    int res;
    char *msg;

    nlh = (struct nlmsghdr*)skb->data;
    printk(MODULE_NAME "received message: %s\n", (char*)nlmsg_data(nlh));
    pid = nlh->nlmsg_pid;

    msg = (char*)nlmsg_data(nlh);
    msg_size = strlen(msg);
    skb_out = nlmsg_new(msg_size, 0);

    if(!skb_out)
    {
        printk(MODULE_NAME "Failed to allocate new skb\n");
        return;
    }
    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, msg_size,0);

    NETLINK_CB(skb_out).dst_group = 0; /* not in mcast group */
    strncpy(nlmsg_data(nlh),msg,msg_size);

    res = nlmsg_unicast(nl_sk,skb_out,pid);

    if( res < 0)
        printk(MODULE_NAME "Error while sending message back to user\n");
}

static int __init nl_test_init(void)
{
    printk(MODULE_NAME "Entering netlink test module: %s\n", __FUNCTION__);
    struct netlink_kernel_cfg cfg = {
        .input = clbkrcv,
    };
    nl_sk = netlink_kernel_create(&init_net, NETLINK_USER, &cfg);
    if(!nl_sk)
    {
        printk(KERN_ALERT "Error creating socket.\n");
        return -1;
    }
    return 0;
}

static void __exit nl_test_exit(void)
{
    printk(MODULE_NAME "Exit netlink test module\n");
    netlink_kernel_release(nl_sk);
}

module_init(nl_test_init);
module_exit(nl_test_exit);
